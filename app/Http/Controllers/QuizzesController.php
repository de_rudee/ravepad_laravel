<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auth;
use App\QuizResponse;

class QuizzesController extends Controller
{
  var $name = 'Quizzes';
  var $helpers = array('Form', 'MyForm', 'Html', 'Javascript');
  var $uses = array('User', 'Quiz', 'Quizchoice', 'Quizresponse', 'Page');
  var $components = array('MyAuth', 'Session', 'RequestHandler', 'MyPhoto', 'Cookie');

  /**
   * AJAX call that gets one random quiz for user to choose (one that he hasn't chosen yet)
   **/
  function getFeatureQuiz($pageSlug = null)
  {
    if (!$this->isAjax) {
      die();
    }

    // Grab one quiz that has a picture, and user has not answered yet.
    // We should grab the most popular one, and randomize it to choose one.
    $current_user_id = Auth::user->id;
    $pageId = $pageSlug;
    if ($pageId == 0) {
      $pageRestriction = null;
    } else {
      $pageRestriction = array('Quiz.id_page' => "$pageId");
    }

    $quiz_response_conditions = array(
      'Quizresponse.id_user' => $current_user_id,
      'Quizresponse.deleted' => 0,
    );

    // Find quizzes the current user has already responded to, so they're excluded.
    $quizzesToExclude = array();
    if ($pageRestriction) {
      $quiz_response_conditions['Quiz.id_page'] = $pageId;
    }
    $quizResponseData = $this->Quizresponse->find('all', array(
      'fields' => array('Quizresponse.id_quiz'),
      'conditions' => $quiz_response_conditions,
      'order' => array('COUNT(*) DESC'),
      'group' => array('Quizresponse.id_quiz'),
      'limit' => 20,
    ));
    foreach($quizResponseData as $element) {
      array_push($quizzesToExclude, $element['Quizresponse']['id_quiz']);
    }

    $quiz_conditions = array(
        'Page.is_active' => 1,
        'Quiz.is_active' => 1,
    );
    if ($pageRestriction) {
      // Featured quiz on spot/page
      $quiz_conditions[] = $pageRestriction;
      $quiz_conditions[] = array('NOT' => array('Quiz.id' => $quizzesToExclude));
    } else {
      // Featured quiz on Homepage - from ALL pages (no restrictions)

      // generate array of random quizzes to display
      $lastQuiz = $this->Quiz->find('first', array('order' => array('Quiz.id DESC'), 'fields' => array('Quiz.id')));
      $maxQuizCount = $lastQuiz['Quiz']['id'];
// temporary stuff, until all pages are enabled
$maxQuizCount = Configure::read('MAX_QUIZ_FOR_FEATURED_QUIZ');

      $randomQuizzes = array();
      while (count($randomQuizzes) < 20) {
        $random_no = rand(1, $maxQuizCount);
        if (!in_array($random_no, $quizzesToExclude)) {
          $randomQuizzes[] = $random_no;
        }
      }
      $quiz_conditions['Quiz.id'] = $randomQuizzes;
    }

    /*
    // Original Logic: Enable this later. I think this logic tries to find the most responded to Quizzes that the current user hasnt answered (most popular quizzes)

    $this->Quizresponse->bindModel(array(
      'belongsTo' => array(
        'Quiz' => array('foreignKey' => false,
                'type' => 'RIGHT',
                'conditions' => array(
                  'AND' => array(
                    array('Quiz.id = Quizresponse.id_quiz'),
                    array('NOT' => array('Quiz.image_url' => "")),
                    $pageRestriction,
                )),
          ),
        ),
      ),
      false
    );

    $dbo = $this->Quizresponse->getDataSource();
    $subQuery = $dbo->buildStatement(
      array(
        'table' => $dbo->fullTableName($this->Quizresponse),
        'alias' => 'Quizresponse',
        'joins' => array(),
        'fields' => array( '`id_quiz`'),
        'order' => null,
        'group' => null,
        'limit' => null,
        'offset' => null,
        'conditions' => array(
          'AND' => array(
            array('Quizresponse.id_user' => $current_user_id),
            array('NOT' => array('Quizresponse.deleted' => 1)),
          )
        ),
      ),
      $this->Quizresponse
    );
    $subSecondQuery = '`Quiz`.`id` NOT IN (' . $subQuery . ') ';
    $subQuery = '`Quizresponse`.`id_quiz` NOT IN (' . $subQuery . ') ';

    $quizId_data = $this->Quizresponse->find('all', array(
      'fields' => array('Quizresponse.id_quiz'),
      'conditions' => array(
        'AND' => array(
          'Quizresponse.deleted' => 0,
          array($dbo->expression($subQuery)),
      )),
      'order' => array('COUNT(*) DESC'),
      'group' => array('Quizresponse.id_quiz'),
      'limit' => 3,
    ));

    $quiz_array = array();
    if (empty($quizId_data)) {
      $quizId_data = $this->Quiz->find('all', array(
        'fields' => array('Quiz.id'),
        'conditions' => array(
          'AND' => array(
            array('NOT' => array('Quiz.image_url' => "")),
            array($dbo->expression($subSecondQuery)),
            $pageRestriction,
            'Page.is_active' => 1,
            'Quiz.is_active' => 1,
          )
        ),
        'order' => 'rand()',
        'limit' => 3,
      ));
      foreach($quizId_data as &$element) {
        array_push($quiz_array, $element['Quiz']['id']);
      }
    } else {
      foreach($quizId_data as &$element) {
        array_push($quiz_array, $element['Quizresponse']['id_quiz']);
      }
    }
    // End: I think this logic tries to find the most responded to Quizzes that the current user hasnt answered (most popular quizzes). Note: 'Quiz.id' => $quiz_array is appended as a condition to Quiz->find below
    */

    /* Bind/unBind the model to reduce the amount of data sent. */
    $this->Quiz->unBindModel(array('hasMany' => array('Quizresponse')));
    $this->Quiz->bindModel(array(
      'hasMany' => array(
        'Quizchoice' => array(
          'foreignKey' => 'id_quiz',
          'fields' => array('Quizchoice.description'),
        ),
      )),
      false
    );

    $featureQuiz = $this->Quiz->find('first', array(
      'fields' => array('Page.id', 'Page.slug', 'Page.name', 'Page.thumbnail_url', 'Quiz.id', 'Quiz.slug', 'Quiz.image_url', 'Quiz.question_text'),
      'limit' => 1,
      'conditions' => $quiz_conditions,
      //'order' => 'rand()',
      'order' => 'Quiz.id ASC',
    ));

    if (!empty($featureQuiz['Quiz']['image_url'])) {
      $is_external_image = 1;
      if (strstr($featureQuiz['Quiz']['image_url'], "http://")) {
        $image_path = $featureQuiz['Quiz']['image_url'];
        $is_external_image = 1;
      } else {
        $image_path = '../webroot/img/photos/quizzes/' . $featureQuiz['Quiz']['id'] . '/' . $featureQuiz['Quiz']['image_url'];
        $is_external_image = 0;
      }
      $featureQuiz['Quiz']['is_external_image'] = $is_external_image;

      //list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
      //list($featureQuiz['Quiz']['width'], $featureQuiz['Quiz']['height']) = $this->readjustImage($im_w, $im_h, "200", "200");
      $featureQuiz['Quiz']['width'] = 200;
      $featureQuiz['Quiz']['height'] = 200;
      $featureQuiz['Quiz']['padding_width'] = (200 - $featureQuiz['Quiz']['width']) / 2;
      $featureQuiz['Quiz']['padding_height'] = (200 - $featureQuiz['Quiz']['height']) / 2;
    }

    echo json_encode(array("featureQuiz" => $featureQuiz));
    exit;
  }

  /**
	 * View a particular quiz, and allow a user to select one choice.
	 */
	function view($pageSlug = null, $quizId = null, $quizSlug = null) {
		$this->checkSlugs($pageSlug, $quizId, $quizSlug, 'view');
		$this->set('admin', $this->MyAuth->isAdmin());

		$this->initVariables($pageSlug);
		$pageId = $this->page['id'];

		if ($quizSlug == null) {
			$this->redirect($this->globalLink($pageSlug,"quizzes"));
		} else {
			$this->data = $this->Quiz->find('first', array(
				'conditions' => array(
					'AND' => array(
						'Quiz.id' => $quizId,
						'Quiz.id_page' => $pageId,
						'Page.is_active' => 1,
						'Quiz.is_active' => 1,
					)
				)
			));

			/* If quiz doesn't exists, exit with warning. */
			if (empty($this->data)) {
				$this->Session->setFlash('We\'re sorry, you entered an invalid URL.');
				$this->redirect($this->globalLink($pageSlug,"quizzes"));
			}
			$this->set('pageTitle', $this->data['Quiz']['question_text'] . " - " . $this->page['name'] . " Trivia Quiz");
			$this->set('pageDescription', $this->data['Quiz']['question_text'] . " - Try to get the correct answer to this " . $this->page['name'] . " Trivia Quiz on " . Configure::read('DOMAIN') . "! " . Configure::read('CAPTION'));

			foreach($this->data['Quizchoice'] as &$choice) {
				if (!empty($choice['image_url'])) {
					$is_external_image = 1;
					if (strstr($choice['image_url'], "http://")) {
						$image_path = $choice['image_url'];
						$is_external_image = 1;
					} else {
						$image_path = '../webroot/img/photos/quizzes/' . $this->data['Quiz']['id'] . '/' . $choice['image_url'];
						$is_external_image = 0;
					}
					$choice['is_external_image'] = $is_external_image;

/*
					list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
					list($choice['width'], $choice['height']) = $this->readjustImage($im_w, $im_h, "100", "100");
*/
					$choice['width'] = 100;
					$choice['height'] = 100;

					$choice['padding_width'] = (100 - $choice['width']) / 2;
					$choice['padding_height'] = (100 - $choice['height']) / 2;
				}
			}

			if (!empty($this->data['Quiz']['image_url'])) {
				$is_external_image = 1;
				if (strstr($this->data['Quiz']['image_url'], "http://")) {
					$image_path = $this->data['Quiz']['image_url'];
					$is_external_image = 1;
				} else {
					$image_path = '../webroot/img/photos/quizzes/' . $this->data['Quiz']['id'] . '/' . $this->data['Quiz']['image_url'];
					$is_external_image = 0;
				}
				$this->data['Quiz']['is_external_image'] = $is_external_image;

/*
				list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
				list($this->data['Quiz']['width'], $this->data['Quiz']['height']) = $this->readjustImage($im_w, $im_h, "250", "250");
*/
				$this->data['Quiz']['width'] = 250;
				$this->data['Quiz']['height'] = 250;

				$this->data['Quiz']['padding_width'] = (250 - $this->data['Quiz']['width']) / 2;
				$this->data['Quiz']['padding_height'] = (250 - $this->data['Quiz']['height']) / 2;
			}

			$this->set('quiz', $this->data);
			$user = $this->User->find('first', array(
				'conditions' => array('User.id' => $this->data['Quiz']['id_user']),
				'recursive' => 0,
			));
			$this->set('creator', $user);

			/* Check if user has already chosen this data. If so, let's set the information to response. */
			if ($this->MyAuth->user()) {
				$this->Quizresponse->data = $this->Quizresponse->find('first', array('conditions' => array('Quizresponse.id_user'=> $this->MyAuth->user('id'), 'Quizresponse.id_quiz' => $this->data['Quiz']['id'])));
				if (!empty($this->Quizresponse->data) && $this->Quizresponse->data['Quizresponse']['deleted'] != 1) {
					$this->redirect($this->globalLink($pageSlug,"quizzes/results/$quizId/$quizSlug"));
				}
			} else {
				$cookie_array = $this->getCookieData();
				if(in_array($this->data['Quiz']['id'], $cookie_array['quizId'])) {
					$this->redirect($this->globalLink($pageSlug,"quizzes/results/$quizId/$quizSlug"));
				}
			}

			/* Get the next quiz in case the user pressed it. */
			$slug = $this->getNextQuiz($pageSlug, $quizSlug);
			if ($slug == null) {
				$this->set('nextQuiz', "/complete");
			} else {
				$this->set('nextQuiz', "/view/" . $slug['Quiz']['id'] . "/" . $slug['Quiz']['slug']);
			}

			/* Check to see if user logged in with max_choices set or not */
			$cookie_array = $this->getCookieData();
			if ($cookie_array['total_num'] >= Configure::read('MAX_UNSIGNED_ENTRIES')) {
				$this->set('max_choices', 1);
			} else {
				$this->set('max_choices', 0);
			}
		}
	}

	/**
	 * This function processes the answer user has chosen and chooses the next quiz.
	 **/
	function nextQuiz($pageSlug = null, $quizId = null, $quizSlug = null)
	{
		$this->checkSlugs($pageSlug, $quizId, $quizSlug, 'nextQuiz');
		$this->set('admin', $this->MyAuth->isAdmin());

		$this->initVariables($pageSlug);
		$pageId = $this->page['id'];

		if ($quizSlug == null) {
			$this->redirect($this->globalLink($pageSlug,"quizzes"));
			return;
		}

		$this->Quiz->data = $this->Quiz->find('first', array(
			'conditions' => array(
				'AND' => array(
					'Quiz.id' => $quizId,
					'Quiz.id_page' => $pageId,
					'Page.is_active' => 1,
					'Quiz.is_active' => 1,
				)
			)
		));

		/* Check to see if user logged in with max_choices set or not */
		$cookie_array = $this->getCookieData();
		if ($cookie_array['total_num'] >= Configure::read('MAX_UNSIGNED_ENTRIES')) {
			$this->set('max_choices', 1);
		} else {
			$this->set('max_choices', 0);
		}

		/**
		 * Record results chosen by user.
		 **/
		if (!empty($this->params['form'])) {
			/* Sanity check: user selected only one choice. */
			if (count($this->params['form']) != 1) {
				$this->Session->setFlash('Error: Something bad happened. Please try again.');
			} else {

				/* Get ID of quiz choice */
				$this->Quizchoice->data = $this->Quizchoice->findAllByIdQuiz($this->Quiz->data['Quiz']['id']);
				$choiceId = $this->Quizchoice->data[key($this->params['form'])]['Quizchoice']['id'];

				/* User validation */
				if (!$this->MyAuth->user()) {
					$cookie_array = $this->getCookieData();
					if($cookie_array['total_num'] <= Configure::read('MAX_UNSIGNED_ENTRIES') && !in_array($this->Quiz->data['Quiz']['id'], $cookie_array['quizId'])) {
						$this->data = array();
						$this->data['id_quizchoice'] = $choiceId;
						$this->data['id_quiz'] = $this->Quiz->data['Quiz']['id'];
						array_push($cookie_array['quizData'], $this->data);

						array_push($cookie_array['quizId'], $this->Quiz->data['Quiz']['id']);
						$cookie_array['total_num']++;
						$this->writeCookieData($cookie_array);
						$logonmsg = '<div class="flashLogin"><a href="' . Router::url('/login/') . '">Login</a></div>';
						$createAccountmsg = '<div class="flashCreateAccount"><a href="' . Router::url('/register/') . '">Create an account</a></div>';
						$msg = 'We will remember your response. Please ' . $createAccountmsg . ' or ' . $logonmsg . ' to permanently save all your responses.';
						$this->Session->setFlash($msg);
					}

					// Copy-paste from code below. We really need to refactor this better.
					if ($this->Quizchoice->data[key($this->params['form'])]['Quizchoice']['correct']) {
						$this->set('correct', 1);
					} else {
						foreach ($this->Quizchoice->data as $element) {
							/* We assume that there's one correct answer */
							if ($element['Quizchoice']['correct']) {
								$this->set('correctAnswer', $element);
							}
						}
					}

					/* Generate random quiz entry, and go there. */
					$nextSlug = $this->getNextQuiz($pageSlug, -1);
					$this->set('prevId', $quizId);
					$this->set('prevSlug', $quizSlug);
					if ($nextSlug == null) {
						$this->Session->setFlash('You have answered all available quizzes for this page.');
						//$this->index($pageId, null);
						//$this->render("/quizzes/index");
						$this->redirect($this->globalLink($pageSlug,"quizzes"));
						return;
					}

					$nextSlug['Quiz']['total_count'] = 0;
					foreach($nextSlug['Quizchoice'] as &$choice) {
						$choice['response_count'] = 0;
					}
					foreach($nextSlug['Quizchoice'] as &$choice) {
						if (!empty($choice['image_url'])) {
							$is_external_image = 1;
							if (strstr($choice['image_url'], "http://")) {
								$image_path = $choice['image_url'];
								$is_external_image = 1;
							} else {
								$image_path = '../webroot/img/photos/quizzes/' . $nextSlug['Quiz']['id'] . '/' . $choice['image_url'];
								$is_external_image = 0;
							}
							$choice['is_external_image'] = $is_external_image;
/*
							list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
							list($choice['width'], $choice['height']) = $this->readjustImage($im_w, $im_h, "100", "100");
*/
							$choice['width'] = 100;
							$choice['height'] = 100;

							$choice['padding_width'] = (100 - $choice['width']) / 2;
							$choice['padding_height'] = (100 - $choice['height']) / 2;
						}
					}
					foreach($nextSlug['Quizresponse'] as &$response) {
						if ($response['deleted'] == 0) {
							$nextSlug['Quiz']['total_count']++;
							foreach($nextSlug['Quizchoice'] as &$choice) {
								if ($choice['id'] == $response['id_quizchoice']) {
									$choice['response_count']++;
									break;
								}
							}
						}
					}

					if (!empty($nextSlug['Quiz']['image_url'])) {
						$is_external_image = 1;
						if (strstr($nextSlug['Quiz']['image_url'], "http://")) {
							$image_path = $nextSlug['Quiz']['image_url'];
							$is_external_image = 1;
						} else {
							$image_path = '../webroot/img/photos/quizzes/' . $nextSlug['Quiz']['id'] . '/' . $nextSlug['Quiz']['image_url'];
							$is_external_image = 0;
						}
						$nextSlug['Quiz']['is_external_image'] = $is_external_image;
/*
						list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
						list($nextSlug['Quiz']['width'], $nextSlug['Quiz']['height']) = $this->readjustImage($im_w, $im_h, "250", "250");
*/
						$nextSlug['Quiz']['width'] = 250;
						$nextSlug['Quiz']['height'] = 250;

						$nextSlug['Quiz']['padding_width'] = (250 - $nextSlug['Quiz']['width']) / 2;
						$nextSlug['Quiz']['padding_height'] = (250 - $nextSlug['Quiz']['height']) / 2;
					}

					$this->set('quiz', $nextSlug);
					$user = $this->User->find('first', array(
						'conditions' => array('User.id' => $nextSlug['Quiz']['id_user']),
						'recursive' => 0,
					));
					$this->set('creator', $user);

					/* Get the next quiz in case the user pressed it. */
					$slug = $this->getNextQuiz($pageSlug, $quizSlug);
					if ($slug == null) {
						$this->set('nextQuiz', "/complete");
					} else {
						$this->set('nextQuiz', "/view/" . $slug['Quiz']['id'] . "/" . $slug['Quiz']['slug']);
					}

					$this->set('pageTitle', $nextSlug['Quiz']['question_text'] . " - " . $this->page['name'] . " Trivia Quiz");
					$this->set('pageDescription', $nextSlug['Quiz']['question_text'] . " - See if you can get the correct answer to this " . $this->page['name'] . " Trivia Quiz on " . Configure::read('DOMAIN') . "! " . Configure::read('CAPTION'));
					$this->render("/quizzes/view");
					return;
				}
				$user = $this->MyAuth->user('id');

				/* Search in the quizresponses to see whether the user has selected this quiz before. */
				$this->data = $this->Quizresponse->find('first', array('conditions' => array('Quizresponse.id_user'=> $user, 'Quizresponse.id_quiz' => $this->Quiz->data['Quiz']['id'])));
				if (!empty($this->data)) {
					if ($this->data['Quizresponse']['deleted'] == 1) {
						/* User has selected earlier. Update (remove delete) his new choice. */
						$this->data['Quizresponse']['id_quizchoice'] = $choiceId;
						unset($this->data['Quizresponse']['modified']);
						$this->data['Quizresponse']['deleted'] = 0;
					} else {
						/* User selected again (should not be here). Force create (error out there) */
						unset($this->data['Quizresponse']['id']);
					}
				} else {
					/* Create a new entry of user response. */
					$this->data['Quizresponse']['id_quiz'] = $this->Quiz->data['Quiz']['id'];
					$this->data['Quizresponse']['id_quizchoice'] = $choiceId;
					// HACK: user set to 1.
					$this->data['Quizresponse']['id_user'] = $user;

				}

				/* Find the quizchoice user made, and increment the total response by 1. */
				$this->Quizchoice->id = $choiceId;
//				$responseCount = $this->Quizchoice->data[key($this->params['form'])]['Quizchoice']['response_count'] + 1;

				/* Determine if the choice is correct, and report to page */
				if ($this->Quizchoice->data[key($this->params['form'])]['Quizchoice']['correct']) {
					$this->set('correct', 1);
				} else {
					foreach ($this->Quizchoice->data as $element) {
						/* We assume that there's one correct answer */
						if ($element['Quizchoice']['correct']) {
							$this->set('correctAnswer', $element);
						}
					}
				}

				/* Save the data */
				// TODO: Generalize this type of errors.
				if (!$this->Quizresponse->saveAll($this->data['Quizresponse'])) {
					if (count($this->Quizresponse->validationErrors)) {
						foreach ($this->Quizresponse->validationErrors as $error) {
							$this->Session->setFlash($error);
						}
					} else {
						$this->Session->setFlash('Database Error: unable to save.');
					}
				} else {
//					$this->Quizchoice->saveField('response_count', $responseCount);

					/* Generate random quiz entry, and go there. */
					$nextSlug = $this->getNextQuiz($pageSlug, -1);
					$this->set('prevId', $quizId);
					$this->set('prevSlug', $quizSlug);
					if ($nextSlug == null) {
						$this->Session->setFlash('You have answered all available quizzes for this page.');
						//$this->index($pageId, null);
						//$this->render("/quizzes/index");
						$this->redirect($this->globalLink($pageSlug,"quizzes"));
						return;
					}

					$nextSlug['Quiz']['total_count'] = 0;
					foreach($nextSlug['Quizchoice'] as &$choice) {
						$choice['response_count'] = 0;
					}
					foreach($nextSlug['Quizchoice'] as &$choice) {
						if (!empty($choice['image_url'])) {
							$is_external_image = 1;
							if (strstr($choice['image_url'], "http://")) {
								$image_path = $choice['image_url'];
								$is_external_image = 1;
							} else {
								$image_path = '../webroot/img/photos/quizzes/' . $nextSlug['Quiz']['id'] . '/' . $choice['image_url'];
								$is_external_image = 0;
							}
							$choice['is_external_image'] = $is_external_image;

/*
							list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
							list($choice['width'], $choice['height']) = $this->readjustImage($im_w, $im_h, "100", "100");
*/
							$choice['width'] = 100;
							$choice['height'] = 100;

							$choice['padding_width'] = (100 - $choice['width']) / 2;
							$choice['padding_height'] = (100 - $choice['height']) / 2;
						}
					}
					foreach($nextSlug['Quizresponse'] as &$response) {
						if ($response['deleted'] == 0) {
							$nextSlug['Quiz']['total_count']++;
							foreach($nextSlug['Quizchoice'] as &$choice) {
								if ($choice['id'] == $response['id_quizchoice']) {
									$choice['response_count']++;
									break;
								}
							}
						}
					}

					if (!empty($nextSlug['Quiz']['image_url'])) {
						$is_external_image = 1;
						if (strstr($nextSlug['Quiz']['image_url'], "http://")) {
							$image_path = $nextSlug['Quiz']['image_url'];
							$is_external_image = 1;
						} else {
							$image_path = '../webroot/img/photos/quizzes/' . $nextSlug['Quiz']['id'] . '/' . $nextSlug['Quiz']['image_url'];
							$is_external_image = 0;
						}
						$nextSlug['Quiz']['is_external_image'] = $is_external_image;
/*
						list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
						list($nextSlug['Quiz']['width'], $nextSlug['Quiz']['height']) = $this->readjustImage($im_w, $im_h, "250", "250");
*/
						$nextSlug['Quiz']['width'] = 250;
						$nextSlug['Quiz']['height'] = 250;

						$nextSlug['Quiz']['padding_width'] = (250 - $nextSlug['Quiz']['width']) / 2;
						$nextSlug['Quiz']['padding_height'] = (250 - $nextSlug['Quiz']['height']) / 2;
					}

					$this->set('quiz', $nextSlug);
					$user = $this->User->find('first', array(
						'conditions' => array('User.id' => $nextSlug['Quiz']['id_user']),
						'recursive' => 0,
					));
					$this->set('creator', $user);

					/* Get the next quiz in case the user pressed it. */
					$slug = $this->getNextQuiz($pageSlug, $quizSlug);
					if ($slug == null) {
						$this->set('nextQuiz', "/complete");
					} else {
						$this->set('nextQuiz', "/view/" . $slug['Quiz']['id'] . "/" . $slug['Quiz']['slug']);
					}

					$this->set('pageTitle', $nextSlug['Quiz']['question_text'] . " - " . $this->page['name'] . " Trivia Quiz");
					$this->set('pageDescription', $nextSlug['Quiz']['question_text'] . " - See if you can get the correct answer to this " . $this->page['name'] . " Trivia Quiz on " . Configure::read('DOMAIN') . "! " . Configure::read('CAPTION'));
					$this->render("/quizzes/view");
					return;
				}
			}
		}
		foreach($this->Quiz->data['Quizchoice'] as &$choice) {
			if (!empty($choice['image_url'])) {
				$is_external_image = 1;
				if (strstr($choice['image_url'], "http://")) {
					$image_path = $choice['image_url'];
					$is_external_image = 1;
				} else {
					$image_path = '../webroot/img/photos/quizzes/' . $this->Quiz->data['Quiz']['id'] . '/' . $choice['image_url'];
					$is_external_image = 0;
				}
				$choice['is_external_image'] = $is_external_image;

/*
				list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
				list($choice['width'], $choice['height']) = $this->readjustImage($im_w, $im_h, "100", "100");
*/
				$choice['width'] = 100;
				$choice['height'] = 100;

				$choice['padding_width'] = (100 - $choice['width']) / 2;
				$choice['padding_height'] = (100 - $choice['height']) / 2;
			}
		}

		if (!empty($this->Quiz->data['Quiz']['image_url'])) {
			$is_external_image = 1;
			if (strstr($this->Quiz->data['Quiz']['image_url'], "http://")) {
				$image_path = $this->Quiz->data['Quiz']['image_url'];
				$is_external_image = 1;
			} else {
				$image_path = '../webroot/img/photos/quizzes/' . $this->Quiz->data['Quiz']['id'] . '/' . $this->Quiz->data['Quiz']['image_url'];
				$is_external_image = 0;
			}
			$this->Quiz->data['Quiz']['is_external_image'] = $is_external_image;

/*
			list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
			list($this->Quiz->data['Quiz']['width'], $this->Quiz->data['Quiz']['height']) = $this->readjustImage($im_w, $im_h, "250", "250");
*/
			$this->Quiz->data['Quiz']['width'] = 250;
			$this->Quiz->data['Quiz']['height'] = 250;

			$this->Quiz->data['Quiz']['padding_width'] = (250 - $this->Quiz->data['Quiz']['width']) / 2;
			$this->Quiz->data['Quiz']['padding_height'] = (250 - $this->Quiz->data['Quiz']['height']) / 2;
		}

		/* Gather information about this quiz to display on webpage. */
		$this->set('pageTitle', $this->Quiz->data['Quiz']['question_text'] . " - " . $this->page['name'] . " Trivia Quiz");
		$this->set('pageDescription', $this->Quiz->data['Quiz']['question_text'] . " - See if you can get the correct answer to this " . $this->page['name'] . " Trivia Quiz on " . Configure::read('DOMAIN') . "! " . Configure::read('CAPTION'));
		$this->set('quiz', $this->Quiz->data);
		$user = $this->User->find('first', array(
			'conditions' => array('User.id' => $this->data['Quiz']['id_user']),
			'recursive' => 0,
		));
		$this->set('creator', $user);

		$this->redirect($this->globalLink($pageSlug,"quizzes/view/$quizId/$quizSlug"));

		/* Get the next quiz in case the user pressed it. */
		$slug = $this->getNextQuiz($pageSlug, $quizSlug);
		if ($slug == null) {
			$this->set('nextQuiz', "/complete");
		} else {
			$this->set('nextQuiz', "/view/" . $slug['Quiz']['id'] . "/" . $slug['Quiz']['slug']);
		}

	}

	/**
	 * This function looks and finds a quiz that the user hasn't answered yet.
	 * @ The quiz entry if there is one.
	 * @ null if user answered all quizzes.
	 **/
	function getNextQuiz($pageSlug = null, $quizSlug = null)
	{
		// TODO: It's possible that there's one quiz left unanswered and user could end up not seeing another quiz, and go back to index. Fix it.
		// TODO: What's the case when all quizzes have been answered? Are they handled properly? (I don't think so).
		$this->initVariables($pageSlug);
		$pageId = $this->page['id'];

		$quizId = $this->slugToId($pageId, $quizSlug);
		if (!$this->page) {
			return null;
		} else if ($quizId == 0) {
			$quizId = -1;
		}

		/* If user has any quiz responses in cookies, we should check it out here. Only if user isn't logged in. */
		if (!$this->MyAuth->user()) {
			$cookie_array = $this->getCookieData();
			array_push($cookie_array['quizId'], $quizId);
			$quizId = $cookie_array['quizId'];
		}

		$dbo = $this->Quizresponse->getDataSource();
		$subQuery = $dbo->buildStatement(
			array(
				'table' => $dbo->fullTableName($this->Quizresponse),
				'alias' => 'Quizresponse',
				'joins' => array(),
				'fields' => array( '`id_quiz`'),
				'order' => null,
				'group' => null,
				'limit' => null,
				'offset' => null,
				'conditions' => array(
					'AND' => array(
						array('Quizresponse.id_user' => $this->MyAuth->user('id')),
						array('NOT' => array('Quizresponse.deleted' => 1)),
					)
				),
			),
			$this->Quizresponse
		);
		$subQuery = '`Quiz`.`id` NOT IN(' . $subQuery . ') ';

		$quiz_data = $this->Quiz->find('first', array(
			'conditions' => array(
				'AND' => array(
					array($dbo->expression($subQuery)),
					array('NOT' => array('Quiz.id' => $quizId)),
					'Page.id' => $pageId,
					'Page.is_active' => 1,
					'Quiz.is_active' => 1,
				)
			),
			'limit' => 1,
			'order' => 'rand()'
		));
		return $quiz_data;
	}

	/**
	 * This function finds a random quiz that the user hasn't answered and chooses it.
	 **/
	function next_random_quiz($pageSlug = null, $current_quizSlug = null)
	{
		$this->checkSlugs($pageSlug, $quizSlug, 'next_random_quiz');
		$slug = $this->getNextQuiz($pageSlug, $current_quizSlug);

		if ($slug == null) {
			$this->Session->setFlash('You have selected all available quizzes for this page.');
			$this->redirect($this->globalLink($pageSlug,"quizzes"));
		}
		$this->redirect($this->globalLink($pageSlug,"quizzes/view/" . $slug['Quiz']['slug']));
	}

	/**
	 * This is just a simple function that redirects and says that the user completed all quizzes.
	 **/
	function complete($pageSlug = null, $quizSlug = null)
	{
		$this->Session->setFlash('You have selected all available quizzes for this page.');
		$this->redirect($this->globalLink($pageSlug,"quizzes"));
	}

	function results($pageSlug = null, $quizId = null, $quizSlug = null)
	{
		$this->checkSlugs($pageSlug, $quizId, $quizSlug, 'results');
		$this->set('admin', $this->MyAuth->isAdmin());

		$this->initVariables($pageSlug);
		$pageId = $this->page['id'];

		if ($quizSlug == null) {
			$this->redirect($this->globalLink($pageSlug,"quizzes"));
			return;
		}

		/* Gather information about this quiz to display on webpage. */
		$this->Quiz->data = $this->Quiz->find('first', array(
			'conditions' => array(
				'AND' => array(
					'Quiz.id' => $quizId,
					'Quiz.id_page' => $pageId,
					'Page.is_active' => 1,
					'Quiz.is_active' => 1,
				)
			)
		));

		/* If quiz doesn't exists, exit with warning. */
		if (empty($this->Quiz->data)) {
			$this->Session->setFlash('We\'re sorry, you entered an invalid URL.');
			$this->redirect($this->globalLink($pageSlug,"quizzes"));
		}

		$this->set('pageTitle', "Results for: " . $this->Quiz->data['Quiz']['question_text'] . " - " . $this->page['name'] . " Trivia Quiz");
		$this->set('pageDescription', "Results for: " . $this->Quiz->data['Quiz']['question_text'] . " - " . $this->page['name'] . " Trivia Quiz on " . Configure::read('DOMAIN') . "! " . Configure::read('CAPTION'));

		$this->Quiz->data['Quiz']['total_count'] = 0;
		foreach($this->Quiz->data['Quizchoice'] as &$choice) {
			$choice['response_count'] = 0;
		}
		foreach($this->Quiz->data['Quizchoice'] as &$choice) {
			if (!empty($choice['image_url'])) {
				$is_external_image = 1;
				if (strstr($choice['image_url'], "http://")) {
					$image_path = $choice['image_url'];
					$is_external_image = 1;
				} else {
					$image_path = '../webroot/img/photos/quizzes/' . $this->Quiz->data['Quiz']['id'] . '/' . $choice['image_url'];
					$is_external_image = 0;
				}
				$choice['is_external_image'] = $is_external_image;

/*
				list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
				list($choice['width'], $choice['height']) = $this->readjustImage($im_w, $im_h, "100", "100");
*/
				$choice['width'] = 100;
				$choice['height'] = 100;

				$choice['padding_width'] = (100 - $choice['width']) / 2;
				$choice['padding_height'] = (100 - $choice['height']) / 2;
			}
		}
		foreach($this->Quiz->data['Quizresponse'] as &$response) {
			if ($response['deleted'] == 0) {
				$this->Quiz->data['Quiz']['total_count']++;
				foreach($this->Quiz->data['Quizchoice'] as &$choice) {
					if ($choice['id'] == $response['id_quizchoice']) {
						$choice['response_count']++;
						break;
					}
				}
			}
		}

		if (!empty($this->Quiz->data['Quiz']['image_url'])) {
			$is_external_image = 1;
			if (strstr($this->Quiz->data['Quiz']['image_url'], "http://")) {
				$image_path = $this->Quiz->data['Quiz']['image_url'];
				$is_external_image = 1;
			} else {
				$image_path = '../webroot/img/photos/quizzes/' . $this->Quiz->data['Quiz']['id'] . '/' . $this->Quiz->data['Quiz']['image_url'];
				$is_external_image = 0;
			}
			$this->Quiz->data['Quiz']['is_external_image'] = $is_external_image;

/*
			list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
			list($this->Quiz->data['Quiz']['width'], $this->Quiz->data['Quiz']['height']) = $this->readjustImage($im_w, $im_h, "250", "250");
*/
			$this->Quiz->data['Quiz']['width'] = 250;
			$this->Quiz->data['Quiz']['height'] = 250;

			$this->Quiz->data['Quiz']['padding_width'] = (250 - $this->Quiz->data['Quiz']['width']) / 2;
			$this->Quiz->data['Quiz']['padding_height'] = (250 - $this->Quiz->data['Quiz']['height']) / 2;
		}
		$user = $this->User->find('first', array(
			'conditions' => array('User.id' => $this->Quiz->data['Quiz']['id_user']),
			'recursive' => 0,
		));
		$this->set('creator', $user);

		/**
		 * Get the user's choice if he selected an answer.
		 **/
		if ($this->MyAuth->user()) {
			$this->Quizresponse->data = $this->Quizresponse->find('first', array('conditions' => array('Quizresponse.id_user'=> $this->MyAuth->user('id'), 'Quizresponse.id_quiz' => $this->Quiz->data['Quiz']['id'])));
			if (empty($this->Quizresponse->data) && $this->Quizresponse->data['Quizresponse']['deleted'] == 1) {
				$this->redirect($this->globalLink($pageSlug,"quizzes/view/$quizId/$quizSlug"));
			}

			/* Determine if user selected the correct answer. */
			$this->set('userChoice', $this->Quizresponse->data);
			foreach($this->Quiz->data['Quizchoice'] as $element) {
				if ($element['id'] == $this->Quizresponse->data['Quizresponse']['id_quizchoice']) {
					if ($element['correct']) {
						$this->set('correct', 1);
						break;
					}
				}
			}
		} else {
			/* User isn't logged in. Check cookie information, and load response. If not found, redirect to view. */
			$cookie_array = $this->getCookieData();
			if(!in_array($this->Quiz->data['Quiz']['id'], $cookie_array['quizId'])) {
				$this->redirect($this->globalLink($pageSlug,"quizzes/view/$quizId/$quizSlug"));
			} else {
				$key = array_search($this->Quiz->data['Quiz']['id'], $cookie_array['quizId']);
				$this->data = array();
				$this->data['Quizresponse']['deleted'] = 0;
				$this->data['Quizresponse']['id_quizchoice'] = $cookie_array['quizData'][$key]['id_quizchoice'];
				$this->set('userChoice', $this->data);
				$this->set('extra_cookie', 1);

				foreach($this->Quiz->data['Quizchoice'] as &$choice) {
					if ($choice['id'] == $cookie_array['quizData'][$key]['id_quizchoice']) {
						$choice['response_count']++;
						break;
					}
				}
				$this->Quiz->data['Quiz']['total_count']++;
			}
		}
		$this->set('quiz', $this->Quiz->data);

		/* Get the next quiz in case the user pressed it. */
		$slug = $this->getNextQuiz($pageSlug, $quizSlug);
		if ($slug == null) {
			$this->set('nextQuiz', "/complete");
		} else {
			$this->set('nextQuiz', "/view/" . $slug['Quiz']['id'] . "/" . $slug['Quiz']['slug']);
		}
	}

	/**
	 * AJAX request for finding out more quizzes. We return 5 right now.
	 **/
	 // TODO: make it login proof (possible if user isn't logged in).
	function getMoreQuizzes($pageSlug = null)
	{
		if (!$this->isAjax) {
			die();
		}

		$pageId = $this->Page->slugToId($pageSlug);
		if ($pageId == 0) {
			exit;
		}

		$current_quizSlug = $this->params['url']['slug'];
		$current_quizId = $this->slugToId($pageId, $current_quizSlug);
		$current_user = $this->MyAuth->user();

		$dbo = $this->Quizresponse->getDataSource();
		$subQuery = $dbo->buildStatement(
			array(
				'table' => $dbo->fullTableName($this->Quizresponse),
				'alias' => 'Quizresponse',
				'joins' => array(),
				'fields' => array( '`id_quiz`'),
				'order' => null,
				'group' => null,
				'limit' => null,
				'offset' => null,
				'conditions' => array(
					'AND' => array(
						array('Quizresponse.id_user' => $this->MyAuth->user('id')),
						array('NOT' => array('Quizresponse.deleted' => 1)),
					)
				),
			),
			$this->Quizresponse);
		// TODO: figure out how to do NOT IN with subquery... cakephp doesn't seem to like this.
		$subQuery = '`Quiz`.`id` NOT IN (' . $subQuery . ') ';

		$quiz_data = $this->Quiz->find('all', array(
				'conditions' => array(
					'AND' => array(
						array('NOT' => array('Quiz.id' => "$current_quizId")),
						array($dbo->expression($subQuery)),
						'Quiz.id_page' => $pageId,
						'Page.is_active' => 1,
						'Quiz.is_active' => 1,
					)
				),
				'limit' => 5,
				'fields' => array('Quiz.id', 'Quiz.question_text', 'Quiz.slug')
			));
		echo json_encode(array("data" => $quiz_data));
		exit;
	}

	/**
	 * AJAX request for finding quiz stats from current user.
	 **/
	function getQuizStat($pageSlug = null)
	{
		if (!$this->isAjax) {
			die();
		}

		$pageId = $this->Page->slugToId($pageSlug);
		if ($pageId == 0) {
			exit;
		}

		$localQuizCreatedByAll = $this->Quiz->find('count', array('recursive' => 0, 'conditions' => array('Quiz.id_page' => $pageId, 'Page.is_active' => 1, 'Quiz.is_active' => 1)));
		$globalQuizCreatedByAll = $this->Quiz->find('count', array('recursive' => 0, 'conditions' => array('Page.is_active' => 1, 'Quiz.is_active' => 1)));

		//$localQuizCreatedByMe = $this->Quiz->find('count', array('recursive' => 0, 'conditions' => array('Quiz.id_user' => $this->MyAuth->user('id'), 'Quiz.id_page' => $pageId, 'Page.is_active' => 1, 'Quiz.is_active' => 1)));
		//$globalQuizCreatedByMe = $this->Quiz->find('count', array('recursive' => 0, 'conditions' => array('Quiz.id_user' => $this->MyAuth->user('id'), 'Page.is_active' => 1, 'Quiz.is_active' => 1)));

		/* Check to see if user is logged in. */
		if ($this->MyAuth->user()) {
			//$current_quizId = $this->params['url']['id'];
			//$current_user = $this->MyAuth->user();

			$dbo = $this->Quizresponse->getDataSource();
			$subQuery = $dbo->buildStatement(
				array(
					'table' => $dbo->fullTableName($this->Quizresponse),
					'alias' => 'Quizresponse',
					'joins' => array(),
					'fields' => array( '`id_quizchoice`'),
					'order' => null,
					'group' => null,
					'limit' => null,
					'offset' => null,
					'conditions' => array(
						'AND' => array(
							array('Quizresponse.id_user' => $this->MyAuth->user('id')),
							array('NOT' => array('Quizresponse.deleted' => 1)),
						)
					),
				),
				$this->Quizresponse
			);
			$subQuery = '`Quizchoice`.`id` IN (' . $subQuery . ') ';

			$quiz_data = $this->Quizchoice->find('all', array(
							'conditions' => array(
								'AND' => array(
									'Quizchoice.correct' => '1',
									array($dbo->expression($subQuery)),
								)
							),
							'fields' => array('Quizchoice.id_quiz')
					));

			$quizAllCorrect = array();
			foreach($quiz_data as $element) {
				array_push($quizAllCorrect, $element['Quizchoice']['id_quiz']);
			}

			$quizCorrect = $this->Quiz->find('count', array(
					'conditions' => array(
						'AND' => array(
							'Quiz.id' => $quizAllCorrect,
							'Quiz.id_page' => $pageId,
							'Page.is_active' => 1,
							'Quiz.is_active' => 1,
						)
					)
				));

			$subQuery = $dbo->buildStatement(
				array(
					'table' => $dbo->fullTableName($this->Quizresponse),
					'alias' => 'Quizresponse',
					'joins' => array(),
					'fields' => array( '`id_quiz`'),
					'order' => null,
					'group' => null,
					'limit' => null,
					'offset' => null,
					'conditions' => array(
						'AND' => array(
							array('NOT' => array('Quizresponse.deleted' => 1)),
							'Quizresponse.id_user' => $this->MyAuth->user('id'),
						)
					),
				),
				$this->Quizresponse
			);
			$subQuery = '`Quiz`.`id` NOT IN (' . $subQuery . ') ';
			$numTotalQuiz = $this->Quiz->find('count', array(
					'conditions' => array(
						'AND' => array(
							array($dbo->expression($subQuery)),
							'Quiz.id_page' => $pageId,
							'Page.is_active' => 1,
							'Quiz.is_active' => 1,
						)
					)
				));

			echo json_encode(array(
				//"localQuizCreatedByMe" => $localQuizCreatedByMe,
				//"globalQuizCreatedByMe" => $globalQuizCreatedByMe,
				"localQuizCorrect" => $quizCorrect,
				"globalQuizCorrect" => count($quizAllCorrect),
				"localQuizUnanswer" => $numTotalQuiz,
				"localQuizCreatedByAll" => $localQuizCreatedByAll,
				"globalQuizCreatedByAll" => $globalQuizCreatedByAll
			));
		} else {
			$cookie_array = $this->getCookieData();
			$quizUserResponse = array();
			foreach($cookie_array['quizData'] as $element) {
				array_push($quizUserResponse, $element['id_quizchoice']);
			}

			$quiz_data = $this->Quizchoice->find('all', array(
							'conditions' => array(
								'AND' => array(
									'Quizchoice.correct' => '1',
									'Quizchoice.id' => $quizUserResponse,
								)
							),
							'fields' => array('Quizchoice.id_quiz')
					));

			$quizAllCorrect = array();
			foreach($quiz_data as $element) {
				array_push($quizAllCorrect, $element['Quizchoice']['id_quiz']);
			}

			$quizCorrect = $this->Quiz->find('count', array(
					'conditions' => array(
						'AND' => array(
							'Quiz.id' => $quizAllCorrect,
							'Quiz.id_page' => $pageId,
							'Page.is_active' => 1,
							'Quiz.is_active' => 1,
						)
					)
				));

			$subQuery = $this->Quizchoice->find('all', array(
							'conditions' => array(
								'AND' => array(
									'Quizchoice.id' => $quizUserResponse,
								)
							),
							'fields' => array('Quizchoice.id_quiz')
					));

			$quizAllWrong = array();
			foreach($subQuery as $element) {
				array_push($quizAllWrong, $element['Quizchoice']['id_quiz']);
			}

			$numTotalQuiz = $this->Quiz->find('count', array(
					'conditions' => array(
						'AND' => array(
							array('NOT' => array('Quiz.id' => $quizAllWrong)),
							'Quiz.id_page' => $pageId,
							'Quiz.is_active' => 1,
							'Page.is_active' => 1,
						)
					)
				));

			echo json_encode(array(
				//"localQuizCreatedByMe" => $localQuizCreatedByMe,
				//"globalQuizCreatedByMe" => $globalQuizCreatedByMe,
				"localQuizCorrect" => $quizCorrect,
				"globalQuizCorrect" => count($quizAllCorrect),
				"localQuizUnanswer" => $numTotalQuiz,
				"localQuizCreatedByAll" => $localQuizCreatedByAll,
				"globalQuizCreatedByAll" => $globalQuizCreatedByAll
			));
		}
		exit;
	}

	/**
	 * AJAX call that gets one random quiz for user to choose (one that he hasn't chosen yet)
	 **/
	function getFeatureQuiz($pageSlug = null)
	{
		if (!$this->isAjax) {
			die();
		}

		// Grab one quiz that has a picture, and user has not answered yet.
		// We should grab the most popular one, and randomize it to choose one.
		$current_user_id = $this->MyAuth->user('id');
		$pageId = $this->Page->slugToId($pageSlug);
		if ($pageId == 0) {
			$pageRestriction = null;
		} else {
			$pageRestriction = array('Quiz.id_page' => "$pageId");
		}

		$quiz_response_conditions = array(
			'Quizresponse.id_user' => $current_user_id,
			'Quizresponse.deleted' => 0,
		);

		// Find quizzes the current user has already responded to, so they're excluded.
		$quizzesToExclude = array();
		if ($pageRestriction) {
			$quiz_response_conditions['Quiz.id_page'] = $pageId;
		}
		$quizResponseData = $this->Quizresponse->find('all', array(
			'fields' => array('Quizresponse.id_quiz'),
			'conditions' => $quiz_response_conditions,
			'order' => array('COUNT(*) DESC'),
			'group' => array('Quizresponse.id_quiz'),
			'limit' => 20,
		));
		foreach($quizResponseData as $element) {
			array_push($quizzesToExclude, $element['Quizresponse']['id_quiz']);
		}

		$quiz_conditions = array(
				'Page.is_active' => 1,
				'Quiz.is_active' => 1,
		);
		if ($pageRestriction) {
			// Featured quiz on spot/page
			$quiz_conditions[] = $pageRestriction;
			$quiz_conditions[] = array('NOT' => array('Quiz.id' => $quizzesToExclude));
		} else {
			// Featured quiz on Homepage - from ALL pages (no restrictions)

			// generate array of random quizzes to display
			$lastQuiz = $this->Quiz->find('first', array('order' => array('Quiz.id DESC'), 'fields' => array('Quiz.id')));
			$maxQuizCount = $lastQuiz['Quiz']['id'];
// temporary stuff, until all pages are enabled
$maxQuizCount = Configure::read('MAX_QUIZ_FOR_FEATURED_QUIZ');

			$randomQuizzes = array();
			while (count($randomQuizzes) < 20) {
				$random_no = rand(1, $maxQuizCount);
				if (!in_array($random_no, $quizzesToExclude)) {
					$randomQuizzes[] = $random_no;
				}
			}
			$quiz_conditions['Quiz.id'] = $randomQuizzes;
		}

		/*
		// Original Logic: Enable this later. I think this logic tries to find the most responded to Quizzes that the current user hasnt answered (most popular quizzes)

		$this->Quizresponse->bindModel(array(
			'belongsTo' => array(
				'Quiz' => array('foreignKey' => false,
								'type' => 'RIGHT',
								'conditions' => array(
									'AND' => array(
										array('Quiz.id = Quizresponse.id_quiz'),
										array('NOT' => array('Quiz.image_url' => "")),
										$pageRestriction,
								)),
					),
				),
			),
			false
		);

		$dbo = $this->Quizresponse->getDataSource();
		$subQuery = $dbo->buildStatement(
			array(
				'table' => $dbo->fullTableName($this->Quizresponse),
				'alias' => 'Quizresponse',
				'joins' => array(),
				'fields' => array( '`id_quiz`'),
				'order' => null,
				'group' => null,
				'limit' => null,
				'offset' => null,
				'conditions' => array(
					'AND' => array(
						array('Quizresponse.id_user' => $current_user_id),
						array('NOT' => array('Quizresponse.deleted' => 1)),
					)
				),
			),
			$this->Quizresponse
		);
		$subSecondQuery = '`Quiz`.`id` NOT IN (' . $subQuery . ') ';
		$subQuery = '`Quizresponse`.`id_quiz` NOT IN (' . $subQuery . ') ';

		$quizId_data = $this->Quizresponse->find('all', array(
			'fields' => array('Quizresponse.id_quiz'),
			'conditions' => array(
				'AND' => array(
					'Quizresponse.deleted' => 0,
					array($dbo->expression($subQuery)),
			)),
			'order' => array('COUNT(*) DESC'),
			'group' => array('Quizresponse.id_quiz'),
			'limit' => 3,
		));

		$quiz_array = array();
		if (empty($quizId_data)) {
			$quizId_data = $this->Quiz->find('all', array(
				'fields' => array('Quiz.id'),
				'conditions' => array(
					'AND' => array(
						array('NOT' => array('Quiz.image_url' => "")),
						array($dbo->expression($subSecondQuery)),
						$pageRestriction,
						'Page.is_active' => 1,
						'Quiz.is_active' => 1,
					)
				),
				'order' => 'rand()',
				'limit' => 3,
			));
			foreach($quizId_data as &$element) {
				array_push($quiz_array, $element['Quiz']['id']);
			}
		} else {
			foreach($quizId_data as &$element) {
				array_push($quiz_array, $element['Quizresponse']['id_quiz']);
			}
		}
		// End: I think this logic tries to find the most responded to Quizzes that the current user hasnt answered (most popular quizzes). Note: 'Quiz.id' => $quiz_array is appended as a condition to Quiz->find below
		*/

		/* Bind/unBind the model to reduce the amount of data sent. */
		$this->Quiz->unBindModel(array('hasMany' => array('Quizresponse')));
		$this->Quiz->bindModel(array(
			'hasMany' => array(
				'Quizchoice' => array(
					'foreignKey' => 'id_quiz',
					'fields' => array('Quizchoice.description'),
				),
			)),
			false
		);

		$featureQuiz = $this->Quiz->find('first', array(
			'fields' => array('Page.id', 'Page.slug', 'Page.name', 'Page.thumbnail_url', 'Quiz.id', 'Quiz.slug', 'Quiz.image_url', 'Quiz.question_text'),
			'limit' => 1,
			'conditions' => $quiz_conditions,
			//'order' => 'rand()',
			'order' => 'Quiz.id ASC',
		));

		if (!empty($featureQuiz['Quiz']['image_url'])) {
			$is_external_image = 1;
			if (strstr($featureQuiz['Quiz']['image_url'], "http://")) {
				$image_path = $featureQuiz['Quiz']['image_url'];
				$is_external_image = 1;
			} else {
				$image_path = '../webroot/img/photos/quizzes/' . $featureQuiz['Quiz']['id'] . '/' . $featureQuiz['Quiz']['image_url'];
				$is_external_image = 0;
			}
			$featureQuiz['Quiz']['is_external_image'] = $is_external_image;

			//list($im_w, $im_h, $type, $attr) = getimagesize($image_path);
			//list($featureQuiz['Quiz']['width'], $featureQuiz['Quiz']['height']) = $this->readjustImage($im_w, $im_h, "200", "200");
			$featureQuiz['Quiz']['width'] = 200;
			$featureQuiz['Quiz']['height'] = 200;
			$featureQuiz['Quiz']['padding_width'] = (200 - $featureQuiz['Quiz']['width']) / 2;
			$featureQuiz['Quiz']['padding_height'] = (200 - $featureQuiz['Quiz']['height']) / 2;
		}

		echo json_encode(array("featureQuiz" => $featureQuiz));
		exit;
	}
}
