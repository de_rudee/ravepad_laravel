<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!-- Google Site Verification -->
  <meta name="google-site-verification" content="csd82rXZnd58ZrDwob6H571SiC106vbgVL7MltE5DWQ" />
  <!-- Bing Site Verification -->
  <meta name="msvalidate.01" content="79F1B8B26B6C979DA64AB9CD49F7AFF1" />

  <title>
  @yield('title')
  </title>
  <META HTTP-EQUIV="Content-Language" CONTENT="EN" />
  <META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8" />
  <META HTTP-EQUIV="X-UA-Compatible" content="IE=edge,chrome=1" />
  @yield('seo')
  <META NAME="author" content="{{ config('app.name') }}" />
  <META NAME="viewport" content="width=device-width, initial-scale=1.0" />
  <META NAME="CLASSIFICATION" CONTENT="Opinions, Recommendations" />
  <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
  <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE" />
  <META NAME="application-name" content="RavePad" />
  <META NAME="msapplication-tooltip" content="RavePad" />

  <link href={{asset("favicon.ico")}} type="image/x-icon" rel="icon" /><link href={{asset("favicon.ico")}} type="image/x-icon" rel="shortcut icon" />
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300|Pacifico" />
  <link rel="stylesheet" type="text/css" href={{asset("css/lib/fonts/all.css")}} />
  <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
  <link rel="stylesheet" type="text/css" href={{asset("css/header/header.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/header/login_widget/front.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/footer/footer_style.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/jquery/jqModal/jqModal.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/jquery/tinycircleslider/tinycircleslider.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/bubble_tooltips/bt.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/jquery.jgrowl.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/site.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/validation.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/lib/ui-custom-theme/jquery-ui.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/users/feedback.css")}} />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js" type="text/javascript"></script>

  <script type="text/javascript" src={{asset("js/jquery/jqModal/jqModal.js")}}></script>
  <script type="text/javascript" src={{asset("js/jquery/jqModal/jqModalCustom.js")}}></script>
  <script type="text/javascript" src={asset{("js/jquery/jquery.jgrowl_1.2.6_minimized.js")}}></script><script type="text/javascript" src="js/jquery/jquery.cookie.js"></script>
  <script type="text/javascript" src={{asset("js/jquery/jquery.form.js")}}></script>
  <script type="text/javascript" src={{asset("js/jquery/jquery.form.custom.js")}}></script>

  <script type="text/javascript" src={{asset("js/jquery/jquery.validation.js")}}></script>
  <script type="text/javascript" src={{asset("js/jquery/jquery.validation.methods.js")}}></script>
  <script type="text/javascript" src={{asset("js/jquery/validation.custom.js")}}></script>
  <script type="text/javascript" src={{asset("js/tipsy/jquery.tipsy.js")}}></script>
  <script type="text/javascript" src={{asset("js/jquery/tinycircleslider/jquery.tinycircleslider.min.js")}}></script>
  <script type="text/javascript" src={{asset("js/eventManager.js")}}></script>
  <script type="text/javascript" src={{asset("js/helper.funcs.js")}}></script>
  <script type="text/javascript" src={{asset("js/onload/addonload.js")}}></script>
  <link rel="stylesheet" type="text/css" href={{asset("css/infos/what_is.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/quizzes/quiz_feature.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/pages/page_suggestion.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/images/images_recent.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/widgets.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/pages/topCelebs.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/tipsy/tipsy.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/pages/spotlight.css")}} />
  <link rel="stylesheet" type="text/css" href={{asset("css/picks/picks_interesting.css")}} />
  <!-- Add Google Page-level ads to all pages -->
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-4512674464486566",
    enable_page_level_ads: true
  });
  </script>
  <!-- End: Add Google Page-level ads to all pages -->
@yield('headScripts')
</head>
<body>
  <div id="shadedOverlay" style="display:none;"></div>
  <div id="loader" style="display:none;"></div>

  <div class="onepcssgrid-1200">
    <div class="onerow">
      <div class="col12">
        <div id="topContainer">
          @if (isset($showActivationBar) && ($showActivationBar == 1))
            @include('partials.activationbar')
          @endif

          <div id="topBar"></div>
          <div id="header">
            @include('partials.header')
          </div>
          <div id="mainpart">
            <div id="content">
              @if ((isset($showCrumbs)) && $showCrumbs)
                <div id="breadcrumbs"><?php echo $this->Html->getCrumbs('&nbsp;&nbsp;&middot;&nbsp;&nbsp;', $domain_name); ?></div>
              @endif

              @if (config('app.showBannerAdTop'))
                <div class="ad_leaderboard_728_90">
                  @include('partials.ads.ad_leaderboard_728_90')
                </div>
              @endif

              @if((isset($showBanner)) && $showBanner)
                @include('partials.menu')
              @endif

              <div class="contentContainer">
                <?php $search_type=1; ?>
                @if($search_type == 1)
                  <!-- // Google Custom Search -->
                  <div id="searchResultsContainer" style="display:none;">
                    <div id="searchResultsTitle">Your Search Results</div>
                    <a id="searchResultsClose" class="fa fa-times"></a>
                    <!-- Place this tag where you want the search results to render -->
                    <gcse:searchresults></gcse:searchresults>
                  </div>
                @endif
                <div id="innerContentContainer">
                  @yield('content')
                </div>

              </div>

            </div>
          </div>

          <div class="clear"></div>
          @if (config('app.showBannerAdBottom'))
            <div class="ad_leaderboard_728_90">
              @include('partials.ads.ad_leaderboard_728_90_bluehost')
            </div>
          @endif

          <!--
          <div class="scrollup">:</div>
        -->
        <div id="footer">
          @include('partials.footer')
        </div>
      </div>
    </div>
  </div>
</div>
@if(Session::has('Message.flash'))
    <?php
    $fash= Session::get('Message.flash');
    $request->session()->forget('Master.flash');
    ?>

  <script type="text/javascript">
  // life is the number of seconds to show flash/error messages for
  // $.jGrowl('<?php echo $javascript->escapeString($flash['message']) ?>', {theme: '<?php echo $flash['element'] ?>', life:15000});
  </script>

@endif



<script type="text/javascript">
var webBaseUrl = '/';
var absolutePath = "{{base_path()}}";

/*
* For Ajax requests, show the loading spinner icon
*/
// Shows spinner when loading static pages (and making ajax requests - but that is buggy)
$.ajaxSetup({
  beforeSend: function() {
    $('#loader').show();
  },
  complete: function(){
    $('#loader').hide();
  },
  success: function() {}
});
// Shows spinner when making ajax requests
$(document).ready(function(){
  $("#loader").bind("ajaxSend", function() {
    $(this).show();
  }).bind("ajaxStop", function() {
    $(this).hide();
  }).bind("ajaxError", function() {
    $(this).hide();
  });
});

/*
* Push an event to Google Analytics
*/
$.trackGAEvent = function(category, action, opt_label, opt_value, opt_noninteraction) {
  // <?php
  // if ($isProduction) {
  //   ?>
  //   // Example:
  //   //_gaq.push(['_setAccount', 'UA-1097991-13']);
  //   //_gaq.push(['_trackEvent', 'FeaturedQuiz', 'Image.click', 'Featured Quiz - Image was clicked']);
  //
  //   _gaq.push(['_trackEvent', category, action, opt_label, opt_value, opt_noninteraction]);
  //
  //   <?php
  // }
  // ?>
};
addLoadEvnt(function() {
  enableTooltips('footer', 0.87, false);
});

var searchType = <?php echo $search_type; ?>;
var navShown = 1;
var flameColor = 0;

$(document).ready(function() {

  /**** START HEADER JAVASCRIPT ****/
  $('.key').tipsy({gravity: 'n', fade: true, html: true, title: function() { return "Log in"; }});
  $('.flame').tipsy({gravity: 'n', fade: true, html: true, title: function() { return "What's Hot?"; }});
  $('.bulb').tipsy({gravity: 'n', fade: true, html: true, title: function() { return "Have an Idea? Found a problem? Leave your Feedback!"; }});
  $('.cart').tipsy({gravity: 'n', fade: true, html: true, title: function() { return "See what's new at the Ravepad tee store!"; }});

  (function flickerFlame() {
    if (flameColor == 0) {
      $('.fa-fire').addClass('flickerColor');
      flameColor = 1;
    } else {
      $('.fa-fire').removeClass('flickerColor');
      flameColor = 0;
    }
    setTimeout(flickerFlame, 4000);
  })();

  $('#feedbackButton').tipsy({gravity: 'n', fade: true, html: true, title: function() { return "Have an Idea? Found a problem? Leave your Feedback!"; }});
  $('#feedbackDialog').dialogOverlay({
    trigger:'#feedbackButton',
    effect:'slide',
    opacity:'1'
  });

  $('#joinDialog').dialogOverlay({
    trigger:'#joinButton',
    effect:'slide',
    showCallBackFn:$.joinDialogShowCallback,
    hideCallBackFn:$.joinDialogCloseCallback,
    overlayClass:'blackOverlay'
  });
  $('#joinDialog').jqmAddTrigger('.da-link');

  $('#loginDialog').dialogOverlay({
    trigger:'.loginLinkDiv',
    effect:'slide',
    showCallBackFn:$.loginDialogShowCallback,
    hideCallBackFn:$.loginDialogCloseCallback,
    overlayClass:'blackOverlay'
  });
  //$('#loginDialog').jqmAddTrigger('.loginLinkDiv');

  $('#aboutDialog').dialogOverlay({
    trigger:'#aboutTrigger',
    effect:'slide',
    overlayClass:'blackOverlay'
  });
  $('#aboutDialog').jqmAddClose('.da-link');

  $(".cart").click(function(e) {
    window.location.href = "http://ravepad.spreadshirt.com/";
  });

  $(".signin").click(function(e) {
    e.preventDefault();
    $("#signin_menu").toggle();
    $(".signin").toggleClass("menu-open");
  });

  $("#signin_menu").mouseup(function() {
    return false;
  });

  $(document).mouseup(function(e) {
    if ($(e.target).parent("a.signin").length==0) {
      $(".signin").removeClass("menu-open");
      $("#signin_menu").hide();
    }
  });

  if (searchType == 1) {
    var extraWidth = 150;
    $('#gsc-i-id1').live('focus',function() {
      $.showSearchResults();
      if (navShown) {
        $.hideNavOptions();
        $("#gsc-i-id1").animate({width:($("#gsc-i-id1").width()+extraWidth)+'px'},300);
        $("#googSearch").animate({width:($("#googSearch").width()+extraWidth)+'px'},300);
      }
    }).live('blur',function() {
      if (!navShown) {
        // There is nothing in the search input field, hide the search results box.
        var searchInput = $("#gsc-i-id1").val();
        if (searchInput.length <= 0) {
          $.hideSearchResults();
        }

        // Google makes the search results invisible, hide the search results box. This wont work if the user clicks directly on the search button. Note: There seems to be no way to detect clicks on the search button.
        if ($("#___gcse_1").has("div.gsc-tabsAreaInvisible").length) {
          //$.hideSearchResults();
        }
        $("#gsc-i-id1").animate({width:($("#gsc-i-id1").width()-extraWidth)+'px'},300);
        $("#googSearch").animate({width:($("#googSearch").width()-extraWidth)+'px'},300);
        setTimeout($.showNavOptions,300);
      }
    });

    $.showNavOptions = function() {
      $("#headerMenuHolder ul.nav").show();
      navShown = 1;
    };

    $.hideNavOptions = function() {
      $("#headerMenuHolder ul.nav").hide();
      navShown = 0;
    };

    $.showSearchResults = function() {
      //$('#searchResultsContainer').show();
      $('#searchResultsContainer').css('opacity',1).slideDown();
      $('#pageBanner').hide();
      $('#innerContentContainer').hide();
    };

    $.hideSearchResults = function() {
      //$('#searchResultsContainer').hide();
      $('#searchResultsContainer').slideUp("fast",function() { $('#searchResultsContainer').hide(); });
      $('#pageBanner').show();
      $('#innerContentContainer').show();
    };

    $("#searchResultsClose").click(function(e) {
      $.hideSearchResults();
      $('.gsc-clear-button').click();
    });

    $('.gsc-clear-button').click(function(e) {
      $.hideSearchResults();
    });

  } else if (searchType == 2) {
    /* Modern Style Search Box */
    $('#searchBox').live('focus',function() {
      $("#searchWrapper").animate({width:($("#searchWrapper").width()+200)+'px'},300);
      $("#searchDiv").removeClass("dark");
      $("#searchDiv").addClass("light");
      $("span.searchIcon").removeClass("light");
      $("span.searchIcon").addClass("dark");
    }).live('blur',function() {
      if ($("#searchWrapper").val() == '') {
        $("#searchWrapper").animate({width:($("#searchWrapper").width()-200)+'px'},300);
      }
      $("#searchDiv").removeClass("light");
      $("#searchDiv").addClass("dark");
      $("span.searchIcon").removeClass("dark");
      $("span.searchIcon").addClass("light");
    });

  }


  // doesnt work - why?
  /*
  // Scroll to Top button
  $(document).scroll(function(){
  if ($(this).scrollTop() > 5) {
  $('.scrollup').fadeIn();
} else {
$('.scrollup').fadeOut();
}
});
$('.scrollup').click(function(){
$("html, body").animate({ scrollTop: 0 }, 600);
return false;
});
*/

/**** END HEADER JAVASCRIPT ****/

/**** BODY JAVASCRIPT ****/
if ($('#rotatescroll')) {
  $('#rotatescroll').tinycircleslider({
    interval: true,
    snaptodots: true,
    intervaltime: 10000,
    radius: 140,
    callback: function() {}
  });
}
if ($('.overlay')) {
  $('.overlay').click(function(){
    $.trackGAEvent('CircleSliderImage', 'CircleSliderImage.click', 'Circle Slider Image was clicked');
    if ($('.Login')) {
      $('.Login').click();
    }
  });
}
$('.joinPromo').click(function(){
  $.trackGAEvent('JoinPromoBanner', 'PromoBanner.click', 'Promo Banner to Join RavePad was clicked');
  if ($('.Login')) {
    $('.Login').click();
  }
});

/**** END BODY JAVASCRIPT ****/

/**** FOOTER JAVASCRIPT ****/
<?php $domain_name=config('app.name') ?>
var domain = '<?php echo $domain_name; ?>';
if ((jQuery.cookie("facebook_sliver_hide") == null)) {
  jQuery('#fb_sliver_text').text('Choosing to "Like" ' + domain + ' leads to no unintended side effects.');

  jQuery('#fb_sliver_fan_box').html(
    '<iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FRavePad%2F192729684118503&amp;width=292&amp;colorscheme=dark&amp;show_faces=false&amp;stream=false&amp;header=false&amp;height=62" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:292px; height:62px;background-color: rgba(0,0,0,0);" allowTransparency="true"></iframe>'
  );
  jQuery('#fb_sliver').css({'height':'60px', 'overflow': 'visible'}).fadeTo('slow', .60);
  jQuery('.connect_widget_not_connected_text').css('color', '#fff');

  jQuery('#fb_sliver_close').click(function() {
    jQuery('#fb_sliver').fadeOut('slow').remove();
    jQuery.cookie("facebook_sliver_hide", 1, { path: '/', expires: 7 });
  });
}
/**** END FOOTER JAVASCRIPT ****/

});
</script>
<?php
$show_ads=config("app.showAds");
$show_taboola_ads = config('app.showTaboolaAds');
 ?>
@if ($show_ads && $show_taboola_ads)
  <script>
  window._taboola = window._taboola || [];
  _taboola.push({flush:true});
  </script>
@endif

</body>
</html>
