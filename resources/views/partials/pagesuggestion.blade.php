<div id="showPages" class="widget" style="display:none">
	<div id="showPagesTitle"><i class="fa fa-asterisk"></i> Page Suggestions</div>
	<?php for($i = 0; $i < 3; $i++): ?>
		<div id="showPages<?php echo $i?>" class="showPagesEntry" style='display:none'>
			<div class="showPagesThumbnail"><img /></div>
			<div class="showPagesText"><a href=""></a></div>
			<div class="showPagesFollowBtn btn" style="display:none">Follow Page Text Here...</div>
			<div class="showPagesFollowers" style="display:none;">0 following</div>
		</div>
	<?php endfor; ?>
</div>
<script type="text/javascript">
	var BaseUrl = '/';
	var currentpage = '<?php echo empty($pageSlug) ? 0 : $pageSlug ?>';
	var followPageTxt = "Follow Page Text Here";
	var timeoutevent;
	$('#showPages').ready(function() {
		$.get(BaseUrl + "page/getPageSuggestions", {slug: currentpage}, function(data) {
			var dataJson = $.parseJSON(data);

			if(dataJson.data.length) {
				for(i = 0; i < dataJson.data.length; i++) {
					$('#showPages' + i + " >.showPagesText a").text(dataJson.data[i]['Page']['name']);
					$('#showPages' + i + " >.showPagesText a").attr('href', BaseUrl + 'page/' + dataJson.data[i]['Page']['slug']);
					if (dataJson.data[i]['Page']['thumbnail_url']) {
						$('#showPages' + i + " >.showPagesThumbnail>img").attr("src", BaseUrl + 'img/photos/pages/' + dataJson.data[i]['Page']['id'] + '/' + dataJson.data[i]['Page']['thumbnail_url']);
					} else {
						$('#showPages' + i + " >.showPagesThumbnail>img").attr("src", BaseUrl + 'img/photos/banners/' + dataJson.data[i]['Page']['id'] + '/icon.jpg');
					}
					$('#showPages' + i + " >.showPagesThumbnail>img").attr("name", BaseUrl + 'page/' + dataJson.data[i]['Page']['slug']);
					$('#showPages' + i + " >.showPagesFollowBtn").attr('name', dataJson.data[i]['Page']['slug']);

					// tooltips
					$('#showPages' + i + " >.showPagesFollowBtn").tipsy({gravity: 'w', fade: true, html: true, title: function() { return toCamelCase(followPageTxt); }});

					$('#showPages' + i).attr('style', null);
					if (dataJson.user) {
						// only enabled for Admins
						$('#showPages' + i + ' >.showPagesFollowBtn').attr('style', null);
					}
				}
				$('#showPages').attr('style', null);
			}
		});
	});

	$('.showPagesThumbnail img').click(function() {
		if (currentpage == 0) {
			// home page
			$.trackGAEvent('PageSuggestionsWidget', 'PageSuggestions.Image.click', 'Page Suggestions widget on Main Homepage - Image was clicked');
		} else {
			$.trackGAEvent('PageSuggestionsWidget', 'PageSuggestions.Image.click', 'Page Suggestions widget on Page Home - Image was clicked');
		}
		window.location = $(this).attr('name');
	});

	$(".showPagesText a").click(function() {
		if (currentpage == 0) {
			// home page
			$.trackGAEvent('PageSuggestionsWidget', 'PageSuggestions.PageName.click', 'Page Suggestions widget on Main Homepage - Page Name was clicked');
		} else {
			$.trackGAEvent('PageSuggestionsWidget', 'PageSuggestions.PageName.click', 'Page Suggestions widget on Page Home - Page Name was clicked');
		}
		window.location = $(this).attr('name');
	});

	$('.showPagesFollowBtn').click(function() {
		var btn = this;
		var ids = [];

		if (currentpage == 0) {
			// home page
			$.trackGAEvent('PageSuggestionsWidget', 'PageSuggestions.FollowBtn.click', 'Page Suggestions widget on Main Homepage - Follow Button was clicked');
		} else {
			$.trackGAEvent('PageSuggestionsWidget', 'PageSuggestions.FollowBtn.click', 'Page Suggestions widget on Page Home - Follow Button was clicked');
		}

		ids.push(currentpage);
		$.each($('.showPagesEntry > .btn'), function(index, value) {
			if ($(value).attr('name') != undefined && $(value).attr('name') != $(btn).attr('name')) {
				ids.push($(value).attr('name'));
			}
		});

		$.get(BaseUrl + "page/followPage", {slug: ids, val: $(btn).attr('name')}, function(data) {
			var dataJson = $.parseJSON(data);
			if (dataJson.success == 1) {
				if (dataJson.badge) {
					showAwardedBadge(dataJson.badge);
				}

				if (dataJson.data) {
					// Clone the div.
					$('.showPagesEntry:last').clone(true).insertAfter('.showPagesEntry:last');
					var newname = parseInt($('.showPagesEntry:last').attr('id').substr(9,$('.showPagesEntry:last').attr('id').length)) + 1;
					$('.showPagesEntry:last').attr("id", 'showPages' + newname);
					$('.showPagesEntry:last').attr("style", "display:none;");
					$('.showPagesEntry:last >.showPagesText').text(dataJson.data[0]['Page']['name']);
					if (dataJson.data[0]['Page']['thumbnail_url']) {
						$('.showPagesEntry:last >.showPagesThumbnail>img').attr("src", BaseUrl + 'img/photos/pages/' + dataJson.data[0]['Page']['id'] + '/' + dataJson.data[0]['Page']['thumbnail_url']);
					} else {
						$('.showPagesEntry:last >.showPagesThumbnail>img').attr("src", BaseUrl + 'img/photos/banners/' + dataJson.data[0]['Page']['id'] + '/icon.jpg');
					}
					$('.showPagesEntry:last >.showPagesFollowBtn').attr('name', dataJson.data[0]['Page']['slug']);
					if (dataJson.user) {
						$('.showPagesEntry:last >.showPagesFollowBtn').attr('style', null);
					}
					$('.showPagesEntry:last').addClass("showAfterPage");
				}
				$(btn.parentNode).addClass("successAddPage");
				clearTimeout(timeoutevent);

				timeoutevent = setTimeout("cleanupHideSuccess()",1500);
			}
		});
	});
	function cleanupHideSuccess() {
		// We should refactor this better.
		$.each($('.showPagesEntry'), function(index, value) {
			if ($(value).hasClass('successAddPage')) {
			$(value).removeClass('successAddPage');
				$(value).slideUp('normal', cleanupShowMore);
			}
		});
	};

	function cleanupShowMore() {
		// We should refactor this better.
		var everythingHidden = 0;
		$.each($('.showPagesEntry'), function(index, value) {
			if ($(value).hasClass('showAfterPage') && !$(value).hasClass('successAddPage')) {
				$(value).removeClass('showAfterPage');
				$(value).attr('style', null);
				everythingHidden = 1;
			} else if (!everythingHidden && $(value).attr('style') == 'null') {
				everythingHidden = 1;
			}
		});

		if (everythingHidden == 0) {
			$('#showPages').parent().slideUp('normal', function() {});
		}
	};

</script>
