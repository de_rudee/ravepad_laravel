<?php
	$show_ads = config('app.showAds');
	$ad_type=1;
	if ($show_ads) { ?>
		@include('partials.ads.direct_advertising_link');
		<?php if ($ad_type == 1) {
			// Adsense
?>
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- 728 x 90 - Leaderboard Ad Unit#2 -->
			<ins class="adsbygoogle"
			     style="display:inline-block;width:728px;height:90px"
			     data-ad-client="ca-pub-4512674464486566"
			     data-ad-slot="6609596625"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
<?php
		} else if ($ad_type == 2) {
			// Chitika
?>
			<!-- Begin: Chitika  -->
			<script type="text/javascript">
			  ( function() {
				if (window.CHITIKA === undefined) {
				  window.CHITIKA = { 'units' : [] };
				};
				var unit = {
				  'fluidH' : 1,
				  'nump' : "1",
				  'publisher' : "ravepad",
				  'width' : 720,
				  'height' : "auto",
				  'type' : "mpu",
				  'sid' : "Ravepad",
				  'color_site_link' : "086785",
				  'color_title' : "086785",
				  'color_border' : "FFFFFF",
				  'color_text' : "949494",
				  'color_bg' : "EDEDED"
				};
				var placement_id = window.CHITIKA.units.length;
				window.CHITIKA.units.push(unit);
				document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');
				var s = document.createElement('script');
				s.type = 'text/javascript';
				s.src = 'http://scripts.chitika.net/getads.js';
				try {
				  document.getElementsByTagName('head')[0].appendChild(s);
				} catch(e) {
				  document.write(s.outerHTML);
				}
			}());
			</script>
			<!-- End: Chitika -->

<?php
		} else if ($ad_type == 3) {
			// YesAdvertising
?>
			<!-- Begin: YesAdvertising  -->

			<!-- START CLIENT.YESADVERTISING CODE -->
			<script language="javascript" type="text/javascript" charset="utf-8">
			var cpxcenter_banner_border = '#c9c9c9';
			var cpxcenter_banner_text = '#000000';
			var cpxcenter_banner_bg = '#FFFFFF';
			var cpxcenter_banner_link = '#000000';
			cpxcenter_width = 728;
			cpxcenter_height = 90;
			</script>
			<script language="JavaScript" type="text/javascript" src="//ads.cpxcenter.com/cpxcenter/showAd.php?nid=4&amp;zone=34080&amp;type=banner&amp;sid=26114&amp;pid=25867&amp;subid=">
			</script>
			<!-- END CLIENT.YESADVERTISING CODE -->

			<!-- End: YesAdvertising  -->
<?php
		} else if ($ad_type == 4) {
			// Propeller Ads
?>
			<iframe src="//go.padstm.com/?id=107943&t=iframe" style="width:728px;height:90px;border:0;overflow:hidden;"></iframe>
<?php
		}
	}
?>
