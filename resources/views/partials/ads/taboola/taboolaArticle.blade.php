<?php
	$show_ads = config('app.showAds');
	$show_taboola_ads = config('app.showTaboolaAds');
	if ($show_ads && $show_taboola_ads) {
?>
		<div id='taboola-below-article'></div>
		<script type="text/javascript">
		window._taboola = window._taboola || [];
		_taboola.push({mode:'thumbs-2r', container:'taboola-below-article', placement:'below-article'});
		</script>
<?php
	}
?>
