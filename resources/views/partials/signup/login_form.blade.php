<div id="LoginContainer" style="display:none">
<div id="signInTitleDiv">
	<div id="signInTitle">Sign in to RavePad!</div>
	<a href={{asset("register")}} id="noAccountTop" class="noAccount">or Join here</a>
</div>
<div class="clear"></div>
<div id="LoginContainerDiv">
  <form action={{asset("/cake/users/login_ajax")}} id="UserLoginForm" method="post" accept-charset="utf-8">
    <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
    <table class="formTable"><tbody>
      <tr><td class="label">Email or Username</td><td><input name="data[User][username]" type="text" nojsvalidation="1" id="UserLoginFormUserusername" maxlength="64"></td></tr>
      <tr><td class="label">Password</td><td><input type="password" name="data[User][password]" autocomplete="off" maxlength="20" nojsvalidation="1" id="UserLoginFormUserpassword"></td></tr>
      <input type="hidden" name="data[User][login_page]" value="1" id="UserLoginPage">
      <tr><td class="label"></td><td id="forgotPwdSection">Click here if you <a href="/cake/forgot">Forgot your Password</a></td></tr>
	</tbody></table>
	<div class="loginSubmitBtn"><input id="loginSubmitBtn" label="Login" class="btn btnextralarge" type="submit" value="Log in"></div></form>
	<tr>
		<td class="label"></td>
		<td id="forgotPwdSection">
		Click here if you <a href={{asset("forgot")}}>Forgot your Password</a></td>
	</tr>
	</table>
	<div class="loginSubmitBtn"><input id="loginSubmitBtn" label="Login" class="btn btnextralarge" type="submit" value="Log in"></div></form>
</div>

<div class="clear"></div>
<div class="errorMessages">
	<div id='flashMessage' class='message'>Message to be added here</div>
</div>
<div id="noAccountBottomDiv"><div id="noAccountBottomText">Don't have an account?</div> <a href={{asset("/register")}} id="noAccountBottom" class="noAccount">Create one</a>
</div>
</div>

<script type="text/javascript">
	var okay = 0;

	$('#noAccountTop').click(function(link) {
		$('#signUpDialog').dialog('close');
		$('.signUpForm').click();
		link.preventDefault();
	});
	$('#noAccountBottom').click(function(link) {
		$('#signUpDialog').dialog('close');
		$('.signUpForm').click();
		link.preventDefault();
	});

	$('#LoginContainer').ready(function() {
		$('#LoginContainer').show();
	});
	$('#loginSubmitBtn').click(function(link) {
		if (okay == 0) {
			var options = {
				dataType: 'json',		// 'xml', 'script', or 'json'
				success: showResponse	// post-submit callback
			};
			link.preventDefault();
			$("#UserLoginForm").ajaxSubmit(options);
		} else {
			$('#UserLoginForm').attr('action', 'login');
		}
	});

	function showResponse(responseText, statusText, xhr, $form) {
		if (responseText && responseText['login']) {
			$('#UserLoginErr').remove();
			$('.errorMessages').append('<div id="UserLoginErr" class="error-message">' + responseText['login'] + '</div>');
			$("#UserLoginErr").stop().css("background-color", "#FF9C9C").animate({ backgroundColor: "#FDE9EF"}, 1500);

		} else {
			okay = 1;
			$('#loginSubmitBtn').click();
		}
	}
</script>
