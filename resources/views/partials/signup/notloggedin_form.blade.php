<div id="notLoggedInContainer" style="display:none">
  <div id="notLoggedInContainerTitle" class="center">You must login to answer more.</div>
  <div id="notLoggedInText">Login to save your responses and answer more!</div>
  <div id="JoinRavePad" class="btn btnextralarge">Join RavePad!</div>
  <div id="Login" class="btn btnextralarge">Login</div>
</div>

<script type="text/javascript">
  $('#notLoggedInContainer').ready(function() {
    $('#notLoggedInContainer').show();
  });

  $('#notLoggedInText a').click(function(event) {
	event.preventDefault();
	$('#JoinRavePad').click();
  });


</script>
