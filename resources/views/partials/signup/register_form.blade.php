<div id="signUpFormContainer" style="display:none">
	<?php if(isset($error)): ?>
		<div class="error-message"><?php echo $error ?></div>
	<?php endif; ?>
	<div id="registerTitleDiv">
		<div id="registerTitle">Join RavePad!</div>
		<a href={{asset("/login")}} class="haveAccount" id="haveAccountTop">or Log in here</a>
	</div>
	<div class="clear"> </div>
	<a href="javascript:void(0);" id="toggleFBForm">Use Facebook to pre-fill your information</a>
	<div class="clear"> </div>
	<div id="normalRegistrationPopup" style="display:none">
		<div id="normalRegistrationDiv">
			<div id="normalRegistrationDivBox">
				<div id="normalRegistrationTopBox">Have a Facebook account? <a href="javascript:void(0);" id="toggleFBForm2">Click here to use Facebook to pre-fill your info.<br/><b>NOTE:</b> You will choose your own password and we do <b>NOT</b> get access to your Facebook password.</a></div>

        <form action={{asset('/cake/register')}} id="UserRegisterForm" method="post" accept-charset="utf-8" class="formValidation_submit">
          <div style="display:none;"><input type="hidden" name="_method" value="POST"></div><table class="formTable">
            <tbody>
              <tr><td class="label">Email</td><td><input name="data[User][email]" type="text" id="UserRegisterFormEmail" maxlength="64" class="formValidation_blur"></td></tr>
              <tr><td class="label">Password</td><td><input type="password" name="data[User][newPassword]" autocomplete="off" maxlength="20" id="UserRegisterFormNewPassword" class="formValidation_blur"></td></tr>
              <tr><td class="label">Confirm Password</td><td><input type="password" name="data[User][password]" class="VerifyPass" autocomplete="off" maxlength="20" id="UserRegisterFormPassword"></td></tr>
              <tr><td class="label"></td><td id="tosSection">By clicking the button below you agree to our <a href="#" onclick="showTerms(); return false;">Terms and Conditions</a></td></tr>
				</tbody></table><div class="registerSubmitBtn"><input class="checkPassword btn btnextralarge" type="submit" value="Create Free Account"></div></form>
			</div>
			<div id="UserRegisterFormErrorMsgs" class="RegisterErrors errorMessages"><?php //= $this->MyForm->showErrorMessages('UserRegisterForm'); ?></div>
		</div>
		<?php //echo $this->Html->image('logo/arrow.png', array('id' => 'facebookArrow', 'alt' => 'Register by pre-filling your Facebook information', 'height' => '100')); ?>
		<?php //echo $this->Html->image('logo/Facebook_register.png', array('id' => 'facebookRegisterHelp', 'alt' => 'Register by pre-filling your Facebook information', 'height' => '320')); ?>
	</div>
	<div id="facebookRegistrationPopup" style="display:none">
		<div id="facebookRegistrationDiv">
			<iframe src="https://www.facebook.com/plugins/registration.php?
				client_id=170496756384854&
				redirect_uri=''&
				fields=[
					{'name':'name'},
					{'name':'email'},
					{'name':'gender'},
					{'name':'birthday'},
				]"
				scrolling="auto"
				frameborder="no"
				style="border:none"
				allowTransparency="true"
				width="100%"
				height="320">
			</iframe>
			<div id="fbFormPrefilledText">You will be asked to create a password after this. This form is prefilled for your convenience only. We make use of none of your Facebook information other than what you see here.</div>
			<a id="toggleFBForm3" href="javascript:void(0);">or Don't use Facebook</a>
		</div>
		<!--
		<div id="facebookLovesRPDiv">
			<?php //echo $this->Html->image('logo/Facebook_help.png', array('id' => 'facebookHelp', 'alt' => 'Facebook Loves RavePad', 'height' => '150')); ?>
			<?php //echo $this->Html->image('logo/Facebook_loves_Ravepad.png', array('id' => 'facebookLovesRP', 'alt' => 'Facebook Loves RavePad', 'height' => '200')); ?>
			<?php //echo $this->Html->image('logo/noFacebookSpam.png', array('id' => 'noFacebookSpam', 'alt' => 'Facebook Loves RavePad', 'height' => '80')); ?>
			<div class="clear"> </div>
			<a id="toggleFBForm3" href="javascript:void(0);">or Don't use Facebook</a>
		</div>
		-->
	</div>
</div>
<script type="text/javascript">
	$('#signUpFormContainer').ready(function() {
		$('#signUpFormContainer').show();
		var fbFormShown = true;

		function showFBRegistrationPopUpForm() {
			fbFormShown = true;
			$("#toggleFBForm").text('Don\'t use Facebook');
			$('#facebookRegistrationPopup').show();
			$('#normalRegistrationPopup').hide();
		}

		function showNormalRegistrationPopUpForm() {
			fbFormShown = false;
			$("#toggleFBForm").text('Use Facebook to pre-fill your information');
			$('#facebookRegistrationPopup').hide();
			$('#normalRegistrationPopup').show();
		}

		/*
		 * Typically you will want to use the auth.authResponseChange event.
		 * But in rare cases, you want to distinguish between these three states:we
		 * 1. Connected ("connected")
		 * 2. Logged into Facebook but not connected with your application ("not_authorized")
		 * 3. Not logged into Facebook at all ("unknown")
		 */
		var FBfeedback = false;
		FB.getLoginStatus(function(response) {
			if (response.status == 'unknown') {
				showNormalRegistrationPopUpForm();
			} else if ((response.status == 'connected') || (response.status == 'not_authorized')) {
				//showFBRegistrationPopUpForm();
				showNormalRegistrationPopUpForm();
			} else {
				showNormalRegistrationPopUpForm();
			}
			FBfeedback = true;
		});
		if (!FBfeedback) {
			// Sometimes facebook doesn't callback. When that occurs, we just show Normal registration.
			showNormalRegistrationPopUpForm();
		}

		$("#toggleFBForm").click(function() {
			if (fbFormShown) {
				showNormalRegistrationPopUpForm();
			} else {
				showFBRegistrationPopUpForm();
			}
			$('#signUpFormContainer').parent().parent().css({ 'left' : ($(document).width() - $('#signUpFormContainer').parent().width() - 53.6)/2.0 });
		});
		$("#toggleFBForm2").click(function() {
			if (fbFormShown) {
				showNormalRegistrationPopUpForm();
			} else {
				showFBRegistrationPopUpForm();
			}
			$('#signUpFormContainer').parent().parent().css({ 'left' : ($(document).width() - $('#signUpFormContainer').parent().width() - 53.6)/2.0 });
		});
		$("#toggleFBForm3").click(function() {
			if (fbFormShown) {
				showNormalRegistrationPopUpForm();
			} else {
				showFBRegistrationPopUpForm();
			}
			$('#signUpFormContainer').parent().parent().css({ 'left' : ($(document).width() - $('#signUpFormContainer').parent().width() - 53.6)/2.0 });
		});
	});

	$('#haveAccountTop').click(function(link) {
		$('#signUpDialog').dialog('close');
		$('.Login').click();
		link.preventDefault();
	});

	function passwordFieldsChanged() {
		var pass1 = $('#UserRegisterFormPassword').val();
		var pass2 = $('#UserRegisterFormNewPassword').val();
		if (pass1 === pass2) {
			$('.VerifyPass').addClass('field-valid');
			$('.VerifyPass').removeClass('form-error');
			$('#UserRegisterFormErrorMsgsUserRegisterFormPassword').remove();
		} else {
			if ($('#UserRegisterFormErrorMsgsUserRegisterFormPassword').length == 0) {
				$('.VerifyPass').addClass('form-error');
				$('.errorMessages').append('<div id="UserRegisterFormErrorMsgsUserRegisterFormPassword" class="error-message">Password and confirmation password need to match.</div>');
			}
		}
	}

	$('.VerifyPass').keyup(function() {
		passwordFieldsChanged();
	});

	$('#UserRegisterFormNewPassword').keyup(function() {
		passwordFieldsChanged();
	});

	$('.checkPassword').click(function(event) {
		var pass1 = $('#UserRegisterFormPassword').val();
		var pass2 = $('#UserRegisterFormNewPassword').val();
		if (pass1 !== pass2) {
			event.preventDefault();
		}
	});

</script>
