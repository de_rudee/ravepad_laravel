<div id="picksInteresting" style="display:none;">
	<div id="picksInterestingTitle">
		<div id="picksInterestingImage"></div>
		<div id="picksInterestingTitleText">Interesting Picks</div>
	</div>
	<div id="picksInterestingArea">
		<?php for($i = 0; $i< 10; $i++): ?>
			<div id="picksInterestingEntry<?php echo $i;?>" class="picksInterestingEntry" style="display:none;">
				<?php if (empty($pageSlug)): ?>
					<div class="picksInterestingPicture">
						<img width="50" height="50" alt="" src="">
					</div>
				<?php endif; ?>
				<div class="picksInterestingTextArea">
					<div class="picksInterestingTextpage"><a href="">page</a></div>
					<div class="picksInterestingPick"><a href="">a question</a></div>
					<div class="picksInterestingText">fanpicked</div>
				</div>
				<div class="picksInterestingToptwo">
					<div class="picksInterestingChoice"><img width="100" height="100" alt="" src="" style="width: 100px; height: 100px; padding: 0px;"></div>
					<div class="picksInterestingChoice"><img width="100" height="100" alt="" src="" style="width: 100px; height: 100px; padding: 0px;"></div>
					<div class="picksInterestingTextIcon">100%</div>
					<div class="picksInterestingTextIcon">100%</div>
				</div>
			</div>
		<?php endfor; ?>
	</div>
	<div id="picksInterestingMore"><?php if (!empty($pageSlug)) { echo $this->Html->link('more picks >>', "/page/$pageSlug/picks");} ?></div>
</div>

<script type="text/javascript">
	var pageSlug = '<?php echo empty($pageSlug) ? 0 : $pageSlug ?>';
	var registerCtrBaseUrl = '/register';
	var userLogged = <?php
	if(Auth::user()){
		echo 1;
	}
	else{
		echo 0;
	}
	
	?>;

	$('#picksInteresting').ready(function() {
		$.get(webBaseUrl + "page/" + pageSlug + "/picks/getInterestingPicks", null, function(data) {
			var dataJson = $.parseJSON(data);

			if (dataJson.interestingPicks.length) {
				for(i = 0; i < dataJson.interestingPicks.length; i++) {
					/*
					var page_idx = 0;
					for(j = 0; j < dataJson.pagesData.length; j++) {
						if (dataJson.pagesData[j]['Page']['id'] == dataJson.interestingPicks[i]['Pick']['id_page']) {
							page_idx = j;
						}
					}
					*/

					if (pageSlug == 0) {
						var thumbUrl;
						if (dataJson.interestingPicks[i]['Page']['thumbnail_url']) {
							thumbUrl = webBaseUrl + "img/photos/pages/" + dataJson.interestingPicks[i]['Page']['id'] + '/' + dataJson.interestingPicks[i]['Page']['thumbnail_url'];
						} else {
							thumbUrl = webBaseUrl + "img/photos/banners/" + dataJson.interestingPicks[i]['Page']['id'] + '/icon.jpg';
						}
						$('#picksInterestingEntry' + i + ' >.picksInterestingPicture > img').attr('src', thumbUrl);
						$('#picksInterestingEntry' + i + ' >.picksInterestingPicture').attr('value', dataJson.interestingPicks[i]['Page']['id']);
						$('#picksInterestingEntry' + i + ' >.picksInterestingTextArea > .picksInterestingTextpage a').text(dataJson.interestingPicks[i]['Page']['name']);
						$('#picksInterestingEntry' + i + ' >.picksInterestingTextArea > .picksInterestingTextpage a').attr('href',webBaseUrl + 'page/' + dataJson.interestingPicks[i]['Page']['slug']);
					} else {
						$('#picksInterestingEntry' + i + ' >.picksInterestingTextArea > .picksInterestingTextpage').empty();
					}
					$('#picksInterestingEntry' + i + ' >.picksInterestingTextArea > .picksInterestingPick > a').text(dataJson.interestingPicks[i]['Pick']['question_text']);

					$('#picksInterestingEntry' + i + ' >.picksInterestingTextArea > .picksInterestingPick > a').attr('href', webBaseUrl + 'page/' + dataJson.interestingPicks[i]['Page']['slug'] + '/picks/view/' + dataJson.interestingPicks[i]['Pick']['id'] + "/" + dataJson.interestingPicks[i]['Pick']['slug']);

					dataJson.interestingPicks[i]['Pickchoice'].sort(test);
					var totalResponses = 0;
					for (j = 0; j < dataJson.interestingPicks[i]['Pickchoice'].length; j++) {
						totalResponses += Number(dataJson.interestingPicks[i]['Pickchoice'][j]['response_count']);
					}
					if (totalResponses == 0) {
						$('#picksInterestingEntry' + i + ' >.picksInterestingTextArea > .picksInterestingText:last').text('');
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingTextIcon').text('');
					} else {
						$('#picksInterestingEntry' + i + ' >.picksInterestingTextArea > .picksInterestingText:last').html("The community picked: <b>"+dataJson.interestingPicks[i]['Pickchoice'][0]['description']+"</b>");
						var firstResponse = dataJson.interestingPicks[i]['Pickchoice'][0]['response_count'] * 100/totalResponses;
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingTextIcon:first').text(parseInt(firstResponse) + '%');
						var secondResponse = dataJson.interestingPicks[i]['Pickchoice'][1]['response_count'] * 100/totalResponses;
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingTextIcon:last').text(parseInt(secondResponse) + '%');
					}

					if (dataJson.interestingPicks[i]['Pickchoice'][0] != undefined && dataJson.interestingPicks[i]['Pickchoice'][0]['image_url']) {
						if (dataJson.interestingPicks[i]['Pickchoice'][0]['is_external_image']) {
							$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:first > img').attr('src', dataJson.interestingPicks[i]['Pickchoice'][0]['image_url']);
						} else {
							$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:first > img').attr('src', webBaseUrl + "img/photos/picks/" + dataJson.interestingPicks[i]['Pick']['id'] + '/' + dataJson.interestingPicks[i]['Pickchoice'][0]['image_url']);
						}
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:first > img').attr({
													'title' : dataJson.interestingPicks[i]['Pickchoice'][0]['description'],
													'width' : dataJson.interestingPicks[i]['Pickchoice'][0]['width'],
													'height' : dataJson.interestingPicks[i]['Pickchoice'][0]['height'] });
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:first').css({
							'height' : dataJson.interestingPicks[i]['Pickchoice'][0]['height'],
							'width' : dataJson.interestingPicks[i]['Pickchoice'][0]['width'],
							'padding-top' : dataJson.interestingPicks[i]['Pickchoice'][0]['padding_height'] + 'px ',
							'padding-bottom' : dataJson.interestingPicks[i]['Pickchoice'][0]['padding_height'] + 'px ',
							'padding-right' : dataJson.interestingPicks[i]['Pickchoice'][0]['padding_width'] + 'px ',
							'padding-left' : dataJson.interestingPicks[i]['Pickchoice'][0]['padding_width'] + 'px ',
							});
						//$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:first').css('padding', (dataJson.interestingPicks[i]['Pickchoice'][0]['height']-100)/2 + 'px ' + (dataJson.interestingPicks[i]['Pickchoice'][0]['width']-100)/2 + 'px ' + (dataJson.interestingPicks[i]['Pickchoice'][0]['height']-100)/2  + 'px ' + (dataJson.interestingPicks[i]['Pickchoice'][0]['width']-100)/2 + 'px ');
					} else {
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:first').html(dataJson.interestingPicks[i]['Pickchoice'][0]['description']);
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:first').attr({'title' : dataJson.interestingPicks[i]['Pickchoice'][0]['description']});
					}
					if (dataJson.interestingPicks[i]['Pickchoice'][1] != undefined && dataJson.interestingPicks[i]['Pickchoice'][1]['image_url']) {
						if (dataJson.interestingPicks[i]['Pickchoice'][1]['is_external_image']) {
							$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:last > img').attr('src', dataJson.interestingPicks[i]['Pickchoice'][1]['image_url']);
						} else {
							$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:last > img').attr('src', webBaseUrl + "img/photos/picks/" + dataJson.interestingPicks[i]['Pick']['id'] + '/' + dataJson.interestingPicks[i]['Pickchoice'][1]['image_url']);
						}
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:last > img').attr({
													'title' : dataJson.interestingPicks[i]['Pickchoice'][1]['description'],
													'width' : dataJson.interestingPicks[i]['Pickchoice'][1]['width'],
													'height' : dataJson.interestingPicks[i]['Pickchoice'][1]['height'] });
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:last').css({
							'height' : dataJson.interestingPicks[i]['Pickchoice'][1]['height'],
							'width' : dataJson.interestingPicks[i]['Pickchoice'][1]['width'],
							'padding-top' : dataJson.interestingPicks[i]['Pickchoice'][1]['padding_height'] + 'px ',
							'padding-bottom' : dataJson.interestingPicks[i]['Pickchoice'][1]['padding_height'] + 'px ',
							'padding-right' : dataJson.interestingPicks[i]['Pickchoice'][1]['padding_width'] + 'px ',
							'padding-left' : dataJson.interestingPicks[i]['Pickchoice'][1]['padding_width'] + 'px ',
							});
					} else {
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:last').html(dataJson.interestingPicks[i]['Pickchoice'][1]['description']);
						$('#picksInterestingEntry' + i + ' >.picksInterestingToptwo > .picksInterestingChoice:last').attr({'title' : dataJson.interestingPicks[i]['Pickchoice'][1]['description']});
					}
					$('#picksInterestingEntry' + i).attr('style', null);
				}
				$('#picksInteresting').attr('style', null);
			}
		});
	});

	function test(a, b) {
		if (a['response_count'] == b['response_count']) {
			return a['id'] < b['id'] ? -1 : 1;
		}
		return (a['response_count'] < b['response_count']) ? 1 : -1;
	}

	/* click functions */
	$('.picksInterestingPicture').click(function(link) {
		if (pageSlug == 0) {
			link.stopPropagation();
			window.location = webBaseUrl + 'page/' + $(this).attr('value');
		}
	});

	// if you enable this, clicking anywhere in the div takes you to the pick
	$('.picksInterestingEntry').click(function() {
		window.location = $(this).children('.picksInterestingTextArea').children('.picksInterestingPick').children('a').attr('href');
	});


	/*
	$('.picksInterestingPick > a').click(function(link) {
		if (userLogged == 0) {
			link.preventDefault();
			window.location = registerCtrBaseUrl;
		}
	});
	*/
</script>
