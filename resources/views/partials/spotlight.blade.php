<div id="showSpotlight" style="display:none;">
	<div id="showSpotlightTitle">
		<div id="showSpotlightTitleImage"></div>
		<div id="showSpotlightTitleText">You might also like..</div>
	</div>
	<div id="showSpotlightArea">
		<?php for($i = 0; $i < 8; $i++): ?>
			<div id="showSpotlightEntry<?php echo $i?>" class="showSpotlightEntry" style="display:none;">
				<div class="showSpotlightEntryImage"><a href=""><img width=100 height=100 class="circular" src=""></a></div>
				<div class="showSpotlightEntryText"><a href=""></a></div>
				<div class="showSpotlightEntryFollow btn" style="display:none">+</div>
			</div>
		<?php endfor; ?>
	</div>
</div>

<script type="text/javascript">
	$(".showSpotlightEntry").mouseenter(function(){$(this).find("img").removeClass('circular');});
	$(".showSpotlightEntry").mouseleave(function(){$(this).find("img").addClass('circular');});

	var userLogged = <?php
	if(Auth::user()){
		echo 1;
	}
	else{
		echo 0;
	}
	$followPageTxt="Follow Page Text";
	$unfollowPageTxt="Unfollow Page Text";
	?>;
	var followPageTxt = "<?php echo $followPageTxt; ?>";
	var unfollowPageTxt = "<?php echo $unfollowPageTxt; ?>";

	$('#showSpotlight').ready(function() {
		$.get(webBaseUrl + "page/getSpotlights", null, function(data) {
			var dataJson = $.parseJSON(data);

			for (i = 0; i < dataJson.pageData.length; i++) {
				var pageName = dataJson.pageData[i]['Page']['name'];
				$('#showSpotlightEntry' + i + ' > .showSpotlightEntryText > a').text(pageName);
				$('#showSpotlightEntry' + i + ' > .showSpotlightEntryText > a').attr('href', webBaseUrl + 'page/' + dataJson.pageData[i]['Page']['slug']);
				var thumbUrl;
				if (dataJson.pageData[i]['Page']['thumbnail_url']) {
					thumbUrl = webBaseUrl + 'img/photos/pages/' + dataJson.pageData[i]['Page']['id'] + '/' + dataJson.pageData[i]['Page']['thumbnail_url'];
				} else {
					thumbUrl = webBaseUrl + 'img/photos/banners/' + dataJson.pageData[i]['Page']['id'] + '/icon.jpg';
				}
				$('#showSpotlightEntry' + i + ' > .showSpotlightEntryImage > a > img').attr('src', thumbUrl);
				$('#showSpotlightEntry' + i + ' > .showSpotlightEntryImage > a > img').attr({
						'width' : dataJson.pageData[i]['Page']['width'],
						'height' : dataJson.pageData[i]['Page']['height'] });
				$('#showSpotlightEntry' + i + ' > .showSpotlightEntryImage').css({
						'height' : dataJson.pageData[i]['Page']['height'],
						'width' : dataJson.pageData[i]['Page']['width'],
						'padding-top' : dataJson.pageData[i]['Page']['padding_height'] + 'px ',
						'padding-bottom' : dataJson.pageData[i]['Page']['padding_height'] + 'px ',
						'padding-right' : dataJson.pageData[i]['Page']['padding_width'] + 'px ',
						'padding-left' : dataJson.pageData[i]['Page']['padding_width'] + 'px ',
						});
				$('#showSpotlightEntry' + i + ' > .showSpotlightEntryImage > a').attr('href', webBaseUrl + 'page/' + dataJson.pageData[i]['Page']['slug']);
				$('#showSpotlightEntry' + i + ' > .showSpotlightEntryFollow').attr('name', dataJson.pageData[i]['Page']['id']);
				if (dataJson.user) {
					$('#showSpotlightEntry' + i + ' > .showSpotlightEntryFollow').attr('style', null);
				}
				$('#showSpotlightEntry' + i).attr('style', null);
				$('#showSpotlightEntry' + i + ' > .showSpotlightEntryFollow').tipsy({gravity: 's', fade: true, html: true, title: function() { return toCamelCase(followPageTxt); }});
			}
			$('#showSpotlight').attr('style',null);
		});
	});

	$('.showSpotlightEntryFollow').click(function() {
		var btn = this;

		if (userLogged == 1) {
			if ($(btn).hasClass("showSpotlightEntryAdd")) {
				$.get(webBaseUrl + "page/unfollowPage", {val: $(btn).attr('name')}, function(data) {
					var dataJson = $.parseJSON(data);

					if (dataJson.success == 1) {
						$(btn).text('+');
						$(btn).removeClass("showSpotlightEntryAdd");
						$(btn).tipsy({gravity: 's', fade: true, html: true, title: function() { return toCamelCase(followPageTxt); }});
					}
				});
			} else {
				$.get(webBaseUrl + "page/followPage", {val: $(btn).attr('name')}, function(data) {
					var dataJson = $.parseJSON(data);

					if (dataJson.success == 1) {
						$(btn).text('-');
						$(btn).addClass("showSpotlightEntryAdd");
						$(btn).tipsy({gravity: 's', fade: true, html: true, title: function() { return toCamelCase(unfollowPageTxt); }});
					}
				});
			}
		} else {
			window.location = webBaseUrl + 'register/';
		}
	});
</script>
