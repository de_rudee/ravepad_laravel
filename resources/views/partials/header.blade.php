<?php
	$deactivationArray = config('app.deactivationDaysArray');

	// last entry in the array, when account deactivation will happen
	$deactivation_days = 30; //$deactivationArray[count($deactivationArray)-1];
?>

<div id="reminderBar" style="display:none">
	<div id="reminderText">
		You need to activate your account within <?php echo $deactivation_days; ?> days of signing up or it will be deactivated. Click on the link in your email, or
<a href={{asset("/activate")}} title="Resend activation email">have it sent again.</a>
	</div>
	<div id="reminderCloseBox">
			<img src={{asset("img/footer/fb_sliver_close_button.gif")}} alt="X" title="" height="15px" style="cursor:pointer;">
	</div>
</div>
<div id="reminderBarBuffer" style="display:none"></div>

<script type="text/javascript">
	if ((jQuery.cookie("rp_id[activate_bar]") == null)) {
		$('#reminderBar').slideDown();
		$('#reminderBarBuffer').slideDown();
	}

	$('#reminderCloseBox').click(function() {
		$('#reminderBar').slideUp();
		$('#reminderBarBuffer').slideUp();
		jQuery.cookie("rp_id[activate_bar]", 1, { path: '/', expires: 7 });
	});
</script>
