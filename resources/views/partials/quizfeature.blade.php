<div id="showFeatureQuiz" style="display:none;" class="quizWidget">
	<div id="showFeatureQuizTitle"><div id="showFeatureQuizImage"></div>
		<div id="showFeatureQuizText">Random Quiz Question</div>
	</div>
	<div id="showFeatureQuizEntry">
		<div id="showFeatureQuizPage"><a href="">page</a></div>
		<div id="showFeatureQuizQuestion"><a href="">question</a></div>
		<div id="showFeatureQuizQuestionImage"><img width="200" src=""></div>
		<div id="showFeatureQuizOptions">
			<form id="showFeatureQuizFormID" accept-charset="utf-8" method="post">
			<?php
				$choiceArray = array("A", "B", "C", "D");
				for ($i = 0; $i < 4; $i++):
			?>
			<div class="showFeatureQuizChoice" id="showFeatureQuiz<?php echo $i ?>" style="display:none;">
				<input type="button" value="<?php echo $choiceArray[$i]; ?>">
				<div class="showFeatureQuizChoiceText">text</div>
			</div>
			<?php endfor; ?>
			<div id="hiddensubmit" style="display:none;">
				<input type="submit" id="submitchoice" name="0">
			</div>
			</form>
		</div>
	</div>
</div>

<div class="notLoggedIn" style="display:none;"></div>
<div class="Login" style="display:none;"></div>

<script type="text/javascript">
	var pageSlug = '<?php echo empty($pageSlug) ? 0 : $pageSlug ?>';
	var userLogged = <?php
			if(Auth::user()){
				echo 1;
			}
			else{
				echo 0;
			}
	 ?>

	/*
	 * Algorithm found here:
	 * http://stackoverflow.com/questions/962802/is-it-correct-to-use-javascript-array-sort-method-for-shuffling
	 * http://javascript.about.com/library/blsort2.htm
	 * This is not a true sort. This will need to be replaced later.
	 */
	function randOrd() {
		return (Math.round(Math.random()) - 0.5);
	}

	$('#showFeatureQuiz').ready(function() {
		$.get(webBaseUrl + "page/" + pageSlug + "/quizzes/getFeatureQuiz", null, function(data) {
			var dataJson = $.parseJSON(data);
			if (dataJson.featureQuiz) {
				$('#showFeatureQuizPage a').text(dataJson.featureQuiz['Page']['name']);
				$('#showFeatureQuizPage a').attr('href', webBaseUrl + "page/" + dataJson.featureQuiz['Page']['slug']);
				$('#showFeatureQuizQuestion a').text(dataJson.featureQuiz['Quiz']['question_text']);
				$('#showFeatureQuizQuestion a').attr('href', webBaseUrl + "page/" + dataJson.featureQuiz['Page']['slug'] + "/quizzes/view/" + dataJson.featureQuiz['Quiz']['id'] + "/" + dataJson.featureQuiz['Quiz']['slug']);

				if (dataJson.featureQuiz['Quiz']['image_url']) {
					if (dataJson.featureQuiz['Quiz']['is_external_image']) {
						$('#showFeatureQuizQuestionImage > img').attr('src', dataJson.featureQuiz['Quiz']['image_url']);
					} else {
						$('#showFeatureQuizQuestionImage > img').attr('src', webBaseUrl + "img/photos/quizzes/" + dataJson.featureQuiz['Quiz']['id'] + "/" + dataJson.featureQuiz['Quiz']['image_url']);
					}
					$('#showFeatureQuizQuestionImage > img').attr({
							'width' : dataJson.featureQuiz['Quiz']['width'],
							//'height' : dataJson.featureQuiz['Quiz']['height']
					});
					$('#showFeatureQuizQuestionImage').css({
							//'height' : dataJson.featureQuiz['Quiz']['height'],
							'width' : dataJson.featureQuiz['Quiz']['width'],
							'padding-top' : dataJson.featureQuiz['Quiz']['padding_height'] + 'px ',
							'padding-bottom' : dataJson.featureQuiz['Quiz']['padding_height'] + 'px ',
							'padding-right' : dataJson.featureQuiz['Quiz']['padding_width'] + 'px ',
							'padding-left' : dataJson.featureQuiz['Quiz']['padding_width'] + 'px ',
							});
					$('#showFeatureQuizQuestionImage > img').attr('name', webBaseUrl + "page/" + dataJson.featureQuiz['Page']['slug'] + "/quizzes/view/" + dataJson.featureQuiz['Quiz']['id'] + "/" + dataJson.featureQuiz['Quiz']['slug']);
				}
				for (i = 0; i < dataJson.featureQuiz['Quizchoice'].length; i++) {
					dataJson.featureQuiz['Quizchoice'][i]['idx'] = i;
				}
				dataJson.featureQuiz['Quizchoice'].sort(randOrd);
				for (i = 0; i < dataJson.featureQuiz['Quizchoice'].length; i++) {
					$('#showFeatureQuiz' + i + ' > div').text(dataJson.featureQuiz['Quizchoice'][i]['description']);
					$('#showFeatureQuiz' + i).attr('name', dataJson.featureQuiz['Quizchoice'][i]['idx']);
					$('#showFeatureQuiz' + i).attr('style', null);
				}
				$('#showFeatureQuizFormID').attr('action', webBaseUrl + "page/" + dataJson.featureQuiz['Page']['slug'] + "/quizzes/nextQuiz/" + dataJson.featureQuiz['Quiz']['id'] + "/" + dataJson.featureQuiz['Quiz']['slug']);
				$('#showFeatureQuiz').attr('style', null);
			}
		});
	});

	var signUpForm = '@include("partials.signup.register_form")';
	var notLoggedInForm = '@include("partials.signup.notloggedin_form")';
	var LoginForm = '@include("partials.signup.login_form")';
	$('#showFeatureQuizPage a').click(function(event) {
		if (pageSlug == 0) {
			// home page
			$.trackGAEvent('FeaturedQuiz', 'FeaturedQuizWidget.PageName.click', 'Featured Quiz on Main Homepage - Page name was clicked');
		} else {
			$.trackGAEvent('FeaturedQuiz', 'FeaturedQuizWidget.PageName.click', 'Featured Quiz on Page Home - Page name was clicked');
		}
	});

	$('.showFeatureQuizChoice').click(function(event) {
		/* Intercept if user (not logged in) selected enough answers for now */
		if (pageSlug == 0) {
			// home page
			$.trackGAEvent('FeaturedQuiz', 'FeaturedQuizWidget.QuizChoice.click', 'Featured Quiz on Main Homepage - Quiz Choice was clicked');
		} else {
			$.trackGAEvent('FeaturedQuiz', 'FeaturedQuizWidget.QuizChoice.click', 'Featured Quiz on Page Home - Quiz Choice was clicked');
		}

		if (1) {
			event.preventDefault();
			$('.notLoggedIn').click();

		} else {
			$('#submitchoice').attr('name', $(this).attr('name'));
			$('#submitchoice').click();
		}
	});

	$('#showFeatureQuizQuestion').click(function(a) {
		if (pageSlug == 0) {
			// home page
			$.trackGAEvent('FeaturedQuiz', 'FeaturedQuizWidget.QuizQuestion.click', 'Featured Quiz on Main Homepage - Quiz Question was clicked');
		} else {
			$.trackGAEvent('FeaturedQuiz', 'FeaturedQuizWidget.QuizQuestion.click', 'Featured Quiz on Page Home - Quiz Question was clicked');
		}
	});

	$('#showFeatureQuizQuestionImage img').click(function(a) {
		if (pageSlug == 0) {
			// home page
			$.trackGAEvent('FeaturedQuiz', 'FeaturedQuizWidget.Image.click', 'Featured Quiz on Main Homepage - Image was clicked');
		} else {
			$.trackGAEvent('FeaturedQuiz', 'FeaturedQuizWidget.Image.click', 'Featured Quiz on Page Home - Image was clicked');
		}
		window.location = $(this).attr('name');
	});

	$('.showFeatureQuizChoice').hover(function() {
		$(this).addClass('pretty-hover');
	}, function() {
		$(this).removeClass('pretty-hover');
	});

</script>
