<div id="pageMenu">
	<div id="pageThumbnail">
		<a href="<?php echo $this->Html->url('/page/'.$pageSlug) ?>">
			<?php if (isset($pageThumbnailPath)) : ?>
				<img class="pageBannerImgSmall" height="95" width="95" src="<?php echo $pageThumbnailPath; ?>">
			<?php endif ?>
		</a>
	</div>
	<div id="pageActions">
		<h2 id="pageTitleOverlay1" class="pageTitleOverlay"><?php echo $pageName; ?></h2>
		<div id="pageFollowStat">

			<?php if (configure('app.showFollowCounts')) : ?>
			<div id="pageFollowFans">
				<span id="pageFollowCount">
				<?php
					if ($pageFollowCount == 0) {
						echo "Join the community!";
					} else if ($pageFollowCount == 1) {
						echo "Followed by 1 person";
					} else {
						echo "Followed by $pageFollowCount people";
					}
				?>
				</span>
			</div>
			<?php endif; ?>

			<?php if (!empty($pageFollow)): ?>
			<div id="pageFollowBtn" class="btn btnDehighlight" name="<?php echo $pageSlug ?>">Unfollow
			<?php else: ?>
			<div id="pageFollowBtn" class="btn" name="<?php echo $pageSlug ?>"><?php echo $followPageTxt; ?>
			<?php endif; ?>
			</div>
		</div>
	</div>

	<div id="pageBannerCaption">
		<?php
			$caption = $pageCaption;
			if (strlen($pageCaption) > 0) {
		?>
				<div class="message">
					<i class="quoteIcon fa fa-quote-left"></i>
					<?php
						if (strlen($pageCaption) > 300) {
							$caption = substr($pageCaption, 0, 300) . '...';
						}
						echo $caption;
					?>
					<div class="clear"></div>
				</div>
				<!--
				<div class="status_arrow_right"></div>
				-->
		<?php
			}
		?>
	</div>

	<div class="clear"></div>

</div>
<div class="clear"></div>
	<div id="tabbed_box" class="tabbed_box">
		<div class="tabbed_area">
			<div class="tabs">
				<div><a href="<?php echo $this->Html->url('/page/'.$pageSlug) ?>" id="link_home" class="tab active"><div id="home" class="icons home"></div></a></div>

				<div><a href="<?php echo $this->Html->url('/page/'.$pageSlug.'/images') ?>" id="link_images" class="tab"><div id="images" class="icons images"></div></a></div>
	<?php
		if ($numVideos > 0) {
	?>
				<div><a href="<?php echo $this->Html->url('/page/'.$pageSlug.'/videos') ?>" id="link_videos" class="tab"><div id="videos" class="icons videos"></div></a></div>
	<?php
		}
	?>
				<?php if (configure('app.showWall')): ?>
					<div><a href="<?php echo $this->Html->url('/page/'.$pageSlug.'/wall') ?>" id="link_wall" class="tab"><div id="wall" class="icons wall"></div></a></div>
				<?php endif; ?>

				<?php if (configure('app.showForum')): ?>
					<div><a href="<?php echo $this->Html->url('/page/'.$pageSlug.'/forum') ?>" id="link_forum" class="tab"><div id="forum" class="icons forum"></div></a></div>
				<?php endif; ?>
	<?php
		if ($numPicks > 0) {
	?>
				<div><a href="<?php echo $this->Html->url('/page/'.$pageSlug.'/picks') ?>" id="link_picks" class="tab"><div id="picks" class="icons picks"></div></a></div>
	<?php
		}
	?>
	<?php
		if ($numQuizzes > 0) {
	?>
				<div><a href="<?php echo $this->Html->url('/page/'.$pageSlug.'/quizzes') ?>" id="link_quizzes" class="tab"><div id="quizzes" class="icons quizzes"></div></a></div>
	<?php
		}
	?>
			</div>

			<div id="content_home" class="content">
			</div>

			<?php if (configure('app.showWall')): ?>
				<div id="content_wall" class="content">
				</div>
			<?php endif; ?>

			<div id="content_images" class="content">
			</div>
			<div id="content_videos" class="content">
			</div>

			<?php if (configure('app.showForum')): ?>
				<div id="content_forum" class="content">
				</div>
			<?php endif; ?>

			<div id="content_picks" class="content">
			</div>
			<div id="content_quizzes" class="content">
			</div>
		</div>
	</div>

<script type="text/javascript">
//<!--
	function selectTab(id) {
		// switch all tabs off
		$(".active").removeClass("active");

		// switch this tab on
		$("#link_" + id).addClass("active");

		// slide all other tab content up, and this tab's content up
		//$(".content").slideUp();
		//$("#" + id).slideDown();

		// hide all other tab content, and show this tab's content
		$(".content").hide();
		$("#" + "content_" + id).show();
		$("#" + id).addClass(id + 'Click');
	}

	// When the document loads do everything inside here ...
	$(document).ready(function(){
		$("a.tab").click(function () {
			var tabId = $(this).attr("id");
			selectTab(tabId.substring(5,30));
		});
		var activeTab = '<?php echo $activeTab; ?>';
		selectTab(activeTab);
	});

	function removeAllClickClasses() {
		$('.icons').each(function() {
			$(this).removeClass($(this).attr('id') + 'Click');
		});
	}

	$('#home').hover(function() {
		$(this).addClass('homeHover');
	}, function() {
		$(this).removeClass('homeHover');
	});

	$('#images').hover(function() {
		$(this).addClass('imagesHover');
	}, function() {
		$(this).removeClass('imagesHover');
	});

	$('#videos').hover(function() {
		$(this).addClass('videosHover');
	}, function() {
		$(this).removeClass('videosHover');
	});

	$('#picks').hover(function() {
		$(this).addClass('picksHover');
	}, function() {
		$(this).removeClass('picksHover');
	});

	$('#quizzes').hover(function() {
		$(this).addClass('quizzesHover');
	}, function() {
		$(this).removeClass('quizzesHover');
	});

	$('#forum').hover(function() {
		$(this).addClass('forumHover');
	}, function() {
		$(this).removeClass('forumHover');
	});

	$('#wall').hover(function() {
		$(this).addClass('wallHover');
	}, function() {
		$(this).removeClass('wallHover');
	});

	$('#favorites').hover(function() {
		$(this).addClass('favoritesHover');
	}, function() {
		$(this).removeClass('favoritesHover');
	});

	var pageName = "<?php echo $pageName; ?>";
	var followPageTxt = "<?php echo $followPageTxt; ?>";

	// Create a bubble popup for each element with class attribute ".icons"
	// can also set gravity/position using: gravity: 'n' | 'e' | 'w' | 's'
	// for auto-gravity use: gravity: $.fn.tipsy.autoWE  OR gravity: $.fn.tipsy.autoNS
	//$('.icons').tipsy({gravity: $.fn.tipsy.autoNS, fade: false, html: true, title: function() { return 'Home'.toUpperCase(); }});
	$('.icons').tipsy({gravity: 's', fade: true, html: true, title: function() { return toCamelCase(pageName + ' ' + this.getAttribute('id')); }});

	// Logic for Follow / Unfollow
	var pageFollow = <?php echo (empty($pageFollow)) ? 0 : 1 ?>;

	if (!pageFollow) {
		$('#pageFollowBtn').tipsy({gravity: 'e', fade: true, html: true, title: function() { return "Join our community of <?php echo $pageName ?> fans"; }});
	}

	var followBaseUrl = '<?php echo $html->url("/") ?>';
	var userLogged = <?php
			$username = $this->Session->read('Auth.User.username');
			if (empty($username)) {
				echo 0;
			} else {
				echo 1;
			} ?>;

	$('#pageFollowBtn').click(function() {
		var btn = this;
		$.trackGAEvent('Page', 'Page.FollowButton.click', 'Follow Button (next to banner) was clicked on Page home');
		if (userLogged == 1) {
			if ($(btn).hasClass("btnDehighlight")) {
				$.get(followBaseUrl + "page/unfollowPage", {val: $(btn).attr('name')}, function(data) {
					var dataJson = $.parseJSON(data);

					if (dataJson.success == 1) {
						if (dataJson.badge) {
							showRevokedBadge(dataJson.badge);
						}
						$(btn).text(followPageTxt);
						$(btn).removeClass("btnDehighlight");
						$(btn).tipsy({gravity: 'e', fade: true, html: true, title: function() { return "Join our community of <?php echo $pageName ?> fans"; }});

						if (dataJson.newCount <= 0) {
							$('#pageFollowCount').text('Join the community!');
						} else if (dataJson.newCount == 1) {
							$('#pageFollowCount').text('Followed by ' + dataJson.newCount + ' person');
						} else {
							$('#pageFollowCount').text('Followed by ' + dataJson.newCount + ' people');
						}
						/* Synchronize with other follow button (if existed) */
						if ($('#followDiv > #followBtn').length > 0) {
							$('#followDiv > #followBtn').text(followPageTxt);
							$('#followDiv > #followBtn').removeClass("btnDehighlight");

							if (dataJson.newCount <= 0) {
								$('#followDiv > #followFans > #followFanCount').text('Join the community!');
							} else if (dataJson.newCount == 1) {
								$('#followDiv > #followFans > #followFanCount').text('Followed by ' + dataJson.newCount + ' person');
							} else {
								$('#followDiv > #followFans > #followFanCount').text('Followed by ' + dataJson.newCount + ' people');
							}
						}
					}
				});
			} else {
				$.get(followBaseUrl + "page/followPage", {val: $(btn).attr('name')}, function(data) {
					var dataJson = $.parseJSON(data);

					if (dataJson.success == 1) {
						if (dataJson.badge) {
							showAwardedBadge(dataJson.badge);
						}
						$(btn).text('Unfollow');
						$(btn).addClass("btnDehighlight");

						// hack: cancel the tipsy popup, use undefined setting for gravity
						$(btn).tipsy({gravity: 'x', fade: true, html: true, title: function() { return null; }});

						if (dataJson.newCount <= 0) {
							$('#pageFollowCount').text('Join the community!');
						} else if (dataJson.newCount == 1) {
							$('#pageFollowCount').text('Followed by ' + dataJson.newCount + ' person');
						} else {
							$('#pageFollowCount').text('Followed by ' + dataJson.newCount + ' people');
						}
						if ($('#followDiv > #followBtn').length > 0) {
							$('#followDiv > #followBtn').text('Unfollow');
							$('#followDiv > #followBtn').addClass("btnDehighlight");

							if (dataJson.newCount <= 0) {
								$('#followDiv > #followFans > #followFanCount').text('Join the community!');
							} else if (dataJson.newCount == 1) {
								$('#followDiv > #followFans > #followFanCount').text('Followed by ' + dataJson.newCount + ' person');
							} else {
								$('#followDiv > #followFans > #followFanCount').text('Followed by ' + dataJson.newCount + ' people');
							}
						}
					}
				});
			}
		} else {
			$('.Login').click();
			//window.location = followBaseUrl + 'register/';
		}
	});

//-->
</script>
