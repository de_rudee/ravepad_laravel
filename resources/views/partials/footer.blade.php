<?php
	$domain_name = config('app.domain');
	$domain_url = config('app.domainUrl');
	$copyright_start_date = config('app.copyrightStartDate');
?>
<script type="text/javascript" src={{asset('js/bubble_tooltips/BubbleTooltips.js')}}></script>
<div id="bottomBar"></div>
<div id="footer-inner">
	<div>
		<p>
			<div id="copyright">
<?php
				//echo "&copy; ".$copyright_start_date."-".date('Y').'&nbsp;&nbsp;';
				echo "&nbsp;&nbsp;&nbsp;&copy; ".date('Y').'&nbsp;&nbsp;';
				$tooltiptxt = 'Ravepad: The place to rave about anything and everything!';
				$tooltipftr = '';
				echo 'copyright text';
?>
				&nbsp;All Rights Reserved.
			</div>
			<div id="terms">
<?php
				echo "&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;";
				$tooltiptxt = 'We take your privacy seriously';
				$tooltipftr = 'Read further..'; ?>
				<a href={{asset("privacy")}} target="_blank" title="{{$tooltiptxt}}">Privacy</a>

<?php				echo "&nbsp;&nbsp; |&nbsp;&nbsp; ";

				$tooltiptxt = 'Terms and conditions';
				$tooltipftr = ''; ?>
			  <a href={{asset("terms")}} target="_blank" title="{{$tooltiptxt}}">Terms</a>
<?php
				echo "&nbsp;&nbsp; | &nbsp;&nbsp; ";
				// For Google Author information in search results. See: http://support.google.com/webmasters/bin/answer.py?hl=en&answer=1408986
				$tooltiptxt = 'Never miss an update! Circle Ravepad on Google+';
				$tooltipftr = ''; ?>
				<a href="https://plus.google.com/104999760378793178439?rel=author" target="_blank" title="{{$tooltiptxt}}">Ravepad on Google+</a>
<?php
				// End: For Google Author information in search results
?>
			</div>
		</p>

	</div>
</div><!-- footer-inner -->

<div id="FB_HiddenContainer" style="position:absolute; top:-10000px; width:0px; height:0px;" ></div>

<style type="text/css">
	#fb_sliver {
		position: fixed; bottom: 0; left:0; width: 100%; z-index: 10000; height: 1px; overflow:hidden; text-align: center; color: #FFF; font-size: 18px; font-family: Helvetica; background-color: rgb(0,0,0);
		ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=60)"; /* internet explorer 8 */
		filter: alpha(opacity=60); /* internet explorer 5~7 */
		-khtml-opacity: 0.6;      /* khtml, old safari */
		-moz-opacity: 0.6;       /* mozilla, netscape */
		opacity: 0.6;           /* fx, safari, opera */
	}
	#fb_sliver_wrapper { left: 50%; margin-left: -490px; position: absolute; }
	#fb_sliver_close { cursor:pointer; float: left; width: 20px; display: inline; margin: 5px 0 0 30px; }
	#fb_sliver_text { float: left; display: inline; margin: 0 10px 0 0; line-height: 60px; min-width: 680px; }
	#fb_sliver_fan_box { width: 230px; height: 60px; float: left; display: inline;}
</style>

<?php if (config('showFacebookSilverInFooter')): ?>
	<div id="fb_sliver">
		<div id="fb_sliver_wrapper">
			<div class="Frame" style="background: transparent;">
				<div id="fb_sliver_text">&nbsp;</div>
				<div id="fb_sliver_fan_box">&nbsp;</div>
				<div id="fb_sliver_close">
					<img src={{asset("img/footer/fb_sliver_close_button.gif")}} alt="" title="Close">
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
