<div id="imagesRecent" style="display:none;">
	<div id="imagesRecentTitle">
		<div id="imagesRecentTitleImage"></div>
		<div id="imagesRecentTitleText">Random Images</div>
	</div>
	<div id="imagesRecentArea">
		<?php
			if (empty($pageSlug)) {
				$max = config('minRecentImagesWidget');
			} else {
				$max = config('minRecentImagesWidget');
			}
			for($i = 0; $i< $max; $i++):
		?>
				<div id="imagesRecentEntry<?php echo $i;?>" class="imagesRecentEntry" style="display:none;">
					<div class="imagesRecentEntryPicture">
						<a href=""><img width="100" height="100" alt="" src=""></a>
					</div>
					<div class="imagesRecentEntryText"><a href=""></a></div>
				</div>
		<?php
			endfor;
		?>
	</div>
	<div id="imagesRecentMore"><?php if (!empty($pageSlug)) { echo $this->Html->link('more ' . $pageName . ' images >>', "/page/$pageSlug/images");} ?></div>
</div>

<script type="text/javascript">
	var pageSlug = '<?php echo empty($pageSlug) ? 0 : $pageSlug ?>';
	var imageModelName = '<?php echo empty($imageModelName) ? 'Image' : $imageModelName ?>';
	var registerCtrBaseUrl = '/register';
	var userLogged = <?php
			if(Auth::user()){
				echo 1;
			}
			else{
				echo 0;
			}
			?>;
	var minImages = <?php
			if (empty($pageSlug)) {
				echo config('minRecentImagesFrontpage');
			} else {
				echo config('minRecentImagesWidget');
			} ?>;

	$('#imagesRecent').ready(function() {
		var Ajaxurl = "";
		if (pageSlug != 0) {
			Ajaxurl = webBaseUrl + "images/recentImages/" + pageSlug;
		} else {
			Ajaxurl = webBaseUrl + "images/recentImages";
		}
		$.get(Ajaxurl, null, function(data) {
			var dataJson = $.parseJSON(data);

			if (dataJson.imageList && dataJson.imageList.length >= minImages) {
				for (i = 0; i < dataJson.imageList.length; i++) {
					var imgSrc;
					if (dataJson.imageList[i][imageModelName]['is_external_image'] == 1) {
						imgSrc = dataJson.imageList[i][imageModelName]['url'];
					} else {
						imgSrc = webBaseUrl + 'img/photos/images/' + dataJson.imageList[i]['Page']['id'] + '/' + dataJson.imageList[i][imageModelName]['url'];
					}
					$('#imagesRecentEntry' + i + ' > .imagesRecentEntryPicture > a > img').attr('src', imgSrc);
					$('#imagesRecentEntry' + i + ' > .imagesRecentEntryPicture > a > img').attr({
						'width' : dataJson.imageList[i][imageModelName]['width'],
						'height' : dataJson.imageList[i][imageModelName]['height'] });
					$('#imagesRecentEntry' + i + ' > .imagesRecentEntryPicture').css({
						'height' : dataJson.imageList[i][imageModelName]['height'],
						'width' : dataJson.imageList[i][imageModelName]['width'],
						'padding-top' : dataJson.imageList[i][imageModelName]['padding_height'] + 'px ',
						'padding-bottom' : dataJson.imageList[i][imageModelName]['padding_height'] + 'px ',
						'padding-right' : dataJson.imageList[i][imageModelName]['padding_width'] + 'px ',
						'padding-left' : dataJson.imageList[i][imageModelName]['padding_width'] + 'px ',
						});
					$('#imagesRecentEntry' + i + ' > .imagesRecentEntryPicture > a').attr('href', webBaseUrl + 'page/' + dataJson.imageList[i]['Page']['slug'] + '/images/view/' + dataJson.imageList[i][imageModelName]['id'] + "/" + dataJson.imageList[i][imageModelName]['slug']);
					$('#imagesRecentEntry' + i + ' > .imagesRecentEntryText > a').attr('href', webBaseUrl + 'page/' + dataJson.imageList[i]['Page']['slug'] + '/images/view/' + dataJson.imageList[i][imageModelName]['id'] + "/" + dataJson.imageList[i][imageModelName]['slug']);
					$('#imagesRecentEntry' + i + ' > .imagesRecentEntryText > a').text(dataJson.imageList[i][imageModelName]['title']);
					$('#imagesRecentEntry' + i).attr('style', null);
				}
				$('#imagesRecent').attr('style', null);
			}
		});
	});

	$('.imagesRecentEntry').click(function() {
		window.location = $(this).children('.imagesRecentEntryText').children('a').attr('href');
	});

</script>
