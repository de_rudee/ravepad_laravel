<?php
	$deactivationArray = Configure::read('DEACTIVATION_DAYS_ARRAY');

	// last entry in the array, when account deactivation will happen
	$deactivation_days = $deactivationArray[count($deactivationArray)-1];
?>

<div id="reminderBar" style="display:none">
	<div id="reminderText">
		You need to activate your account within <?php echo $deactivation_days; ?> days of signing up or it will be deactivated. Click on the link in your email, or
<?php
		echo $html->link(__('have it sent again.', true), '/activate',
			array(
			//'target' =>'_blank',
			'title' => 'Resend activation email',
			)
		);
?>
	</div>
	<div id="reminderCloseBox">
		 <?php
			echo $this->Html->image("footer/fb_sliver_close_button.gif", array(
					"alt" => 'X',
					"title" => 'dismiss this notification',
					"height" => '15px',
					"style" => 'cursor: pointer;'
				));
		  ?>
	</div>
</div>
<div id="reminderBarBuffer" style="display:none"></div>

<script type="text/javascript">
	if ((jQuery.cookie("rp_id[activate_bar]") == null)) {
		$('#reminderBar').slideDown();
		$('#reminderBarBuffer').slideDown();
	}

	$('#reminderCloseBox').click(function() {
		$('#reminderBar').slideUp();
		$('#reminderBarBuffer').slideUp();
		jQuery.cookie("rp_id[activate_bar]", 1, { path: '/', expires: 7 });
	});
</script>
