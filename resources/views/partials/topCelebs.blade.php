<div id="showTopCelebs" style="display:none;">
	<div id="showTopCelebsTitle">
		<div id="showTopCelebsTitleImage"></div>
		<div id="showTopCelebsTitleText">Trending right now..</div>
	</div>
	<div id="showTopCelebsArea">
		<?php for($i = 0; $i < 8; $i++): ?>
			<div id="showTopCelebsEntry<?php echo $i?>" class="showTopCelebsEntry" style="display:none;">
				<div class="showTopCelebsEntryImage"><a href=""><img width=100 height=100 class="circular" src=""></a></div>
				<div class="showTopCelebsEntryText"><a href=""></a></div>
				<div class="showTopCelebsEntryFollow btn" style="display:none">+</div>
			</div>
		<?php endfor; ?>
	</div>
</div>

<script type="text/javascript">
	$(".showTopCelebsEntry").mouseenter(function(){$(this).find("img").removeClass('circular');});
	$(".showTopCelebsEntry").mouseleave(function(){$(this).find("img").addClass('circular');});

	var userLogged = <?php
	if(Auth::user()){
		echo 1;
	}
	else{
		echo 0;
	}
	$followPageTxt="Follow Page Text";
	$unfollowPageTxt="Unfollow Page Text";
	?>;
	var followPageTxt = "<?php echo $followPageTxt; ?>";
	var unfollowPageTxt = "<?php echo $unfollowPageTxt; ?>";

	$('#showTopCelebs').ready(function() {
		$.get(webBaseUrl + "pages/getTopCelebs", null, function(data) {
			var dataJson = $.parseJSON(data);

			for (i = 0; i < dataJson.pageData.length; i++) {
				var pageName = dataJson.pageData[i]['Page']['name'];
				$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryText > a').text(pageName);
				$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryText > a').attr('href', webBaseUrl + 'page/' + dataJson.pageData[i]['Page']['slug']);
				var thumbUrl;
				if (dataJson.pageData[i]['Page']['thumbnail_url']) {
					thumbUrl = webBaseUrl + 'img/photos/pages/' + dataJson.pageData[i]['Page']['id'] + '/' + dataJson.pageData[i]['Page']['thumbnail_url'];
				} else {
					thumbUrl = webBaseUrl + 'img/photos/banners/' + dataJson.pageData[i]['Page']['id'] + '/icon.jpg';
				}
				$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryImage > a > img').attr('src', thumbUrl);
				$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryImage > a > img').attr({
						'width' : dataJson.pageData[i]['Page']['width'],
						'height' : dataJson.pageData[i]['Page']['height'] });
				$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryImage').css({
						'height' : dataJson.pageData[i]['Page']['height'],
						'width' : dataJson.pageData[i]['Page']['width'],
						'padding-top' : dataJson.pageData[i]['Page']['padding_height'] + 'px ',
						'padding-bottom' : dataJson.pageData[i]['Page']['padding_height'] + 'px ',
						'padding-right' : dataJson.pageData[i]['Page']['padding_width'] + 'px ',
						'padding-left' : dataJson.pageData[i]['Page']['padding_width'] + 'px ',
						});
				$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryImage > a').attr('href', webBaseUrl + 'page/' + dataJson.pageData[i]['Page']['slug']);
				$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryFollow').attr('name', dataJson.pageData[i]['Page']['id']);
				if (dataJson.user) {
					$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryFollow').attr('style', null);
				}
				$('#showTopCelebsEntry' + i).attr('style', null);
				$('#showTopCelebsEntry' + i + ' > .showTopCelebsEntryFollow').tipsy({gravity: 's', fade: true, html: true, title: function() { return toCamelCase(followPageTxt); }});
			}
			$('#showTopCelebs').attr('style',null);
		});
	});

	$('.showTopCelebsEntryFollow').click(function() {
		var btn = this;

		if (userLogged == 1) {
			if ($(btn).hasClass("showTopCelebsEntryAdd")) {
				$.get(webBaseUrl + "page/unfollowPage", {val: $(btn).attr('name')}, function(data) {
					var dataJson = $.parseJSON(data);

					if (dataJson.success == 1) {
						$(btn).text('+');
						$(btn).removeClass("showTopCelebsEntryAdd");
						$(btn).tipsy({gravity: 's', fade: true, html: true, title: function() { return toCamelCase(followPageTxt); }});
					}
				});
			} else {
				$.get(webBaseUrl + "page/followPage", {val: $(btn).attr('name')}, function(data) {
					var dataJson = $.parseJSON(data);

					if (dataJson.success == 1) {
						$(btn).text('-');
						$(btn).addClass("showTopCelebsEntryAdd");
						$(btn).tipsy({gravity: 's', fade: true, html: true, title: function() { return toCamelCase(unfollowPageTxt); }});
					}
				});
			}
		} else {
			window.location = webBaseUrl + 'register/';
		}
	});
</script>
