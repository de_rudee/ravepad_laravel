@extends('layouts.default')

@section('title','Ravepad - the place to rave about anything and everything!')

@section('seo')
<META NAME="description" CONTENT="Ravepad is a network of clubs for fans of celebrities to discover and share photos, answer quizzes, take polls, and discuss news and opinions with fellow fans. It is THE place to rave about anything and everything!">
<META NAME="keywords" CONTENT="fan pages, fan clubs, fanclubs, fans, fansite, celebrities, entertainment, sharing, pictures, photos, polls, quizzes, wallpapers, news, articles, links, headlines, icons, fan art, trivia, quiz, community, music, television, RavePad">
@endsection

@section('headScripts')
<link rel="stylesheet" type="text/css" href={{asset('css/widgets.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/pageCommon.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/infos/home.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/infos/what_is.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/quizzes/quiz_feature.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/pages/page_suggestion.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/images/images_recent.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/tipsy/tipsy.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/pages/spotlight.css')}}>
<link rel="stylesheet" type="text/css" href={{asset('css/picks/picks_interesting.css')}}>
@endsection
@section('content')
<div id="" class="onepcssgrid-1200">
  <div class="onerow">
    <div class="col4 last">
      <div id="whatIs" class="widget">
        <div id="whatIs">
          <div id="whatIsTitle">
            <!--<div id="whatIsImage"></div>-->
            <div id="whatIsTitleText">What is {{ config('app.name') }}?</div>
          </div>
          <div id="whatIsDescription">
            {{ config('app.name') }} is a collection of communities on any topic: people, celebrities, movies, music, places, travel, cars, sports and more!
          </div>

          <div id="joinBtn" class="btn join callToActionBtn">Join for free!</div>
        </div>

        <script type="text/javascript">
        $('#joinBtn').click(function() {
          $.trackGAEvent('WhatIsWidget', 'WhatIsWidget.Btn.click', 'WhatIsWidget on Homepage - Join Button was clicked');
          $('.Login').click();
        });
        </script>
      </div>
      <div id="featureQuiz" class="quizWidget">
        @include('partials.quizfeature')
      </div>

      @include('partials.ads.ad_medium_rectangle_336_280')

      <div id="pageSuggestions" class="widget">
        @include('partials.pagesuggestion')
      </div>
      <div id="facebookLike" class="widget">
        @include('partials.facebookplugin')
      </div>
      <div id="recentImages" class="widget">
        @include('partials.imagesrecent')
      </div>
    </div>

    <div class="col8">
      <div id="topCelebs">
        @include('partials.topCelebs')
      </div>

      <?php if (config('app.showAds')) : ?>
        <div class="ad_leaderboard_728_90">
          @include('partials.ads.ad_leaderboard_728_90_2')
        </div>
      <?php endif; ?>

      <div id="spotlight">
        @include('partials.spotlight')
      </div>
      <div id="interestingPicks">
        @include('partials.picksinteresting')
      </div>

      <div id="taboolaArticleAd">
        @include('partials.ads.taboola.taboolaArticle')
      </div>
      @include('partials.ads.ad_spreadshirt')
    </div>
  </div>
</div>

@endsection
