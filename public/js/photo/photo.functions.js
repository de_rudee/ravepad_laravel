$(function(){

var uploadSrc;
var uploadDest;

var uploadDialog = $('#uploadDialog')
	.dialog({
		autoOpen: false,
		modal: true,
		height: 'auto',
		width: 'auto',
		resizable:true,
		draggable: false,
		title: 'Upload Image',
		closeOnEscape: true
	});

	// post-submit callback
	function showResponse(responseText, statusText, xhr, $form) {
		// for normal html responses, the first argument to the success callback 
		// is the XMLHttpRequest object's responseText property 

		// if the ajaxSubmit method was passed an Options Object with the dataType 
		// property set to 'xml' then the first argument to the success callback 
		// is the XMLHttpRequest object's responseXML property 

		// if the ajaxSubmit method was passed an Options Object with the dataType 
		// property set to 'json' then the first argument to the success callback 
		// is the json data object returned by the server 

		var relativePath = "../../../";
		if (responseText['level']) {
			relativePath = "../";
			// TODO: Fix this to a more general path.
		}

		if (responseText['name']['error'] != undefined) {
			$('<div class="imageError">' + responseText['name']['error'] + '</div>').insertAfter('#' + uploadSrc);
			$('.imageError').css({
				'float' : 'left',
				'background' : '#FFC0C0',
			});
			$('#' + uploadSrc).attr("src", "");
		} else {
			$('#' + uploadSrc).attr("src", relativePath + "img/photos/" + responseText['type'] + "/tmp/" + responseText['name']);
			$('#' + uploadDest).attr("value", responseText['name']);
		}
	}

	// upload more photos handler
	$('.uploadPhoto').click(function(){
		$('#uploadDialog').html(uploadPhotoForm);
		$('#uploadDialog').dialog('open');
		uploadSrc = this.attributes['src'].value;
		uploadDest = this.attributes['dest'].value;
		
		/* Check if there's an imageError div. If so, remove it. */
		if ($('.imageError')) {
			$('.imageError').remove();
		}
		
		$('#submitUploadPhotoForm').click(function() {
		
			if (!$('#PhotoFile:file').val() && !$('#uploadPhotoFormUrl').val()) {
				$('#uploadPhotoError').attr('style', '');
				$('#uploadPhotoError').css('background', '#FFC0C0');
				$('#uploadPhotoError').text(" Please either upload a file or input a URL.");
			} else {
				var options = {
					dataType: 'json',		// 'xml', 'script', or 'json'
					success: showResponse	// post-submit callback
				};
				$("#uploadPhotoForm").ajaxSubmit(options);
				$('#uploadDialog').dialog('close');
				$('#' + uploadSrc).attr("src", '../../../img/spinners/spin.gif');
				$('#' + uploadSrc).attr("style", "");
			}
			return false;
		});
	});
});
