function GoogleMap(vertextNum, defaultRadius, imagesBaseUrl)
{
    this.imagesBaseUrl = imagesBaseUrl;
    this.vertexNum = vertextNum;
    this.defaultRadius = defaultRadius;
    this.requestActive = 0;       // flag tells that request to google is runing
    this.geocodeCache = {};       // cache of geocoder requests

    // map markers
    this.centerMarker = null;
    this.searchAreaMarkers = [];
    this.searchArea=null;          // google polyline object
    this.shrinkMarker = null;
    this.searchRadius = this.defaultRadius;

    // address that matches with center point on map, if it present (not null)
    // then it means all is in valid state and can be saved in form
    this.resultAddress = null;
    
    // math const
    this.toRadConst = Math.PI/180 ;
    this.earthRadius = 3963; // miles
    
    
    /**
     * Builds ideal polygon with 12 sides, which is in drawn into specified circle,
     * returns coords of its vertexes
     * @param center google.maps.LatLng object
     * @param radius in miles
     * @retruns array of google.maps.LatLng objects
     */
    this.getPolyVertexCoords = function(center, radius)
        {
            var d2r = Math.PI / 180;
            circleLatLngs = new Array();
        
            // Convert statute miles into degrees latitude
            var circleLat = radius * 0.014483;
            var circleLng = circleLat / Math.cos(center.lat() * d2r);
        
            // Create polygon points (extra point to close polygon)
            for (var i = 0; i < this.vertexNum+1; i++) { 
                // Convert degrees to radians
                var theta = Math.PI * (i / (this.vertexNum / 2)); 
                var vertexLat = center.lat() + (circleLat * Math.sin(theta)); 
                var vertexLng = center.lng() + (circleLng * Math.cos(theta));
                var vertextLatLng = new google.maps.LatLng(vertexLat, vertexLng);
                circleLatLngs.push(vertextLatLng); 
            }

            circleLatLngs.splice(this.vertexNum, 1);
            return circleLatLngs;
            
            /*
            var center_lat = center.lat();
            var center_lng = center.lng();

            // number of sides of the polygon
            var angle_between_corners = 2*Math.PI/this.vertexNum;
            var dLat = (radius/this.earthRadius)/this.toRadConst; // radius length in latitude graduses
            var dLng = dLat*Math.cos(center_lat*this.toRadConst); // radius length in longtitude graduses

            var coords = [];
            
            for (var i=0; i < this.vertexNum; i++)
            {
                var pointDLat = dLat * Math.cos(i * angle_between_corners);
                var pointDLng = dLng * Math.sin(i * angle_between_corners);
                coords[i] = new google.maps.LatLng(center_lat + pointDLat, center_lng + pointDLng);
            }

            return coords;
            */
        }

    /**
     * Draws polygon on map (markers).
     * @param center center coords object
     * @param coords array of objects
     */
    this.drawPolygonOnMap = function(center, coords)
    {
        /*
        // remove markers if they set
        if (this.centerMarker)
        {
            this.centerMarker.setMap(null);
            for(var i=0; i<this.polygonMarkers.length; i++)
            {
                this.searchAreaMarkers[i].setMap(null);
            }

            this.searchArea.setMap(null);
            this.centerMarker.setMap(null);
        }
        */

		var polyMarkerOptions = {
			map: this.map,
			draggable: true,
			clickable: false,
			icon: self.imagesBaseUrl + 'mm_20_3d_red.png',
			shadow: self.imagesBaseUrl + 'mm_20_shadow.png'
		};

        
        if (!this.centerMarker)
        {
            // create new markers, polygon first
            for(var i=0; i<coords.length; i++)
            {
                polyMarkerOptions.position = coords[i];
                polyMarkerOptions.id = i;
                polyMarkerOptions.title = '#'+i+' Drag to change search area shape';
                this.searchAreaMarkers[i] = new google.maps.Marker(polyMarkerOptions);

                // add drag listener
                google.maps.event.addListener(this.searchAreaMarkers[i], "dragend", function(newPos) {
                                                  /*
                                                  // move polygon vertex
                                                  
                                                  var path = self.searchArea.getPath();
                                                  path.setAt(this.id, newPos.latLng);
                                                  self.searchArea.setPath(path);
                                                  */

                                                  // calculate new radius
                                                  var radius = self.calculateDistance(newPos.latLng, self.centerMarker.getPosition());
                                                  
                                                  // calculate new polygon points
                                                  var coords = self.getPolyVertexCoords(self.centerMarker.getPosition(), radius);
                                                  // show it
                                                  self.drawPolygonOnMap(self.centerMarker.getPosition(), coords);

                                                  // remove pick best match if any
                                                  self.bestMatchRemoveList();
                                              });
            }

            // draw line
            this.searchArea = new google.maps.Polygon({
                  paths: coords,
                        strokeColor: "#FF0000",
                        strokeOpacity: 0.9,
                        strokeWeight: 2,
                        fillColor: "#FF0000",
                        fillOpacity: 0.4,
                        map:this.map
                        });

            // create center marker
            this.centerMarker = new google.maps.Marker({
                        map: this.map,
                        draggable: true,
                        clickable: false,
                        position:center
                        });
                
            // add drag listener
            google.maps.event.addListener(this.centerMarker, "dragstart", function(curPos) {
                                              // remeber surrent pos of center
                                              self._curCenterPos = curPos.latLng;
                                          });

            google.maps.event.addListener(this.centerMarker, "dragend", function(newPos) {
                                              // calculate deltas
                                              var dLat = newPos.latLng.lat()-self._curCenterPos.lat();
                                              var dLng = newPos.latLng.lng()-self._curCenterPos.lng();
                                              
                                              // move polygon
                                              var path = self.searchArea.getPath();
                                              
                                              path.forEach(function(element, index){
                                                               // move vertex
                                                               var newPos = new google.maps.LatLng(element.lat()+dLat, element.lng()+dLng)
                                                               path.setAt(index, newPos);
                                                               //move marker
                                                               self.searchAreaMarkers[index].setPosition(newPos);
                                                                   });
                                              self.searchArea.setPath(path);
                                              self.reverseGeocode(newPos.latLng);
                                              self.rezoomToGood();
                                          });
            
        }
        else
        {
            // move existing markers
            for(var i=0; i<coords.length; i++)
            {
                this.searchAreaMarkers[i].setPosition(coords[i]); 
            }

            // draw polygon
            this.searchArea.setPath(coords);

            // move center marker
            this.centerMarker.setPosition(center);
        }

        // move map
        this.map.setCenter(center);
        this.rezoomToGood();

        // update form fields
        this.calculateRadius();
    }


    /**
     * Calculates value for radius field based on current markers values,
     * updates radius field
     */
    this.calculateRadius = function()
    {
        var radius = 0;
        for(var i=0; i<this.searchAreaMarkers.length; i++)
        {
            var distance = this.calculateDistance(this.searchAreaMarkers[i].getPosition(), this.centerMarker.getPosition());
            if (distance>radius)
            {
                radius = distance;
            }
        }

        // round
        radius = Math.round(radius);

        // posibly truncate
        if (radius>maxAllowedRadius)
        {
            radius = maxAllowedRadius;
            this.showNotificationToUser('Radius can be maximum '+maxAllowedRadius+' miles');
        }

        this.searchRadius = radius;
        // update field
        this.setRadiusFormField(radius);
    }

    /**
     * Calculates minimum bounding rectangle for given array of points
     * @returns google.maps.LatLngBounds object presenting minimum bounding rectangle
     */
    this.getMBR = function(points)
    {
        var minx = points[0].lat() ;
        var miny = points[0].lng() ;
        var maxx = points[0].lat() ;
        var maxy = points[0].lng() ;

        for(var i = 1; i < points.length ; i++ )
        {
            if ( points[i].lat() > maxx )
            {
                maxx = points[i].lat() ;
            }
            if ( points[i].lat() < minx )
            {
                minx = points[i].lat() ;
            }
            if ( points[i].lng() > maxy )
            {
                maxy = points[i].lng() ;
            }
            if ( points[i].lng() < miny )
            {
                miny = points[i].lng() ;
            }
        }

        return ( new google.maps.LatLngBounds(new google.maps.LatLng(maxx,miny), new google.maps.LatLng(minx,maxy)) ) ;
    }


    /**
     * Calculates distance in miles between 2 points, points are
     * google.map.LatLng objects
     */
    this.calculateDistance = function (point1, point2)
        {
            var dLat = (point2.lat()-point1.lat())*this.toRadConst;
            var dLon = (point2.lng()-point1.lng())*this.toRadConst; 
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(point1.lat()*this.toRadConst) * Math.cos(point2.lat()*this.toRadConst) * 
            Math.sin(dLon/2) * Math.sin(dLon/2); 
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            return this.earthRadius * c;
        }


    /**
     * Rezooms map so it includes all points + some small surrounding area
     * event safe function
     */
    this.rezoomToGood = function()
    {
        // calculate mbr
        var mbrBounds = self.getMBR(self.searchArea.getPath().getArray());
        
        // add shrink marker
        if (!self.shrinkMarker)
        {
            self.shrinkMarker = new google.maps.Marker({
                  map: self.map,
                        draggable: true,
                        clickable: false,
                        position:mbrBounds.getNorthEast(),
                        icon: self.imagesBaseUrl+'4_directions.gif'
                        });
            
            // add drag listener
            google.maps.event.addListener(self.shrinkMarker, "dragstart", function(curPos) {
                                              // remember surrent pos of center
                                              self._curShrinkMarkerPos = curPos.latLng;
                                          });

            google.maps.event.addListener(self.shrinkMarker, "dragend", function(newPos) {
                                              // calculate distance
                                              var oldDistance = self.calculateDistance(self._curShrinkMarkerPos, self.centerMarker.getPosition());
                                              var newDistance = self.calculateDistance(newPos.latLng, self.centerMarker.getPosition());
                                              
                                              // adjust radius
                                              self.searchRadius += (newDistance-oldDistance);
                                              self.setRadiusFormField(self.searchRadius);

                                              // redraw polygon
                                              var coords = self.getPolyVertexCoords(self.centerMarker.getPosition(), self.searchRadius);
                                              self.drawPolygonOnMap(self.centerMarker.getPosition(), coords);

                                              // remove pick best match if any
                                              self.bestMatchRemoveList();
                                          });
        }
        else
        {
            // move shrink marker to ne corner
            self.shrinkMarker.setPosition(mbrBounds.getNorthEast());
        }
            
        // rezoom
        self.map.fitBounds(mbrBounds);
    }
    

    this.setRadiusFormField = function(value){

        // reset error message
        formValidationNotify({status: 'pass', field: $('#mapDialogRadius')});

        $('#mapDialogRadius').val(value);
    }
    
    
    /**
     * Performs geocoding request and shows list of possible locations so user
     * can select one of them. If there is one choise just select it and moves
     * map accordingly
     */
    this.showAddressOnMap = function(address, radius, successCallback){

        // reset result address
        this.resultAddress = null;

        // validate fields
        var error = false;

        if (!address)
        {
            error = true;
            formValidationNotify({field: $('#mapDialogAddress'), status: 'fail', prompt: 'Please enter correct address'});
        }
        else
        {
            // remove error message
            formValidationNotify({field: $('#mapDialogAddress'), status: 'pass'});
        }
        
        if (+radius != parseFloat(radius) || radius<=0)
        {
            // error
            formValidationNotify({field: $('#mapDialogRadius'), status: 'fail', prompt: 'Please enter correct radius, it should be greater 0'});
            error = true;
        }
        else
        {
            // remove error message
            formValidationNotify({field: $('#mapDialogRadius'), status: 'pass'});
        }

        if (error)
        {
            return false;
        }

        // perform request
        this.getAddressCoords(address, function(results){

                                  // show best match list, and redwraw polygon
                                  // if there is single match
                                  if(!self.bestMatchShowList(results, true))
                                  {
                                      // it is invalid address show error message
                                      formValidationNotify({field: $('#mapDialogAddress'), status: 'fail', prompt: 'Address you entered is invalid. Please correct it. Try to enter in format: city, state, country'});
                                  }
                                  else
                                  {
                                      if (typeof successCallback != 'undefined')
                                      {
                                          successCallback();
                                      }
                                  }
                              });
    }


    /**
     * Called after user clicked on map, performs reverse geolocation and if
     * address is single, assigns it in address box. Otherwise shows pick best
     * match dialog or error message if adress is invalid
     * @param possible LatLng object position of which we want to know
     */
    this.reverseGeocode = function(position){

        // reset result address
        this.resultAddress = null;
        
        this.geocoder.geocode( { latLng: position, language: 'en'}, function(results, status) {
                                   if (status == google.maps.GeocoderStatus.OK || google.maps.GeocoderStatus.ZERO_RESULTS)
                                   {
                                       self.filterGeocodeResults(results, function(filteredList){
                                                                     // show best match list
                                                                     if(!self.bestMatchShowList(filteredList))
                                                                     {
                                                                         // it is invalid location show error message
                                                                         formValidationNotify({field: $('#mapDialogAddress'), status: 'fail', prompt: 'The location selected is not valid.'});
                                                                     }
                                                                 });
                                  }
                                  else
                                  {
                                      alert("Geocode was not successful for the following reason: " + status);
                                  }
                                  
                              });
    }
        
    
    /**
     * Async function
     * Returns address coords using google geo location. 
     * @param address string representing search address
     * @param callback function to call then results will be available, callback
     * has one parameter it is list of filtered results from google
     * filtered results is array of objects {country: , city: , region: ,
     * formattedAddress: , position: LatLng, cityId -- id in local db, coutryId, regionId}
     * which present list of results from google, list may be empty. It means error
     */
    this.getAddressCoords = function(address, callback)
        {
            this.requestActive++;
            var self = this;

            // check cache
            if (typeof this.geocodeCache[address] != 'undefined')
            {
                // cache has necessary value, return it in callback
                callback(this.geocodeCache[address]);
                this.requestActive--;
                return;
            }
            
            this.geocoder.geocode( { 'address': address, language: 'en'}, function(results, status) {
                                  if (status == google.maps.GeocoderStatus.OK && results.length)
                                  {
                                      // if results not empty,filter them through server
                                      if (results.length>0)
                                      {
                                          self.filterGeocodeResults(results, function(filteredList){

                                                                        // end our function work
                                                                        self.getAddressCoordsComplete(address, filteredList, callback);
                                                                    });
                                      }
                                      else
                                      {
                                          self.getAddressCoordsComplete(address, [], callback);
                                      }
                                  }
                                  else
                                  {
                                      //alert("Geocode was not successful for
                                      //the following reason: " + status);
                                      // end work without filtering
                                      self.getAddressCoordsComplete(address, [], callback);
                                  }
                              });
        }


    /**
     * Second part of getAddressCoords
     */
    this.getAddressCoordsComplete = function(address, results, callback)
    {
        // write in cache
        self.geocodeCache[address] = results;
        // call callback
        callback(self.geocodeCache[address]);
        // request ended
        self.requestActive--;
    }

    
    /**
     * Compares addresses with local database, removes addresses which doesn't
     * contain all necessary components, expands address list if name of region
     * matches with city in the country
     * @param results array of geocode results from google
     * @param callback method to call after filtering complete, method should
     * take one parameter filteredList. filteredList is array of objects {country: , city: , region: ,
     * formattedAddress: , position: LatLng, cityId -- id in local db, coutryId, regionId}
     */
    this.filterGeocodeResults = function (results, callback){

        $.post(baseControllerUrl+'lmc_filterGeocodeAddresses', {list: serialize(results)},
               function(data){

                   // restore js objects
                   for(var i=0; i<data.length; i++)
                   {
                       data[i].position = results[data[i].oldResultsIndex].geometry.location;
                   }
                   
                   callback(data);
               },
               
               'json');
    }

    
    /**
     * Assigns resulting coords in hidden fields of form
     * @param center center coords object
     * @param coords array of objects
     */
    this.assignCoordsInResultFields = function(center, coords)
        {
            $('#search_center_lng').val(center.lng());
            $('#search_center_lat').val(center.lat());
            for(var i=0; i<this.vertexNum; i++)
            {
                $('#search_point'+i+'_lng').val(coords[i].lng());
                $('#search_point'+i+'_lat').val(coords[i].lat());
            }
        }
    
    
    /**
     * Clears coords stored in hidden fields
     */
    this.clearStoredCoords = function ()
        {
            $('#search_center_lng').val('');
            $('#search_center_lat').val('');
            for(var i=0; i<this.vertexNum; i++)
            {
                $('#search_point'+i+'_lng').val('');
                $('#search_point'+i+'_lat').val('');
            }
        }
    

    /**
     * Event function. Called on dialog open
     */
    this.onDialogOpen = function()
    {
        // trigger at first open so google map is shown right
        google.maps.event.trigger(this.map, 'resize');

        // clear old error messages/notifications
        $('#mapDialogUserNotification').html('');
        $('#mapDialogAddrFormErrorMsgs').html('');

        var city = $('#locationMapCity').val();

        // fill address in form
        if (!city.match(/\w|\d/))
        {
            city = '';
        }
        else
        {
            city += ', ';
        }

        if ($('#locationMapRegion').val())
        {
            var region = $('#locationMapRegion option[value='+$('#locationMapRegion').val()+']').html()+', ';
        }
        else
        {
            var region = '';
        }
        
        var country = $('#locationMapCountry option[value='+$('#locationMapCountry').val()+']').html();
        var formattedAddress = city+region+country;
        $('#mapDialogAddress').val(formattedAddress);

        // fill radius
        $('#mapDialogRadius').val($('#locationMapRadius').val());
        
        // if we have coords
        if ($('#search_center_lng').val())
        {
            // make coords array
            var coords = [];
            for(var i=0; i<this.vertexNum; i++)
            {
                coords[i] = new google.maps.LatLng($('#search_point'+i+'_lat').val(), $('#search_point'+i+'_lng').val());
            }
            
            // draw polygon
            this.drawPolygonOnMap(new google.maps.LatLng($('#search_center_lat').val(), $('#search_center_lng').val()), coords);

            // set resultAddress
            this.resultAddress = {country: country, city: $('#locationMapCity').val(), region: region, formattedAddress:formattedAddress};
        }
        else
        {
            // try to lookup address
            // if several results show "Pick the best match:" section
            this.showAddressOnMap($('#mapDialogAddress').val(), $('#mapDialogRadius').val());
        }

    }


    /**
     * If radius in input is correct returns it. If radius incorrect returns
     * default radius
     */
    this.getRadiusFromInput = function(input)
    {
        // check radius
        var radius = parseFloat(input);
        if (radius>=0.01)
        {
            return radius;
        }
        else
        {
            return this.defaultRadius;
        }
    }
    

    /**** pick best match related functions ******/

    /**
     * initially shows matches list and fills address box
     * @param redrawPolyIfSingle flag that tells to redraw polygon if there is
     * only one match.
     * @return true if all passed good. False if there is no usable results in
     * list
     */
    this.bestMatchShowList = function(formattedList, redrawPolyIfSingle)
    {
        // empty old list
        this.bestMatchRemoveList();
        
        // remove error messages related with address
        formValidationNotify({field: $('#mapDialogAddress'), status: 'pass'});

        if (formattedList.length==0)
        {
            // no usable results
            return false;
        }

        // record first match in field 
        $('#mapDialogAddress').val(formattedList[0].formattedAddress);
        this.resultAddress = {country: formattedList[0].country, city: formattedList[0].city, region: formattedList[0].region, formattedAddress:formattedList[0].formattedAddress};
        
        if (formattedList.length==1)
        {
            // only one usable entry, no need to show list to user,
            if (redrawPolyIfSingle)
            {
                // do "map it" work
                var coords = this.getPolyVertexCoords(formattedList[0].position, this.getRadiusFromInput($('#mapDialogRadius').val()));
                this.drawPolygonOnMap(formattedList[0].position, coords);

            }

            return true;
        }

        // show list to user
        var listHtml = '<h2 style="margin-top:0px; margin-bottom:5px;">Pick the best match</h2><div style="max-height:190px; overflow: auto">';

        for(var i=0; i<formattedList.length; i++)
        {
            listHtml += '<div class="pickBestMatchItem" country="'+formattedList[i].country+'" city="'+formattedList[i].city+'" region="'+formattedList[i].region+'" lat="'+
                formattedList[i].position.lat()+'" lng="'+formattedList[i].position.lng()+'" formattedAddress="'+formattedList[i].formattedAddress+'">'+
                formattedList[i].formattedAddress+'</div>';
        }

        listHtml += '</div>';
        
        $('#mapDialogBestMatch').html(listHtml);
        
        // add event handler
        $('#mapDialogBestMatch div').click(this.bestMatchClickOnItem);

        return true;
    }


    /**
     * Event function, called after user clicked on best match item. Updates
     * polygon on map and result address
     */
    this.bestMatchClickOnItem = function(){
        
        // set result address
        self.resultAddress = {country: $(this).attr('country'), city: $(this).attr('city'), region: $(this).attr('region'), formattedAddress:$(this).attr('formattedAddress')};
        self.bestMatchRemoveList();

        // fill form field
        $('#mapDialogAddress').val($(this).attr('formattedAddress'));

        // move center to clarified coords
        var center = new google.maps.LatLng($(this).attr('lat'), $(this).attr('lng'));
        var coords = self.getPolyVertexCoords(center, self.getRadiusFromInput($('#mapDialogRadius').val()));
        self.drawPolygonOnMap(center, coords);
    }
    

    /**
     * Removes list of matches
     */
    this.bestMatchRemoveList = function()
    {
        $('#mapDialogBestMatch').html('');
    }

    
    /**** end of pick best match related functions ******/

    
    this.assignSearchMarkersInForm = function(){
        // copy all in form
        this.assignCoordsInResultFields(self.centerMarker.getPosition(), self.searchArea.getPath().getArray());
    }

    
    /**
     * Shows notification message to user, message disappears after some time
     */
    this.showNotificationToUser = function(message)
    {
        $('#mapDialogUserNotification').html(message).fadeToggle("slow", function(){$(this).delay(10000).fadeToggle('slow')});
        
    }

    /**
     * Serves press on map it button on enter button in form fields, mostly done
     * for buggy ie8
     */
    this.mapIt = function(){

        googleMap.showAddressOnMap($('#mapDialogAddress').val(), $('#mapDialogRadius').val());
    }
    
    
    // initialize
    this.geocoder = new google.maps.Geocoder();


    // show map
    var defaultPosition = new google.maps.LatLng(26.000, -1.000);
    var myOptions = {
        zoom: 2,
		center: defaultPosition,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
        draggableCursor: 'pointer',
		//mapTypeId: google.maps.MapTypeId.ROADMAP,

		mapTypeControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
			position: google.maps.ControlPosition.TOP_RIGHT,
			mapTypeIds: Array(google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID)
		},
		navigationControl: true,
		navigationControlOptions: {
			//style: google.maps.NavigationControlStyle.SMALL,
			style: google.maps.NavigationControlStyle.ZOOM_PAN,
			position: google.maps.ControlPosition.TOP_RIGHT
		},
        //	scaleControl: false,
		scaleControlOptions: {
			position: google.maps.ControlPosition.TOP_LEFT
		}
    }
    this.map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);

    // add click on map listener
    var self = this;
    google.maps.event.addListener(this.map, 'click', function(event) {
                                      // recalculate polygon
                                      var coords = self.getPolyVertexCoords(event.latLng, self.getRadiusFromInput($('#mapDialogRadius').val()));
                                      // show it
                                      self.drawPolygonOnMap(event.latLng, coords);
                                      // update address
                                      self.reverseGeocode(event.latLng);
                                  });

    // save button listener
    $('#mapDialogSaveBtn').click(function(){

                                     if (self.resultAddress)
                                     {
                                         // all ok
                                         //mapDialog.dialog( "close" );

                                         // copy address, if all good copies
                                         // address and closes dialog, otherwise
                                         // shows error message
                                         $.getScript(baseControllerUrl+'lmc_copyAddressInForm/'+self.resultAddress.country+'/'+self.resultAddress.region+'/'+
                                                     self.resultAddress.city, function(script){
                                                         // eval(script);
                                                     });
                                     }
                                     else
                                     {
                                         // address was changed and "map it" not
                                         // clicked or we are in error state,
                                         // call showAddressOnMap
                                         self.showAddressOnMap($('#mapDialogAddress').val(), self.getRadiusFromInput($('#mapDialogRadius').val()),
                                                               function(){
                                         
                                                                   // show message to user
                                                                   self.showNotificationToUser('Your new address is shown on the map. Press \'Save\' to confirm your location selection.');
                                                               }
                                             );
                                     }

                                 });

    // close button listener
    $('#mapDialogCloseBtn').click(function(){$('#mapDialog').dialog('close');});

    // address field listener
    $('#mapDialogAddress').change(function(){self.resultAddress = null});

    // special enter key processing in map it form
    $('#mapDialogAddress').keypress(function(e)
                                   {
                                       code= (e.keyCode ? e.keyCode : e.which);
                                       if (code == 13)
                                       {
                                           // check if address was changed
                                           if (self.resultAddress!=null && self.resultAddress.formattedAddress==$(this).val())
                                           {
                                               $('#mapDialogSaveBtn').trigger('click');
                                               e.preventDefault();
                                           }
                                           else
                                           {
                                               self.mapIt();
                                           }
                                       }
                                   });


    // address field listener
    $('#mapDialogRadius').change(function(){self.resultAddress = null}).keypress(function(e){
    
        code = (e.keyCode ? e.keyCode: e.which);
        if(code==13)
        {
            self.mapIt();
        }
    });
}


////////////////////// functions which serves page part


function countryOnChange()
{
    var countryId = $('#locationMapCountry').val();

    // loading regions list
    $.get(baseControllerUrl+'lmc_getRegionsList/'+countryId, function(data){
              $('#locationMapRegion').replaceWith(data);
              addressChanged();
          });
}

function regionOnChange()
{
    addressChanged();
}


function regionOnBlur()
{
    if (!$('#locationMapRegion').val())
    {
        // do nothing, because there is not enough data
        addressValidationNotify({status:'fail', prompt:'Wrong address. State/Region need to be selected.'});
        return false;
    }
}


function cityOnBlur()
{
    if (!$('#locationMapCity').val())
    {
        // do nothing, because there is not enough data
        addressValidationNotify({status:'fail', prompt:'Wrong address. City need to be entered.'});
        return false;
    }
}


/**
 * Validates address in google. If it valid calculates all hidden latitudes and
 * longtitudes, marks fields with green
 * If google gives several possibilities, shows dialog so user can select one
 * If address invalid (google doesn't know it) adds error message, marks field
 * red
 */
function addressChanged(cityChanged)
{
    googleMap.clearStoredCoords();
//    alert('address_changed');

    // static var
    if (cityChanged)
    {
        addressChanged.addressEntered = true;
    }

    // check if all fields filled (basically city field)
    if (!$('#locationMapCity').val())
    {
        // do nothing, because there is not enough data
        if (addressChanged.addressEntered)
        {
            addressValidationNotify({status:'fail', prompt:'Wrong address. City need to be filled.'});
        }
        return false;
    }

    if (!$('#locationMapRegion').val())
    {
        // do nothing, because there is not enough data
        addressValidationNotify({status:'fail', prompt:'Wrong address. State/Region need to be selected.'});
        return false;
    }
    
    // if radius not filled use default
    var radius = googleMap.getRadiusFromInput($('#locationMapRadius').val());
    
    // check if google knows entered address
    googleMap.getAddressCoords($('#locationMapCity').val()+', '+$('#locationMapRegion option[value='+$('#locationMapRegion').val()+']').html()+', '+
                               $('#locationMapCountry option[value='+$('#locationMapCountry').val()+']').html(),
                               function(addresses){

                                   // filter out addresses which do not match
                                   // with dropdown values, google may return
                                   // addresses like 'Los Angeles' restarunt
                                   // Birmingam, Alabama
                                   var filteredAddresses = [];
                                   var curCountryId = parseInt($('#locationMapCountry').val(), 10);
                                   var curRegionId = $('#locationMapRegion').val();
                                   var curCity = $('#locationMapCity').val().toLowerCase().replace(/^\s+|\s+$/g,"");
                                   for (var i=0; i<addresses.length; i++)
                                   {
                                       // addresses[i].city.toLowerCase() == $('#locationMapCity').val() &&
                                       if (addresses[i].city.toLowerCase() == curCity &&
                                           addresses[i].regionId == curRegionId && addresses[i].countryId == curCountryId)
                                       {
                                           filteredAddresses.push(addresses[i]);
                                       }
                                   }
                                   
                                   if (filteredAddresses.length==0)
                                   {
                                       // it is invalid address
                                       //formValidationNotify({field: $("#search_center_lat"), status: 'fail', prompt: 'Invalid address. Please correct it'});
                                       addressValidationNotify({status:'fail', prompt: 'Invalid address. Please correct it'});
                                       
                                       // send delete request (we don't know precisely if address was suggested)
                                       if(addresses.length>0)
                                       {
                                           // we can't do it in getAddressCoords due cache
                                           $.post(baseControllerUrl+'lmc_address_not_match', {params:
                                                      serialize({city: curCity, regionId: curRegionId, countryId: curCountryId, addresses: addresses})});
                                       }
                                       return false;
                                   }

                                   if (filteredAddresses.length==1)
                                   {
                                       // good, calculate coords
                                       var center = filteredAddresses[0].position;
                                       var coords = googleMap.getPolyVertexCoords(center, radius);

                                       // assign in hiddens
                                       googleMap.assignCoordsInResultFields(center, coords);

                                       // mark fields valid
                                       //formValidationNotify({field: $("#search_center_lat"), status: 'pass', prompt: ''});
                                       addressValidationNotify({status:'pass', prompt:''});
                                   }
                                   else
                                   {
                                       // show user dialog so he can select right address
                                       mapDialog.dialog('open');
                                   }
                               }); 
}


/**
 * Function written in ajax validation notation.
 * Checks if request to google currently runs, if it runs postpones validation
 * till google request complete
 * @returns "pass", "fail", or "wait". In case of "wait", later addtional
 */
function validateAddress(params, field)
{
    // for call from validator
    if (typeof(field) == 'undefined')
    {
        field = this;
    }
    
    // if request is active
    if (googleMap.requestActive)
    {
        setTimeout(function(){
                       var result = validateAddress(params, field);
                       if (result!='wait')
                       {
                           // notify validator
                           $.fn.formValidation.delayed.call (field, params, result);
                       }
                   },
                   300);
        return 'wait';
    }

    // no request is active, validate as usual
    if ($('#search_center_lng').val())
    {
        return 'pass';
    }
    else
    {
        return 'fail';
    }
}


/**
 * Marks address fields with red and shows error message in top of form.
 * Or marks field with greeen and removes error message from top of form
 * @param options same as in standard notification call
 */
function addressValidationNotify(options)
{
 
    // by agreement div for error messages of form should have id formIdErrorMsgs
    var formErrorMsgsId = $('#locationMapCity').closest('form').attr('id')+'ErrorMsgs';
    var addressTopErrorMessageId = formErrorMsgsId+'locationMapAddress';

    var fieldNames = {locationMapCity:1, locationMapRegion:1, locationMapCountry:1};

    switch(options.status)
    {
    case 'pass':
        // remove top error message
        $('#'+addressTopErrorMessageId).remove();
        

        for (var field in fieldNames)
        {
            // remove fields error messages
            $('#'+field).removeClass('form-error');
            $('#'+field+'+div.error-message').remove();

            // add success
            $('#'+field).addClass('field-valid');
        }
        break;
    case 'fail':

        if (options.prompt)
        {
            // add top error message
            // div containing error for address exists, insert message in it
            if($('#'+addressTopErrorMessageId).length)
            {
                $('#'+addressTopErrorMessageId).html(options.prompt); 
            }
            else
            {
                $('#'+formErrorMsgsId).append('<div id="'+addressTopErrorMessageId+'" class="error-message">'+options.prompt+'</div>');
            }
        }
        
        // mark fields
        for (var field in fieldNames)
        {
            $('#'+field).removeClass('field-valid');
            $('#'+field).addClass('form-error');
        }
    }
}



// compatibility method
if(!Array.prototype.indexOf)
	Array.prototype.indexOf = function(searchElement, fromIndex){
		for(var i = fromIndex||0, length = this.length; i<length; i++)
			if(this[i] === searchElement) return i;
		return -1
	};

