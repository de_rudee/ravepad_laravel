﻿/*
 * FormHint 1.0 - jQuery Plugin
 * http://www.smartango.com/
 * Copyright (c) 2009 Daniele Cruciani
 * Dual licensed under the MIT and GPL licenses
 */
;(function($){
    $.fn.formHint = function(){
	$(this).each(function(){
	    var formel = $(this);
	    var pos = formel.offset();
	    var hintstr = formel.attr('title');
	    var h = formel.outerHeight();
	    var w = formel.outerWidth();
	    var divhint = $("<div>").html(hintstr)
		.css({
		    position:'absolute',
		    top: pos.top,
		    left: pos.left,
		    width: w,
		    height: h,
		    opacity: '0.4',
		    //'filter':'alpha(opacity = 50)',
		    'padding': '2px',
		    'font-family':formel.css('form-family'),
		    'font-weight':formel.css('font-weight'),
		    'font-size':formel.css('font-size'),
		    'display':'none',
		    'overflow':'hidden',
		    'white-space':'pre',
		    'z-index':'2000'
		});
	    formel.after(divhint);
	    if(formel.val()==''){
		divhint.css('display','block');
	    }
	    divhint.click(function(){$(this).css('display','none');formel.focus();});
	    formel.focus(function(){
		divhint.css('display','none');
	    });
	    formel.blur(function(){
		if(formel.val()==''){
		    divhint.css('display','block');
		}
	    });
	});
	return this;
    }
})(jQuery);
