// JavaScript Document
function ajaxLoadingBegin(target) {
	$(target).prepend('<div class="loading">&nbsp;</div>');
	$('.loading').width($(target).width());
	$('.loading').height($(target).height());
	$('.loading').fadeTo(1, 0.85);
}

function ajaxLoadingComplete() {
	$('.loading').remove();
}

function ajaxLoadForm(formId, url, activate) {
	var formContainer = target+"Container";
	activate = (typeof activate == 'undefined') ? true : activate;
	ajaxLoadingBegin(formContainer);
	$(formContainer).load(url, null, function() {
		if (activate) {
			ajaxActivateForm(formId);
		}
	});
}

function ajaxLoadUrl(target, url) {
	ajaxLoadingBegin(target);
	$(target).load(url, null, null);
}


/**
 * Activates form, makes it submiting using ajax
 *
 * @param target should contain string presenting id of form, for example
 * "#myForm", it assumed that there is wrapper of form with id
 * target+'Container', for example "#myFormContainer" content of it will be
 * replaced with result of form submit request
 *
 * @param callback optinal callback function which called before submit, if
 * function returns false, submit will no happen
 * @param replaceContainer optional param by default false, tells replace whole 
 * container with server response instead of replacing just container contents 
 */
function ajaxActivateForm(target, callback, replaceContainer) {
        var formContainer = target+"Container";
	$(target).ajaxForm({
		target: formContainer,
		replaceTarget: replaceContainer,
		beforeSubmit: function() {
                               // check if validation needed
                               if ($(target).hasClass('formValidation_submit') ) 
                               {
                                   // do validation
                                   var result = $.fn.formValidation._doValidationForm.call($(target)[0]);
                                   if (result!='pass')
                                   {
                                       return false;
                                   }
                               }
                               // check if callback need to be called
                               if (callback && !callback())
                               {
                                   return false;
                               }
			ajaxLoadingBegin(formContainer);
		},
		success: function() {
			ajaxActivateForm(target);
		}
	});
}
function ajaxCancelForm(target) {
	$(target).val('1');
}

