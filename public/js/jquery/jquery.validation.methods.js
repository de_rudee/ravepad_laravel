(function ($) {

/*
@product     JQuery Form Validation plugin
@version     0.9.0
@copyright   Copyright (c) 2010 Yaron Tadmor
@license     GPL license (http://www.gnu.org/licenses/gpl.html)
@requires    

NOTE: This file is part of the jquery.validation.js package by Yaron Tadmor.

*/
	////// Validation methods

	//// REQUIRED
	$.fn.formValidation.required = function(params) {
		return ($(this).val() ? true : false);		
	}
	$.fn.formValidation.required.radio = $.fn.formValidation.required.checkbox = function(params) {
		fieldName = $(this).attr("name");
		var numSelected = $("input[name='"+fieldName+"']:checked").size();
		return (numSelected != 0);
	}
	$.fn.formValidation.required.select = function(params) {
		var numSelected = $(this).find(':selected').size()
		return (numSelected != 0);
	}

	$.fn.formValidation.required.prompt = { "fail": "This field is requried" }; 
	

	
	//// REGEX
	// params: {
	// 	 pattern: regexobj
	// }
	$.fn.formValidation.regex = function(params) {
		return (params.pattern.test ($(this).val()));
	}
	
	$.fn.formValidation.regex.prompt = { "fail": "The value is invalid" }; 

	// regex: email
	$.fn.formValidation.email = function(params) {
		return ($.fn.formValidation.regex.call (this, { pattern: /^[a-zA-Z0-9_\.\-+]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/ } ));
	}
	
	$.fn.formValidation.email.prompt = { "fail": "Invalid email address" }; 

	// regex: phone
	$.fn.formValidation.phone = function(params) {
		return ($.fn.formValidation.regex.call (this, { pattern: /^[0-9\-\(\)\ ]+$/ } ));
	}
	
	$.fn.formValidation.phone.prompt = { "fail": "Invalid phone number" }; 

	// regex: date
	$.fn.formValidation.date = function(params) {
		return ($.fn.formValidation.regex.call (this, { pattern: /^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/ } ));
	}
	
	$.fn.formValidation.date.prompt = { "fail": "Invalid date format" }; 

	// regex: aNumber
	$.fn.formValidation.aNumber = function(params) {
		return ($.fn.formValidation.regex.call (this, { pattern: /^[0-9\.]+$/ } ));
	}
	
	$.fn.formValidation.aNumber.prompt = { "fail": "Numbers only" }; 
	
	
	
	//// LENGTH LIMIT
	// params: {
	// 	 min: <min length>,
	//   max: <max length>,
	// }
	
	$.fn.formValidation.lengthLimit = function(params) {
		fieldLength = $(this).attr('value').length;
		params = $.extend ({ min: 0, max:1000000 }, params);
		
		
		return !(fieldLength > params.max || fieldLength < params.min);
	}
	
	$.fn.formValidation.lengthLimit.prompt = { "fail": function(params) {
			if (typeof params.min == "undefined")
				if (typeof params.max == "undefined")
					return ("");
				else
					return ("Length can be up to %max% characters");
			else
				if (typeof params.max == "undefined")
					return ("Length must be at least %min% characters");
				else
					return ("Length must be between %min% to %max% characters");
		}
	};

	
	$.fn.formValidation.lengthLimit.checkbox = function ( params) {
		params = $.extend ({ min: 0, max:1000000 }, params);

		$this = (this);
		groupname = $this.attr("name");
		groupSize = $this.closest("form").find ("[name='"+groupname+"']:checked").size();
		return !(groupSize > params.max || groupSize < params.min);
	}
	
	$.fn.formValidation.lengthLimit.checkbox.prompt = { "fail": function(params) {
			if (typeof params.min == "undefined")
				if (typeof params.max == "undefined")
					return ("");
				else
					return ("Please select up to %max% options");
			else
				if (typeof params.max == "undefined")
					return ("Please select at least %min%");
				else
					return ("Please select %min% to %max% options");
			
		}
	}
	

	
	//// VALUE LIMIT
	// params
	// params: {
	// 	 min: <min value>,
	//   max: <max value>,
	// }
	$.fn.formValidation.valueLimit = function(params) {
		params = $.extend ({ min: 0, max:1000000 }, params);

		val = $(this).attr('value');
		val = parseFloat (val);
		
		return !(val > params.max || val < params.min);
	}
	
	$.fn.formValidation.valueLimit.prompt = { "fail": function(params) {
		if (typeof params.min == "undefined")
			if (typeof params.max == "undefined")
				return ("");
			else
				return ("Value must be less than %max%");
		else
			if (typeof params.max == "undefined")
				return ("Value must be at least %min%");
			else
				return ("Value must be between %min% to %max%");			
		}
	};

	

	//// MATCH FIELD
	// params: {
	// 	 field: <name of field to match>
	//   fieldString: <user friendly name of field to match>
	// }
	$.fn.formValidation.matchField = function(params) {
		$this = $(this);
		return ($this.attr('value') ==  $this.closest("form").find("[name='"+params.field+"']").attr('value'));
	}
	
	$.fn.formValidation.matchField.prompt = { "fail": "Must be identical to %fieldString%" }; 
	
	
	
	//// AJAX
	// params: {
	// 	 url: url
	// }
	$.fn.formValidation.ajax = function (params) {
		// Send POST with:
		// ajax
		// validation
		// form=<form id>
		// field=<field name>
		// value=<field value>
		
		// expects "pass" if succeeded, anything else otherwise.
	
		var field = this;
		$this = $(this);
		$.ajax ({
			type: "POST",
			url: params.url,
			async: true,
			data: "ajax=1&validation=1&form="+$this.closest("form").attr("id")+"&field="+$this.attr("name")+"&value="+$this.val(),
			dataType: "text",
			error: function (request, textStatus, errorThrown) {
				$.fn.formValidation.delayed.call (field, params, "fail");
			},
			success: function (data, textStatus, request) {
				$.fn.formValidation.delayed.call (field, params, (data == "pass") ? "pass" : "fail");
			}
		});
		
		return ("wait");
	}

	$.fn.formValidation.ajax.prompt = { "pass" :"Value is OK", "wait": "Checking value", "fail": "Value is not valid" }; 
	
	

}) (jQuery);
