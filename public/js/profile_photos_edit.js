$(function(){

uploadDialog = $('#uploadDialog')
    .dialog({
          autoOpen: false,
                modal: true,
                height: 'auto',
                width: 'auto',
                resizable:true,
                draggable: false,
                title: 'Upload photo',
                closeOnEscape: true,
			open: function(event, ui) { $('.ui-widget-overlay').bind('click', function(){ $("#uploadDialog").dialog('close'); }); }
                });
      


      // upload more photos handler
      $('.uploadMoreButton').click(function(){
                                       $('#uploadDialog').html(uploadPhotoForm);
                                       $('#uploadDialog').dialog('open');
                                   });

      $('#thumbnailsList').sortable({
            update: function(event, ui){

                  var sortOrder = getCurrentSortOrder();
                  saveSortOrderToServer(sortOrder);
              }
          });
  });

function saveSortOrderToServer(sortOrder)
{
    // save new order to server
    $.ajaxq('orderQueue', { url:profilePhotoCtrBaseUrl+'save_sort_order', data: {order: sortOrder }, type: 'POST'});
                  
    // change order of big images
    for(var i=0; i<sortOrder.length; i++)
    {
        $('#sortManipulation').append($('#photoCtrl_'+sortOrder[i]).clone());
    }

    $('#bigPhotos').html($('#sortManipulation').html());
    $('#sortManipulation').html('');

    // activate edit caption forms
    for(var i=0; i<sortOrder.length; i++)
    {
        ajaxActivateForm('#editCaptionForm'+sortOrder[i]);
        // deal with main image
        if (i==0)
        {
            $('#thumbnailCtrl_'+sortOrder[i]+' span.mainImage').html('Main Image');
        }
        else
        {
            $('#thumbnailCtrl_'+sortOrder[i]+' span.mainImage').html('<a href="#" onClick="return setMainImage('+sortOrder[i]+');">Set as main image</a>');
        }
    }
    showJgrowlMessage('The order of your photos, as they will show on your profile, has been changed.');
}

function getCurrentSortOrder()
{
    // extract ids
    var sortedIds = $('#thumbnailsList').sortable('toArray');
    var sortOrder = [];
    for(var i=0; i<sortedIds.length; i++)
    {
        var parts = sortedIds[i].split('_');
        sortOrder[i] = parts[1];
    }

    return sortOrder;
}


/**
 * Sets photo with given id to be main image
 */
function setMainImage(photoId, imgHtml)
{
    var sortOrder = getCurrentSortOrder();

    var newSortOrder = [photoId];
    
    // put necessary photo on top
    for(var i=0; i<sortOrder.length; i++ )
    {
        if (sortOrder[i] != photoId)
        {
            newSortOrder.push(sortOrder[i]);
        }
    }

    // save order on server
    saveSortOrderToServer(newSortOrder);

    // change thumbnails order
    for(var i=0; i<newSortOrder.length; i++)
    {
        $('#sortManipulation').append($('#thumbnailCtrl_'+newSortOrder[i]).clone());
    }

    $('#thumbnailsList').html($('#sortManipulation').html());
    $('#sortManipulation').html('');

    // refresh sortable list
    $('#thumbnalisList').sortable('refresh');
	if (imgHtml) {
		showJgrowlMessage('Your main image has been changed to ' + imgHtml);
	} else {
		showJgrowlMessage('Your main image has been changed.');
	}
}


function showEditCaptionForm(photoId)
{
    // show form
    $('#editCaptionLink'+photoId).hide();
    $('#editCaptionFormDiv'+photoId).show();
}


function cancelEditCaptionForm(photoId)
{
    // hide form
    $('#editCaptionLink'+photoId).show();
    $('#editCaptionFormDiv'+photoId).hide();
}


function editThumbnail(photoId)
{
    // clear dialog
    $('#uploadDialog').html('');
    // open dialog
    $('#uploadDialog').dialog('open');

    
    // load save thumbnail form
    $.get(profilePhotoCtrBaseUrl+'save_thumbnail/'+photoId, function(data){
              $('#uploadDialog').html(data);
          });
}

/**
 * Called only for not fully created images
 */
function thumbnailSaveCancel(photoId)
{
    // close dialog
    $('#uploadDialog').dialog('close');
    
    // send ajax notification
    $.get(profilePhotoCtrBaseUrl+'cancel_photo_save/'+photoId);
}


// Our simple event handler, called from onChange and onSelect
// event handlers, as per the Jcrop invocation above
function thumbnailSaveCoords(c)
{
    jQuery('#thumbnail_x1').val(c.x);
    jQuery('#thumbnail_y1').val(c.y);
    jQuery('#thumbnail_x2').val(c.x2);
    jQuery('#thumbnail_y2').val(c.y2);
    //jQuery('#w').val(c.w);
    //jQuery('#h').val(c.h);
};

function deletePhoto(deleteUrl)
{
	showConfirmationDialog('Are you sure you want to delete this photo permanently?',
		function(){
			window.location.href = deleteUrl;
			showJgrowlMessage('Your photo has been deleted.');
		});
	return false;
}

function togglePhotoHide(photoId, visibility, imgHtml)
{
	$.get(profilePhotoCtrBaseUrl+'toggle_photo_hide/'+photoId,
		function(data){
			var visibilityStatus = (visibility==0) ? 'visible.' : 'hidden.';
			$('#togglePhotoHideCtrl_'+photoId).replaceWith(data);
			showJgrowlMessage('Your photo ' + imgHtml + ' will now be ' + visibilityStatus);
		});
	return false;
}
