$(function(){

var uploadSrc;
var uploadDest;

var signUpDialog = $('#signUpDialog')
	.dialog({
		autoOpen: false,
		modal: true,
		height: 'auto',
		width: 'auto',
		resizable:true,
		draggable: false,
		title: '',
		closeOnEscape: true,
		open: function(event, ui) { $('.ui-widget-overlay').bind('click', function(){ $("#signUpDialog").dialog('close'); }); }
	});

	// post-submit callback
	function showResponse(responseText, statusText, xhr, $form) {
		// for normal html responses, the first argument to the success callback 
		// is the XMLHttpRequest object's responseText property 

		// if the ajaxSubmit method was passed an Options Object with the dataType 
		// property set to 'xml' then the first argument to the success callback 
		// is the XMLHttpRequest object's responseXML property 

		// if the ajaxSubmit method was passed an Options Object with the dataType 
		// property set to 'json' then the first argument to the success callback 
		// is the json data object returned by the server 

		console.log(responseText);
		var relativePath = "../../../";
		if (responseText['level']) {
			relativePath = "../";
			// TODO: Fix this to a more general path.
		}

		if (responseText['name']['error'] != undefined) {
			$('<div class="imageError">' + responseText['name']['error'] + '</div>').insertAfter('#' + uploadSrc);
			$('.imageError').css({
				'float' : 'left',
				'background' : '#FFC0C0',
			});
			$('#' + uploadSrc).attr("src", "");
		} else {
			$('#' + uploadSrc).attr("src", relativePath + "img/photos/" + responseText['type'] + "/tmp/" + responseText['name']);
			$('#' + uploadDest).attr("value", responseText['name']);
		}
	}

	$('.signUpForm').click(function(){
		$('#signUpDialog').html(signUpForm);
		$('#signUpFormContainerFields div > input').each(function() {
		  $(this).val($(this).attr('title'));
		  $(this).addClass("defaultTextActive");
		});
		
		$('#signUpDialog').dialog('open');
		
		//uploadSrc = this.attributes['src'].value;
		//uploadDest = this.attributes['dest'].value;
		
		/* Check if there's an imageError div. If so, remove it. */
		if ($('.imageError')) {
			$('.imageError').remove();
		}
	});
	
	$('.notLoggedIn').click(function(){
		$('#signUpDialog').html(notLoggedInForm);
		$('#signUpFormContainerFields div > input').each(function() {
		  $(this).val($(this).attr('title'));
		  $(this).addClass("defaultTextActive");
		});
		
		$('#signUpDialog').dialog('open');
		
		//uploadSrc = this.attributes['src'].value;
		//uploadDest = this.attributes['dest'].value;
		
		/* Check if there's an imageError div. If so, remove it. */
		if ($('.imageError')) {
			$('.imageError').remove();
		}
		
		$('#JoinRavePad').click(function() {
			$('#signUpDialog').dialog('close');
			$('.signUpForm').click();
		});
		
		$('#Login').click(function() {
			$('#signUpDialog').dialog('close');
			$('.Login').click();
		});
	});
	
	$('.Login').click(function(){
		$('#signUpDialog').html(LoginForm);
		$('#signUpFormContainerFields div > input').each(function() {
		  $(this).val($(this).attr('title'));
		  $(this).addClass("defaultTextActive");
		});
		
		$('#signUpDialog').dialog('open');
	});	
	
	$('#loginSubmitBtn').click(function(link) {
		var options = {
			dataType: 'json',		// 'xml', 'script', or 'json'
			success: showResponse	// post-submit callback
		};
		link.preventDefault();
		console.log('hi');
//		$("#UserLoginForm").ajaxSubmit(options);
//		return false;
	});
});


