function showJgrowlMessage(message) {
	$.jGrowl(message, {life:10000});
}

function showAwardedBadge(badgeJson) {
	var badge = $.parseJSON(badgeJson);
	if (badge && badge.name) {
		$.jGrowl('Congrats! ' + badge.description + ' You won the "' + badge.name + '" badge!' + '<br/><img src="' + absolutePath + '/img/' + badge.image_path_active + '" height="100" width="100" class="badgeIcon"/>', {theme: 'black', life:10000, sticky: true, position: 'top-right', header: 'You won a badge!'});
	}
}

function showRevokedBadge(badgeJson) {
	var badge = $.parseJSON(badgeJson);
	if (badge && badge.name) {
		$.jGrowl('You lost your "' + badge.name + '" badge!' + '<br/><img src="' + absolutePath + '/img/' + badge.image_path_inactive + '" height="100" width="100" class="badgeIcon"/>', {theme: 'black', life:10000, sticky: false, position: 'top-right', header: 'We\'re sorry!'});
	}
}

/**
 * Shows confirmation dialog and calls corresponding callback depending from
 * user choice
 * @param message message to show in dialog
 * @param successCallback called if user clicked ok
 * @param cancelCallback called if user clicked cancel
 */
function showConfirmationDialog(message, successCallback, cancelCallback, title)
{
    if ($('#confirmationDialog').length)
    {
        // dialog exists, change handlers
        $('#confirmationDialog').dialog('option', {buttons: {
                                                "Confirm": function() {
                                                    $('#confirmationDialog').attr('success', 1);
                                                    $( this ).dialog( "close" );
                                                    if (successCallback)
                                                    {
                                                        successCallback();
                                                    }
                        
                                                  },
                        
                                                  Cancel: function() {
                                                    $( this ).dialog( "close" );
                                                    /*if(cancelCallback)
                                                    {
                                                        cancelCallback();
                                                    }*/
                                                  },
                                               },
                                                
                                               close: function(event, ui){
                                                if ($('#confirmationDialog').attr('success'))
                                                {
                                                    $('#confirmationDialog').attr('success', 0);
                                                }
                                                else
                                                {
                                                   if(cancelCallback)
                                                   {
                                                      cancelCallback();
                                                   }
                                                }
                                               }
                                              });
        
        $('#confirmationDialog').dialog('open');
    }
    else
    {
        $('<div id="confirmationDialog"></div>').dialog({
              resizable: false,
                    title: title == null ? 'Confirmation' : title,
                    height: 'auto',
                    //width: 'auto',
                    modal: true,
                    buttons: {
                                                "Confirm": function() {
                                                    $('#confirmationDialog').attr('success', 1);
                                                    $( this ).dialog( "close" );
                                                    if (successCallback)
                                                    {
                                                        successCallback();
                                                    }
                        
                                                  },
                        
                                                  Cancel: function() {
                                                    $( this ).dialog( "close" );
                                                    /*if(cancelCallback)
                                                    {
                                                        cancelCallback();
                                                    }*/
                                                  },
                                               },
                                                
                    close: function(event, ui){
                    if ($('#confirmationDialog').attr('success'))
                    {
                        $('#confirmationDialog').attr('success', 0);
                    }
                    else
                    {
                        if(cancelCallback)
                        {
                            cancelCallback();
                        }
                    }
                }
                
            });
    }
    $('#confirmationDialog').html(message);
}


/**
 * Creates dialog
 * @param element either id or element it self on/from that we create dialog
 */
function createDialog(element, options)
{
    var defaultOptions = {
        resizable: false,
        height: 'auto',
        width: 'auto',
        modal: true,
        closeOnEscape: true,
    };

    return $(element).dialog($.extend(defaultOptions, options));
}

/**
 * Camel cases a string
 * @param string to be camel-cased
 */
function toCamelCase(str) {
	return str.replace(/(?:^|\s)\w/g, function(match) {
		return match.toUpperCase();
	});
}

/**
 * Flag / Report content
 */
function hideFlagOptions(id) {
	$('#flagOptionsBoxInner_' + id).hide();
	$('#flagLink_' + id).removeClass('flagButtonActive');
}

function unhideFlagOptions(id) {
	$('#flagOptionsBoxInner_' + id).show();
	$('#flagLink_' + id).addClass('flagButtonActive');
}

function hideFlagResult(id) {
	$('#flagResultBox_' + id).hide();
}

function unhideFlagResult(id) {
	$('#flagResultBox_' + id).show();
}

function showFlagOptions(id, flagReasons) {
	if ($('#flagOptionsBox_' + id).length > 0) {
		if ($('#flagOptionsBoxInner_' + id).is(":visible")) {
			hideFlagOptions(id);
		} else {
			unhideFlagOptions(id);
		}
		return;
	}
	$('<div/>', {
		id: 'flagOptionsBox_' + id,
		//title: 'Report this content',
		class: 'flagOptionsBox'
	}).appendTo($('#flagLink_' + id).parent());
	
	$('<div/>', {
		id: 'flagOptionsBoxInner_' + id,
		class: 'flagOptionsBoxInner'
	}).appendTo($('#flagOptionsBox_' + id));
	
	$('<div/>', {
		id: 'flagResultBox_' + id,
		class: 'flagResultBox'
	}).appendTo($('#flagOptionsBox_' + id));
	hideFlagResult(id);
	
	$('<div/>', {
		id: 'flagTitle_' + id,
		title: 'Report this item',
		class: 'flagTitle',
		text: 'Report this item as:',
	}).appendTo($('#flagOptionsBoxInner_' + id));
	
	$('<input/>', {
		id: 'flagComment_' + id,
		title: 'Add more detail (optional)',
		class: 'flagOptionComment',
		placeholder: 'Optional Details',
	}).appendTo($('#flagOptionsBoxInner_' + id));
	
	for (var i in flagReasons) {
		var flagOptionId = 'flagOption_' + id + '_' + i;
		$('<span/>', {
			id: flagOptionId,
			title: 'Report as ' + flagReasons[i],
			contentId: id,
			flagOptionId: i,
			class: 'flagOptionSpan',
			text: flagReasons[i]
		}).appendTo($('#flagOptionsBoxInner_' + id));
		
		var flagContentUrl = webBaseUrl + "flagged_items/report";
		$('#' + flagOptionId).click(function() {
			var contentId = $(this).attr('contentId');
			var flagOptionId = $(this).attr('flagOptionId');
			
			$.post(flagContentUrl, {
					contentId : contentId,
					flagOptionId : flagOptionId,
					comment : $('#flagComment_' + id).val(),
				}, function(data){
					$('#flagComment_' + id).val('');
					hideFlagOptions(id);
					unhideFlagResult(id);
					if (data.success) {
						if (data.result) {
							$('#flagResultBox_' + id).text(data.result);
						} else {
							$('#flagResultBox_' + id).text('');
						}
					} else {
						if (data.showLogin && ($('.Login'))) {
							$('.Login').click();
						} else {
							//alert(data.result);
						}
					}
				}, 'json'
			);
		});
	}
	$('<span/>', {
		id: 'flagCancel_' + id,
		title: 'Don\'t Report',
		class: 'flagOptionSpan flagCancelSpan',
		text: 'Don\'t Report'
	}).appendTo($('#flagOptionsBoxInner_' + id));
	
	unhideFlagOptions(id);
	$('#flagCancel_' + id).click(function() {
		hideFlagOptions(id);
	});
}
