/*
 * Using Multiple JavaScript Onload Functions.
 * Works even if the onload event handler has been previously assigned.
 * The way this works is relatively simple: if window.onload has not already
 * been assigned a function, the function passed to addLoadEvnt is simply 
 * assigned to window.onload. If window.onload has already been set, a brand 
 * new function is created which first calls the original onload handler, 
 * then calls the new handler afterwards.
 * Usage:
 *	addLoadEvnt(nameOfSomeFunctionToRunOnPageLoad);
 *	addLoadEvnt(function() {
 *	});
 */
function addLoadEvnt(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			if (oldonload) {
				oldonload();
			}
			func();
		}
	}
}
