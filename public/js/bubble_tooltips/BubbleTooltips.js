/*javascript for Bubble Tooltips by Alessandro Fulciniti
- http://pro.html.it - http://web-graphics.com */

function enableTooltips(id, opacity, showlinks, type){
var links,i,h;
if(!document.getElementById || !document.getElementsByTagName) return;
// kunal..directly include the css file
// this is buggy, it doesnt load the css file - reduces page speed
//AddCss();
h=document.createElement("span");
h.id="btc";
h.setAttribute("id","btc");
h.style.position="absolute";
document.getElementsByTagName("body")[0].appendChild(h);
if(opacity==null) opacity=0.85;
if(showlinks==null) showlinks=false;
if(type==null) type="a";
if(id==null) {
	links=document.getElementsByTagName(type);
} else {
	links=document.getElementById(id).getElementsByTagName(type);
}
for(i=0;i<links.length;i++){
    Prepare(links[i], opacity, showlinks);
  }
}

function Prepare(el, opacity, showlinks){
var tooltip,t,b,s,l;
var ttf;
t=el.getAttribute("title");
el.removeAttribute("title");

b=CreateEl("b","bottom");
if (showlinks == true) {
	if(t==null || t.length==0) t="link:";
	l=el.getAttribute("href");
	if(l.length>30) l=l.substr(0,27)+"...";
	b.appendChild(document.createTextNode(l));
}
ttf=el.getAttribute("tooltipfooter");
if (ttf) {
	b.appendChild(document.createTextNode(ttf));
}
if(t==null || t.length==0) return;

tooltip=CreateEl("span","tooltip");
s=CreateEl("span","top");
s.appendChild(document.createTextNode(t));
tooltip.appendChild(s);

tooltip.appendChild(b);
setOpacity(tooltip, opacity);
el.tooltip=tooltip;
el.onmouseover=showTooltip;
el.onmouseout=hideTooltip;
el.onmousemove=Locate;
}

function showTooltip(e){
document.getElementById("btc").appendChild(this.tooltip);
Locate(e);
}

function hideTooltip(e){
var d=document.getElementById("btc");
if(d.childNodes.length>0) d.removeChild(d.firstChild);
}

function setOpacity2(el){
el.style.filter="alpha(opacity:65)";
el.style.KHTMLOpacity="0.65";
el.style.MozOpacity="0.65";
el.style.opacity="0.65";
}

function setOpacity(el, opacity){
el.style.filter="alpha(opacity:"+opacity+")";
el.style.KHTMLOpacity=opacity;
el.style.MozOpacity=opacity;
el.style.opacity=opacity;
}

function CreateEl(t,c){
var x=document.createElement(t);
x.className=c;
x.style.display="block";
return(x);
}

function AddCss(){
var l=CreateEl("link");
l.setAttribute("type","text/css");
l.setAttribute("rel","stylesheet");
l.setAttribute("href","css/bubble_tooltips/bt.css");
l.setAttribute("media","screen");
document.getElementsByTagName("head")[0].appendChild(l);
}

function Locate(e){
var posx=0,posy=0;
if(e==null) e=window.event;
if(e.pageX || e.pageY){
    posx=e.pageX; posy=e.pageY;
    }
else if(e.clientX || e.clientY){
    if(document.documentElement.scrollTop){
        posx=e.clientX+document.documentElement.scrollLeft;
        posy=e.clientY+document.documentElement.scrollTop;
        }
    else{
        posx=e.clientX+document.body.scrollLeft;
        posy=e.clientY+document.body.scrollTop;
        }
    }
    
    document.getElementById("btc").style.top=(posy+20)+"px";
    document.getElementById("btc").style.left=(posx-20)+"px";

/*
// TODO: relocate the tooltip based on window size and position
//alert(screen.availWidth);
//alert(window.innerHeight);
//alert(window.outerHeight);
if (posy > (window.outerHeight-50)) {
	document.getElementById("btc").style.top=(posy-20)+"px";
	document.getElementById("btc").style.left=(posx+10)+"px";
} else {
	document.getElementById("btc").style.top=(posy+20)+"px";
	document.getElementById("btc").style.left=(posx-20)+"px";
}
//alert('posy=' + posy + 'innerHeight=' + window.innerHeight);
*/

}
